package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperUserGroupDao;
import com.examsys.po.PaperUserGroup;
import com.examsys.util.DBUtil;
/**
 * 记录可以参与考试的用户组业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperUserGroupServiceImpl extends AbstractBaseService<PaperUserGroup, Integer> implements PaperUserGroupService {

	private PaperUserGroupDao dao;//记录可以参与考试的用户组数据访问层对象
	
	public PaperUserGroupDao getDao() {
		return dao;
	}

	public void setDao(PaperUserGroupDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加记录可以参与考试的用户组
	 *  
	 */
	@Override
	public boolean add(PaperUserGroup obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}
	
	/**
	 * 添加多条信息
	 * @param paperUserGroups
	 * @throws Exception
	 */
	@Override
	public boolean add(List<PaperUserGroup> paperUserGroups){
		boolean flag=false;
		try {
			this.getDao().add(paperUserGroups);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改记录可以参与考试的用户组
	 */
	@Override
	public boolean update(PaperUserGroup obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除记录可以参与考试的用户组
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取记录可以参与考试的用户组
	 * @param id 编号
	 */
	@Override
	public PaperUserGroup get(Integer id) {
		PaperUserGroup paperUserGroup=null;
		try {
			paperUserGroup = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paperUserGroup;
	}

	/**
	 * 获得记录可以参与考试的用户组表所有记录
	 */
	@Override
	public List<PaperUserGroup> getList() {
		List<PaperUserGroup> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取记录可以参与考试的用户组记录
	 */
	@Override
	public List<PaperUserGroup> getList(PaperUserGroup obj) {
		List<PaperUserGroup> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
