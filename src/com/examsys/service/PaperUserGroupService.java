package com.examsys.service;

import java.util.List;

import com.examsys.po.PaperUserGroup;

/**
 * 记录可以参与考试的用户组业务逻辑层接口
 * @author edu-1
 *
 */
public interface PaperUserGroupService extends IBaseService<PaperUserGroup, Integer> {
	/**
	 * 添加多条信息
	 * @param paperUserGroups
	 * @throws Exception
	 */
	public boolean add(List<PaperUserGroup> paperUserGroups);
}
