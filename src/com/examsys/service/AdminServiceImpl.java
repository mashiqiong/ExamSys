package com.examsys.service;

import java.util.List;

import com.examsys.dao.AdminDao;
import com.examsys.po.Admin;
import com.examsys.util.DBUtil;
/**
 * 管理员业务逻辑层实现类
 * @author edu-1
 *
 */
public class AdminServiceImpl extends AbstractBaseService<Admin, Integer> implements AdminService {

	private AdminDao dao;//管理员数据访问层对象
	
	public AdminDao getDao() {
		return dao;
	}

	public void setDao(AdminDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加管理员
	 *  
	 */
	@Override
	public boolean add(Admin obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改管理员
	 */
	@Override
	public boolean update(Admin obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除管理员
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取管理员
	 * @param id 编号
	 */
	@Override
	public Admin get(Integer id) {
		Admin admin=null;
		try {
			admin = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return admin;
	}

	/**
	 * 获得管理员表所有记录
	 */
	@Override
	public List<Admin> getList() {
		List<Admin> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取管理员记录
	 */
	@Override
	public List<Admin> getList(Admin obj) {
		List<Admin> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 *通过用户名去获取管理员 
	 * @param user_name
	 * @return
	 */
	public Admin getAdminByUserName(String user_name){
		Admin admin=null;
		try {
			admin = this.getDao().getAdminByUserName(user_name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return admin;
	}

}
