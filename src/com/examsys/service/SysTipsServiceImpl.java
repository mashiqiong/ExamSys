package com.examsys.service;

import java.util.List;

import com.examsys.dao.SysTipsDao;
import com.examsys.po.SysTips;
import com.examsys.util.DBUtil;
/**
 * 系统提示信息业务逻辑层实现类
 * @author edu-1
 *
 */
public class SysTipsServiceImpl extends AbstractBaseService<SysTips, Integer> implements SysTipsService {

	private SysTipsDao dao;//系统提示信息数据访问层对象
	
	public SysTipsDao getDao() {
		return dao;
	}

	public void setDao(SysTipsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加系统提示信息
	 *  
	 */
	@Override
	public boolean add(SysTips obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改系统提示信息
	 */
	@Override
	public boolean update(SysTips obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除系统提示信息
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取系统提示信息
	 * @param id 编号
	 */
	@Override
	public SysTips get(Integer id) {
		SysTips sysTips=null;
		try {
			sysTips = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sysTips;
	}

	/**
	 * 获得系统提示信息表所有记录
	 */
	@Override
	public List<SysTips> getList() {
		List<SysTips> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取系统提示信息记录
	 */
	@Override
	public List<SysTips> getList(SysTips obj) {
		List<SysTips> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
