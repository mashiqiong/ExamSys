package com.examsys.service;

import java.util.List;

import com.examsys.dao.ExamDetailDao;
import com.examsys.dao.ExamMainDao;
import com.examsys.po.ExamDetail;
import com.examsys.po.ExamMain;
import com.examsys.util.DBUtil;
/**
 * 考生答题卡业务逻辑层实现类
 * @author edu-1
 *
 */
public class ExamMainServiceImpl extends AbstractBaseService<ExamMain, Integer> implements ExamMainService {

	private ExamMainDao dao;//考生答题卡数据访问层对象
	
	private ExamDetailDao examDetailDao;//考生答题卡明细数据访问层对象
	
	public ExamMainDao getDao() {
		return dao;
	}

	public void setDao(ExamMainDao dao) {
		this.dao = dao;
	}

	public ExamDetailDao getExamDetailDao() {
		return examDetailDao;
	}

	public void setExamDetailDao(ExamDetailDao examDetailDao) {
		this.examDetailDao = examDetailDao;
	}

	/**
	 * 添加考生答题卡
	 *  
	 */
	@Override
	public boolean add(ExamMain obj) {
		boolean flag=false;
		try {
			Integer id = this.dao.getSeq();//先获得主编号
			obj.setId(id);
			
			this.getDao().add(obj);//保存主答题卡
			
			List<ExamDetail> examDetails = obj.getExamDetails();
			for(ExamDetail examDetail:examDetails){
				examDetail.setExamMain(obj);//关联主答题卡
			}
			this.getExamDetailDao().add(examDetails);//保存答题卡明细
			
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改考生答题卡
	 */
	@Override
	public boolean update(ExamMain obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			
			List<ExamDetail> examDetails = obj.getExamDetails();
			for(ExamDetail examDetail:examDetails){
				examDetail.setExamMain(obj);//关联主答题卡
				this.getExamDetailDao().update(examDetail);//保存答题卡明细
			}
			
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除考生答题卡
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getExamDetailDao().deleteByExamMainId(id);//先删除答题卡明细
			this.getDao().delete(id);//再删除主答题卡
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取考生答题卡
	 * @param id 编号
	 */
	@Override
	public ExamMain get(Integer id) {
		ExamMain examMain=null;
		try {
			examMain = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return examMain;
	}

	/**
	 * 获得考生答题卡表所有记录
	 */
	@Override
	public List<ExamMain> getList() {
		List<ExamMain> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取考生答题卡记录
	 */
	@Override
	public List<ExamMain> getList(ExamMain obj) {
		List<ExamMain> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
