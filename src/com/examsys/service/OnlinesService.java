package com.examsys.service;

import com.examsys.po.Onlines;

/**
 * 考试者在线考试的时间记录业务逻辑层接口
 * @author edu-1
 *
 */
public interface OnlinesService extends IBaseService<Onlines, Integer> {

}
