package com.examsys.service;

import java.util.List;

import com.examsys.dao.QuestionDbDao;
import com.examsys.po.QuestionDb;
import com.examsys.util.DBUtil;
/**
 * 题库业务逻辑层实现类
 * @author edu-1
 *
 */
public class QuestionDbServiceImpl extends AbstractBaseService<QuestionDb, Integer> implements QuestionDbService {

	private QuestionDbDao dao;//题库数据访问层对象
	
	public QuestionDbDao getDao() {
		return dao;
	}

	public void setDao(QuestionDbDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加题库
	 *  
	 */
	@Override
	public boolean add(QuestionDb obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改题库
	 */
	@Override
	public boolean update(QuestionDb obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除题库
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取题库
	 * @param id 编号
	 */
	@Override
	public QuestionDb get(Integer id) {
		QuestionDb questionDb=null;
		try {
			questionDb = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionDb;
	}

	/**
	 * 获得题库表所有记录
	 */
	@Override
	public List<QuestionDb> getList() {
		List<QuestionDb> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取题库记录
	 */
	@Override
	public List<QuestionDb> getList(QuestionDb obj) {
		List<QuestionDb> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
