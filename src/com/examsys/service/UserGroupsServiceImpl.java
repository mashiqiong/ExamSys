package com.examsys.service;

import java.util.List;

import com.examsys.dao.UserGroupsDao;
import com.examsys.po.UserGroups;
import com.examsys.util.DBUtil;
/**
 * 会员组或考试组业务逻辑层实现类
 * @author edu-1
 *
 */
public class UserGroupsServiceImpl extends AbstractBaseService<UserGroups, Integer> implements UserGroupsService {

	private UserGroupsDao dao;//会员组或考试组数据访问层对象
	
	public UserGroupsDao getDao() {
		return dao;
	}

	public void setDao(UserGroupsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加会员组或考试组
	 *  
	 */
	@Override
	public boolean add(UserGroups obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改会员组或考试组
	 */
	@Override
	public boolean update(UserGroups obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除会员组或考试组
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取会员组或考试组
	 * @param id 编号
	 */
	@Override
	public UserGroups get(Integer id) {
		UserGroups userGroups=null;
		try {
			userGroups = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userGroups;
	}

	/**
	 * 获得会员组或考试组表所有记录
	 */
	@Override
	public List<UserGroups> getList() {
		List<UserGroups> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取会员组或考试组记录
	 */
	@Override
	public List<UserGroups> getList(UserGroups obj) {
		List<UserGroups> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 通过条件获得信息列表，不包括给定编号的用户组
	 * @param notInIds
	 * @return
	 */
	public List<UserGroups> getList(String notInIds){
		List<UserGroups> list=null;
		try {
			list = this.getDao().getList(notInIds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
}
