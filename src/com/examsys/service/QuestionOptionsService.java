package com.examsys.service;

import com.examsys.po.QuestionOptions;

/**
 * 试题选项业务逻辑层接口
 * @author edu-1
 *
 */
public interface QuestionOptionsService extends IBaseService<QuestionOptions, Integer> {

}
