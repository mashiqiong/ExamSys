package com.examsys.service;

import java.util.List;

import com.examsys.dao.UsersDao;
import com.examsys.po.Users;
import com.examsys.util.DBUtil;
/**
 * 会员或或者考试者业务逻辑层实现类
 * @author edu-1
 *
 */
public class UsersServiceImpl extends AbstractBaseService<Users, Integer> implements UsersService {

	private UsersDao dao;//会员或或者考试者数据访问层对象
	
	public UsersDao getDao() {
		return dao;
	}

	public void setDao(UsersDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加会员或或者考试者
	 *  
	 */
	@Override
	public boolean add(Users obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改会员或或者考试者
	 */
	@Override
	public boolean update(Users obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除会员或或者考试者
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取会员或或者考试者
	 * @param id 编号
	 */
	@Override
	public Users get(Integer id) {
		Users users=null;
		try {
			users = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	/**
	 * 获得会员或或者考试者表所有记录
	 */
	@Override
	public List<Users> getList() {
		List<Users> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取会员或或者考试者记录
	 */
	@Override
	public List<Users> getList(Users obj) {
		List<Users> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * 通过用户名去获取考试者
	 * @param userName
	 * @return
	 */
	public Users getUsersByUserName(String userName){
		Users users=null;
		try {
			users = this.getDao().getUsersByUserName(userName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

}
