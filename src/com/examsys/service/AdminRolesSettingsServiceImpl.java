package com.examsys.service;

import java.util.List;

import com.examsys.dao.AdminRolesSettingsDao;
import com.examsys.po.AdminRolesSettings;
import com.examsys.util.DBUtil;
/**
 * 系统功能业务逻辑层实现类
 * @author edu-1
 *
 */
public class AdminRolesSettingsServiceImpl extends AbstractBaseService<AdminRolesSettings, Integer> implements AdminRolesSettingsService {

	private AdminRolesSettingsDao dao;//系统功能数据访问层对象
	
	public AdminRolesSettingsDao getDao() {
		return dao;
	}

	public void setDao(AdminRolesSettingsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加系统功能
	 *  
	 */
	@Override
	public boolean add(AdminRolesSettings obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改系统功能
	 */
	@Override
	public boolean update(AdminRolesSettings obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除系统功能
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取系统功能
	 * @param id 编号
	 */
	@Override
	public AdminRolesSettings get(Integer id) {
		AdminRolesSettings adminRolesSettings=null;
		try {
			adminRolesSettings = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adminRolesSettings;
	}

	/**
	 * 获得系统功能表所有记录
	 */
	@Override
	public List<AdminRolesSettings> getList() {
		List<AdminRolesSettings> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取系统功能记录
	 */
	@Override
	public List<AdminRolesSettings> getList(AdminRolesSettings obj) {
		List<AdminRolesSettings> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
