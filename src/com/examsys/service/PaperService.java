package com.examsys.service;

import java.util.List;

import com.examsys.po.Paper;

/**
 * 试卷业务逻辑层接口
 * @author edu-1
 *
 */
public interface PaperService extends IBaseService<Paper, Integer> {
	/**
	 * 通过编号范围来获得试卷信息列表
	 * @param inIds
	 * @return
	 */
	public List<Paper> getList(String inIds);
}
