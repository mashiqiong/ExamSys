package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperDetailDao;
import com.examsys.po.PaperDetail;
import com.examsys.util.DBUtil;
/**
 * 试卷明细业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperDetailServiceImpl extends AbstractBaseService<PaperDetail, Integer> implements PaperDetailService {

	private PaperDetailDao dao;//数据访问层对象
	
	public PaperDetailDao getDao() {
		return dao;
	}

	public void setDao(PaperDetailDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加试卷明细
	 */
	@Override
	public boolean add(PaperDetail obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 添加多条信息
	 * @param paperDetails
	 * @throws Exception
	 */
	public boolean add(List<PaperDetail> paperDetails){
		boolean flag=false;
		try {
			this.getDao().add(paperDetails);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}
	/**
	 * 修改试卷明细
	 */
	@Override
	public boolean update(PaperDetail obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 通过编号删除试卷明细
	 */
	@Override
	public boolean delete(Integer obj) {
		boolean flag=false;
		try {
			this.getDao().delete(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 通过编号获取试卷明细
	 */
	@Override
	public PaperDetail get(Integer obj) {
		try {
			return this.getDao().get(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取所有试卷明细记录
	 */
	@Override
	public List<PaperDetail> getList() {
		List<PaperDetail> list=null;
		try {
			list= this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 通过条件获取试卷明细
	 */
	@Override
	public List<PaperDetail> getList(PaperDetail obj) {
		List<PaperDetail> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
