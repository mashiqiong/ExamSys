package com.examsys.service;

import java.util.List;

import com.examsys.dao.ConfigDao;
import com.examsys.po.Config;
import com.examsys.util.DBUtil;
/**
 * 系统参数业务逻辑层实现类
 * @author edu-1
 *
 */
public class ConfigServiceImpl extends AbstractBaseService<Config, Integer> implements ConfigService {

	private ConfigDao dao;//系统参数数据访问层对象
	
	public ConfigDao getDao() {
		return dao;
	}

	public void setDao(ConfigDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加系统参数
	 *  
	 */
	@Override
	public boolean add(Config obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改系统参数
	 */
	@Override
	public boolean update(Config obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除系统参数
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取系统参数
	 * @param id 编号
	 */
	@Override
	public Config get(Integer id) {
		Config config=null;
		try {
			config = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config;
	}

	/**
	 * 获得系统参数表所有记录
	 */
	@Override
	public List<Config> getList() {
		List<Config> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取系统参数记录
	 */
	@Override
	public List<Config> getList(Config obj) {
		List<Config> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
