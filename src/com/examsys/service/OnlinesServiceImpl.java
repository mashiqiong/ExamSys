package com.examsys.service;

import java.util.List;

import com.examsys.dao.OnlinesDao;
import com.examsys.po.Onlines;
import com.examsys.util.DBUtil;
/**
 * 考试者在线考试的时间记录业务逻辑层实现类
 * @author edu-1
 *
 */
public class OnlinesServiceImpl extends AbstractBaseService<Onlines, Integer> implements OnlinesService {

	private OnlinesDao dao;//考试者在线考试的时间记录数据访问层对象
	
	public OnlinesDao getDao() {
		return dao;
	}

	public void setDao(OnlinesDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加考试者在线考试的时间记录
	 *  
	 */
	@Override
	public boolean add(Onlines obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改考试者在线考试的时间记录
	 */
	@Override
	public boolean update(Onlines obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除考试者在线考试的时间记录
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取考试者在线考试的时间记录
	 * @param id 编号
	 */
	@Override
	public Onlines get(Integer id) {
		Onlines onlines=null;
		try {
			onlines = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return onlines;
	}

	/**
	 * 获得考试者在线考试的时间记录表所有记录
	 */
	@Override
	public List<Onlines> getList() {
		List<Onlines> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取考试者在线考试的时间记录记录
	 */
	@Override
	public List<Onlines> getList(Onlines obj) {
		List<Onlines> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
