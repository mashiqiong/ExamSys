package com.examsys.service;

import java.util.List;

import com.examsys.dao.QuestionDao;
import com.examsys.dao.QuestionOptionsDao;
import com.examsys.po.Question;
import com.examsys.po.QuestionOptions;
import com.examsys.util.DBUtil;
/**
 * 试题业务逻辑层实现类
 * @author edu-1
 *
 */
public class QuestionServiceImpl extends AbstractBaseService<Question, Integer> implements QuestionService {

	private QuestionDao dao;//试题数据访问层对象
	private QuestionOptionsDao questionOptionsDao;//试题选项数据访问层对象
	
	public QuestionDao getDao() {
		return dao;
	}

	public void setDao(QuestionDao dao) {
		this.dao = dao;
	}

	public QuestionOptionsDao getQuestionOptionsDao() {
		return questionOptionsDao;
	}

	public void setQuestionOptionsDao(QuestionOptionsDao questionOptionsDao) {
		this.questionOptionsDao = questionOptionsDao;
	}

	/**
	 * 添加试题
	 *  
	 */
	@Override
	public boolean add(Question obj) {
		boolean flag=false;
		try {
			Integer id = this.getDao().getSeq();//获得编号
			obj.setId(id);//设置编号
			this.getDao().add(obj);//调用数据访问层对象存试题对象
			
			//拿出视图层传过来的试题选项
			List<QuestionOptions> list = obj.getQuestionOptionss();
			for(QuestionOptions questionOptions:list){
				questionOptions.setQuestion(obj);//关联试题
				this.getQuestionOptionsDao().add(questionOptions);//调用数据访问层对象存试题选项对象
			}
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改试题
	 */
	@Override
	public boolean update(Question obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);//更新试题
			this.getQuestionOptionsDao().deleteByQuestionId(obj.getId());//先删除数据库中试题选项
			//拿出视图层传过来的试题选项
			List<QuestionOptions> list = obj.getQuestionOptionss();
			for(QuestionOptions questionOptions:list){
				//删除完数据中的选之后，再把当前遍历出来的实体类对象存入数据库
				questionOptions.setQuestion(obj);//关联试题
				this.getQuestionOptionsDao().add(questionOptions);//调用数据访问层对象存试题选项对象
			}
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除试题
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			Question question = this.getDao().get(id);//通过试题编号获取试题对象
			
			//创建查询条件
			QuestionOptions questionOptions=new QuestionOptions();
			questionOptions.setQuestion(question);//关联
			
			//获取试题选项
			List<QuestionOptions> list = this.getQuestionOptionsDao().getList(questionOptions);
			for(QuestionOptions qo:list){
				this.getQuestionOptionsDao().delete(qo.getId());//能过试题选项编号删除数据库中的试题选项
			}
			
			this.getDao().delete(id);//删除完试题选项后，再通过试题编号删除数据库中的试题
			
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取试题
	 * @param id 编号
	 */
	@Override
	public Question get(Integer id) {
		Question question=null;
		try {
			question = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return question;
	}

	/**
	 * 获得试题表所有记录
	 */
	@Override
	public List<Question> getList() {
		List<Question> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取试题记录
	 */
	@Override
	public List<Question> getList(Question obj) {
		List<Question> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * 通过条件获得信息列表，不包括给定编号的试题
	 */
	@Override
	public List<Question> getList(String notInIds,Question question) {
		List<Question> list=null;
		try {
			list = this.getDao().getList(notInIds,question);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
