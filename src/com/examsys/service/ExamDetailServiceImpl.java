package com.examsys.service;

import java.util.List;

import com.examsys.dao.ExamDetailDao;
import com.examsys.po.ExamDetail;
import com.examsys.util.DBUtil;
/**
 * 考生答题明细表实体类或者答题卡明细业务逻辑层实现类
 * @author edu-1
 *
 */
public class ExamDetailServiceImpl extends AbstractBaseService<ExamDetail, Integer> implements ExamDetailService {

	private ExamDetailDao dao;//考生答题明细表实体类或者答题卡明细数据访问层对象
	
	public ExamDetailDao getDao() {
		return dao;
	}

	public void setDao(ExamDetailDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加考生答题明细表实体类或者答题卡明细
	 *  
	 */
	@Override
	public boolean add(ExamDetail obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改考生答题明细表实体类或者答题卡明细
	 */
	@Override
	public boolean update(ExamDetail obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除考生答题明细表实体类或者答题卡明细
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取考生答题明细表实体类或者答题卡明细
	 * @param id 编号
	 */
	@Override
	public ExamDetail get(Integer id) {
		ExamDetail examDetail=null;
		try {
			examDetail = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return examDetail;
	}

	/**
	 * 获得考生答题明细表实体类或者答题卡明细表所有记录
	 */
	@Override
	public List<ExamDetail> getList() {
		List<ExamDetail> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取考生答题明细表实体类或者答题卡明细记录
	 */
	@Override
	public List<ExamDetail> getList(ExamDetail obj) {
		List<ExamDetail> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
