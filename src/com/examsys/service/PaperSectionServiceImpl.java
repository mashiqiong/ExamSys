package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperSectionDao;
import com.examsys.po.PaperSection;
import com.examsys.util.DBUtil;
/**
 * 试卷中的章节业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperSectionServiceImpl extends AbstractBaseService<PaperSection, Integer> implements PaperSectionService {

	private PaperSectionDao dao;//试卷中的章节数据访问层对象
	
	public PaperSectionDao getDao() {
		return dao;
	}

	public void setDao(PaperSectionDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加试卷中的章节
	 *  
	 */
	@Override
	public boolean add(PaperSection obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改试卷中的章节
	 */
	@Override
	public boolean update(PaperSection obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除试卷中的章节
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取试卷中的章节
	 * @param id 编号
	 */
	@Override
	public PaperSection get(Integer id) {
		PaperSection paperSection=null;
		try {
			paperSection = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paperSection;
	}

	/**
	 * 获得试卷中的章节表所有记录
	 */
	@Override
	public List<PaperSection> getList() {
		List<PaperSection> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取试卷中的章节记录
	 */
	@Override
	public List<PaperSection> getList(PaperSection obj) {
		List<PaperSection> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
