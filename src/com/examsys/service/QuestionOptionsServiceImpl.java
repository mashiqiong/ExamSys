package com.examsys.service;

import java.util.List;

import com.examsys.dao.QuestionOptionsDao;
import com.examsys.po.QuestionOptions;
import com.examsys.util.DBUtil;
/**
 * 试题选项业务逻辑层实现类
 * @author edu-1
 *
 */
public class QuestionOptionsServiceImpl extends AbstractBaseService<QuestionOptions, Integer> implements QuestionOptionsService {

	private QuestionOptionsDao dao;//试题选项数据访问层对象
	
	public QuestionOptionsDao getDao() {
		return dao;
	}

	public void setDao(QuestionOptionsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加试题选项
	 *  
	 */
	@Override
	public boolean add(QuestionOptions obj) {
		boolean flag=false;
		try {
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改试题选项
	 */
	@Override
	public boolean update(QuestionOptions obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除试题选项
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取试题选项
	 * @param id 编号
	 */
	@Override
	public QuestionOptions get(Integer id) {
		QuestionOptions questionOptions=null;
		try {
			questionOptions = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionOptions;
	}

	/**
	 * 获得试题选项表所有记录
	 */
	@Override
	public List<QuestionOptions> getList() {
		List<QuestionOptions> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取试题选项记录
	 */
	@Override
	public List<QuestionOptions> getList(QuestionOptions obj) {
		List<QuestionOptions> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
