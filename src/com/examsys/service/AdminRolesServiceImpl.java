package com.examsys.service;

import java.util.List;

import com.examsys.dao.AdminRolesDao;
import com.examsys.po.AdminRoles;
import com.examsys.util.DBUtil;
/**
 * 管理员角色业务逻辑层实现类
 * @author edu-1
 *
 */
public class AdminRolesServiceImpl extends AbstractBaseService<AdminRoles, Integer> implements AdminRolesService {

	private AdminRolesDao dao;//管理员角色数据访问层对象
	
	public AdminRolesDao getDao() {
		return dao;
	}

	public void setDao(AdminRolesDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加管理员角色
	 *  
	 */
	@Override
	public boolean add(AdminRoles obj) {
		boolean flag=false;
		try {
			if(obj.getRole_name()==null//角色名称不能为null
					||"".equals(obj.getRole_name())//角色名称不能为空
					||obj.getRole_name().length()>50){//角色名称长度不能大于50个字条
				return false;
			}
			
			if(obj.getRemark().length()>50){//名称长度不能大于50个字条
				return false;
			}
			
			this.getDao().add(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改管理员角色
	 */
	@Override
	public boolean update(AdminRoles obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 删除管理员角色
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getDao().delete(id);
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取管理员角色
	 * @param id 编号
	 */
	@Override
	public AdminRoles get(Integer id) {
		AdminRoles adminRoles=null;
		try {
			adminRoles = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adminRoles;
	}

	/**
	 * 获得管理员角色表所有记录
	 */
	@Override
	public List<AdminRoles> getList() {
		List<AdminRoles> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取管理员角色记录
	 */
	@Override
	public List<AdminRoles> getList(AdminRoles obj) {
		List<AdminRoles> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
