package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperDao;
import com.examsys.dao.PaperDetailDao;
import com.examsys.dao.PaperSectionDao;
import com.examsys.po.Paper;
import com.examsys.po.PaperSection;
import com.examsys.util.DBUtil;
/**
 * 试卷业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperServiceImpl extends AbstractBaseService<Paper, Integer> implements PaperService {

	private PaperDao dao;//试卷数据访问层对象
	private PaperSectionDao paperSectionDao;//试卷章节数据访问层对象
	private PaperDetailDao paperDetailDao;//试卷明细数据访问层对象
	
	public PaperDao getDao() {
		return dao;
	}

	public void setDao(PaperDao dao) {
		this.dao = dao;
	}

	public PaperSectionDao getPaperSectionDao() {
		return paperSectionDao;
	}

	public void setPaperSectionDao(PaperSectionDao paperSectionDao) {
		this.paperSectionDao = paperSectionDao;
	}

	public PaperDetailDao getPaperDetailDao() {
		return paperDetailDao;
	}

	public void setPaperDetailDao(PaperDetailDao paperDetailDao) {
		this.paperDetailDao = paperDetailDao;
	}

	/**
	 * 添加试卷
	 *  
	 */
	@Override
	public boolean add(Paper obj) {
		boolean flag=false;
		try {
			Integer id = this.getDao().getSeq();
			obj.setId(id);
			
			this.getDao().add(obj);//先保存试卷对象
			
			//再保存章节的对象
			List<PaperSection> paperSections = obj.getPaperSections();
			for(PaperSection ps:paperSections){
				ps.setPaper(obj);//关联试卷对象
				this.paperSectionDao.add(ps);//保存章节对象
			}
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 修改试卷
	 */
	@Override
	public boolean update(Paper obj) {
		boolean flag=false;
		try {
			this.getDao().update(obj);
			List<PaperSection> paperSections = obj.getPaperSections();
			for(PaperSection paperSection:paperSections){
				paperSection.setPaper(obj);
				if(paperSection.getId()==null){//如果此章节信息是新添加的
					this.getPaperSectionDao().add(paperSection);
				}else{//如果此章节信息是新旧有的
					this.getPaperSectionDao().update(paperSection);
				}
			}
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 通过试卷编号删除试卷
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) {
		boolean flag=false;
		try {
			this.getPaperDetailDao().deleteByPaperId(id);//先删除试卷中的试题
			this.getPaperSectionDao().deleteByPaperId(id);//再删除试卷章节
			this.getDao().delete(id);//再删除试卷本身
			DBUtil.commit();//提交事务
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback();//回滚事务
		}
		return flag;
	}

	/**
	 * 获取试卷
	 * @param id 编号
	 */
	@Override
	public Paper get(Integer id) {
		Paper paper=null;
		try {
			paper = this.getDao().get(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paper;
	}

	/**
	 * 获得试卷表所有记录
	 */
	@Override
	public List<Paper> getList() {
		List<Paper> list=null;
		try {
			list = this.getDao().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * 带条件获取试卷记录
	 */
	@Override
	public List<Paper> getList(Paper obj) {
		List<Paper> list=null;
		try {
			list = this.getDao().getList(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * 通过编号范围来获得试卷信息列表
	 * @param inIds
	 * @return
	 */
	public List<Paper> getList(String inIds){
		List<Paper> list=null;
		try {
			list = this.getDao().getList(inIds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
