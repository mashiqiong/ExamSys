package com.examsys.service;

import com.examsys.po.AdminRolesSettings;

/**
 * 系统功能业务逻辑层接口
 * @author edu-1
 *
 */
public interface AdminRolesSettingsService extends IBaseService<AdminRolesSettings, Integer> {

}
