package com.examsys.service;

import com.examsys.po.SysTips;

/**
 * 系统提示信息业务逻辑层接口
 * @author edu-1
 *
 */
public interface SysTipsService extends IBaseService<SysTips, Integer> {

}
