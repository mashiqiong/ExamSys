package com.examsys.view;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
/**
 * 考试者顶部面板
 * @author mashiqiong
 *
 */
public class UserTopPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public UserTopPanel() {
		this.setBounds(0, 0, 515, 80);
		JLabel label = new JLabel("欢迎使用考试系统");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label, GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
					.addGap(5))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label, GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
					.addGap(3))
		);
		setLayout(groupLayout);

	}

}
