package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.Paper;
import com.examsys.po.PaperSection;
import com.examsys.service.Main;
import com.examsys.service.PaperService;
/**
 * 添加试卷窗口
 * @author edu-1
 *
 */
public class AddPaperDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField paperNameTxf;//试卷名称输入框
	private JTextField startHourTxf;//开始考试时间的小时输入框
	private JTextField startMinuteTxf;//开始考试时间的分钟输入框
	private JTextField endHourTxf;//结束考试时间的小时输入框
	private JTextField endMinuteTxf;//结束考试时间的分钟输入框
	private JTextField paperMinuteTxf;//考试总分钟输入框
	private JTextField totalScoreTxf;//考试总分数输入框
	private JComboBox qorderCbb;//试题排序输入框
	private JTextField remarkTxf;//备注输入框
	private JComboBox statusCbb;//状态下拉框
	private JPanel startTimePanel;//开始考试日期选择面板
	private JPanel endTimePanel;//结束考试日期选择面板
	private JPanel postDatePanel;//提交试卷日期选择面板
	private JPanel showScorePanel;//公布考试成绩日期选择面板
	private DateChooser startTimeDcr;
	private DateChooser endTimeDcr;
	private DateChooser postDateDcr;
	private DateChooser showScoreDcr;
	private JPanel panel;//章节面板
	/**
	 * Create the dialog.
	 */
	public AddPaperDialog() {
		setModal(true);
		setTitle("添加试卷");
		setBounds(100, 100, 605, 601);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("试卷名称：");
		
		paperNameTxf = new JTextField();
		paperNameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("开始考试日期：");
		
		startTimePanel = new JPanel();//开始考试日期面板
		
		JLabel label_2 = new JLabel("时");
		
		startHourTxf = new JTextField();
		startHourTxf.setColumns(10);
		
		startMinuteTxf = new JTextField();
		startMinuteTxf.setColumns(10);
		
		JLabel label_3 = new JLabel("分");
		
		JLabel label_4 = new JLabel("结束考试日期：");
		
		endTimePanel = new JPanel();//结束考试日期面板
		
		endHourTxf = new JTextField();
		endHourTxf.setColumns(10);
		
		JLabel label_5 = new JLabel("时");
		
		endMinuteTxf = new JTextField();
		endMinuteTxf.setColumns(10);
		
		JLabel label_6 = new JLabel("分");
		
		JLabel label_7 = new JLabel("考试总分钟：");
		
		paperMinuteTxf = new JTextField();
		paperMinuteTxf.setColumns(10);
		
		JLabel label_8 = new JLabel("分钟");
		
		JLabel label_9 = new JLabel("试卷总分数：");
		
		totalScoreTxf = new JTextField();
		totalScoreTxf.setColumns(10);
		
		JLabel label_10 = new JLabel("提交时间：");
		
		postDatePanel = new JPanel();//提交时间面板
		
		JLabel lblNewLabel = new JLabel("公布成绩时间：");
		
		showScorePanel = new JPanel();//公布成绩时间面板
		
		JLabel label_11 = new JLabel("题目顺序：");
		
		qorderCbb = new JComboBox();
		qorderCbb.setModel(new DefaultComboBoxModel(new String[] {"升序", "降序", "随机"}));
		
		JLabel label_12 = new JLabel("试卷状态：");
		
		statusCbb = new JComboBox();
		statusCbb.setModel(new DefaultComboBoxModel(new String[] {"开放", "不开放"}));
		
		JLabel lblNewLabel_1 = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		
		JButton addBtn = new JButton("添加章节");
		ActionListener addActionListener =new AddActionListener();
		addBtn.addActionListener(addActionListener);
		
		JButton deleteBtn = new JButton("删除章节");
		ActionListener deleteActionListener =new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_1)
								.addComponent(label))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(startTimePanel, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(startHourTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_2)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(startMinuteTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(paperNameTxf))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(label_3))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(label_4)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(endTimePanel, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(label_7)
										.addComponent(label_10))
									.addGap(18)
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
										.addComponent(postDatePanel, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
										.addGroup(gl_contentPanel.createSequentialGroup()
											.addComponent(paperMinuteTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(label_8))
										.addComponent(qorderCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(remarkTxf)
										.addComponent(addBtn)))
								.addComponent(label_11))
							.addGap(18)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(deleteBtn)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(label_9)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(totalScoreTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(endHourTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_5)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(endMinuteTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(label_6))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel)
										.addComponent(label_12))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(showScorePanel, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)))))
						.addComponent(lblNewLabel_1))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(paperNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_1)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2)
								.addComponent(startHourTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(startMinuteTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_3))
							.addComponent(startTimePanel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(label_4)
						.addComponent(endTimePanel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(endHourTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(label_5)
							.addComponent(endMinuteTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(label_6)))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_7)
						.addComponent(paperMinuteTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_8)
						.addComponent(label_9)
						.addComponent(totalScoreTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_10)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
							.addComponent(postDatePanel, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(showScorePanel, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel))))
					.addGap(25)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_11)
						.addComponent(qorderCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_12)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(addBtn)
						.addComponent(deleteBtn))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		GroupLayout gl_showScorePanel = new GroupLayout(showScorePanel);
		gl_showScorePanel.setHorizontalGroup(
			gl_showScorePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 96, Short.MAX_VALUE)
		);
		gl_showScorePanel.setVerticalGroup(
			gl_showScorePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 19, Short.MAX_VALUE)
		);
		showScorePanel.setLayout(gl_showScorePanel);
		GroupLayout gl_postDatePanel = new GroupLayout(postDatePanel);
		gl_postDatePanel.setHorizontalGroup(
			gl_postDatePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 114, Short.MAX_VALUE)
		);
		gl_postDatePanel.setVerticalGroup(
			gl_postDatePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 18, Short.MAX_VALUE)
		);
		postDatePanel.setLayout(gl_postDatePanel);
		GroupLayout gl_endTimePanel = new GroupLayout(endTimePanel);
		gl_endTimePanel.setHorizontalGroup(
			gl_endTimePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 119, Short.MAX_VALUE)
		);
		gl_endTimePanel.setVerticalGroup(
			gl_endTimePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 20, Short.MAX_VALUE)
		);
		endTimePanel.setLayout(gl_endTimePanel);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		GroupLayout gl_startTimePanel = new GroupLayout(startTimePanel);
		gl_startTimePanel.setHorizontalGroup(
			gl_startTimePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 118, Short.MAX_VALUE)
		);
		gl_startTimePanel.setVerticalGroup(
			gl_startTimePanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 20, Short.MAX_VALUE)
		);
		startTimePanel.setLayout(gl_startTimePanel);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		
		ActionListener saveActionListener=new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);
		
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddPaperDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		buttonPane.add(cancelBtn);
		
		initStartTimePanel();//开始考试时间
		initEndTimePanel();//结束考试时间
		initPostDatePanel();//提交考试时间
		initShowScorePanel();//公布考试时间
	}
	
	/**
	 * 添加章节按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			PaperSectionPanel paperSectionPanel=new PaperSectionPanel();
			panel.add(paperSectionPanel);
			panel.updateUI();//刷新界面
		}
	}
	
	/**
	 * 删除章节按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Component[] components = panel.getComponents();//获取面板中的组件
			for(int i=0;i<components.length;i++){
				PaperSectionPanel paperSectionPanel =(PaperSectionPanel)components[i];
				boolean selected = paperSectionPanel.getDeleteCkb().isSelected();
				if(selected){//判断删除复选是否被选中
					panel.remove(paperSectionPanel);//删除此章节
				}
			}
			
			panel.updateUI();//刷新界面
		}
		
	}
	/**
	 * 开始考试时间
	 */
	private void initStartTimePanel(){
		//添加日期控件到面板中
		Date date=new Date();
		startTimeDcr = new DateChooser(date,"yyyy-MM-dd");
		GroupLayout layout = (GroupLayout)startTimePanel.getLayout();
		
		SequentialGroup hGroup = layout.createSequentialGroup();//水平分组
		SequentialGroup vGroup = layout.createSequentialGroup();//垂直分组
		
		hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.startTimeDcr));//将日期控件添加到布局中
		vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.startTimeDcr));//将日期控件添加到布局中
		
		layout.setVerticalGroup(vGroup);//水平
		layout.setHorizontalGroup(hGroup);//垂直
		
	}
	
	/**
	 * 结束考试时间
	 */
	private void initEndTimePanel(){
		//添加日期控件到面板中
		Date date=new Date();
		endTimeDcr = new DateChooser(date,"yyyy-MM-dd");
		GroupLayout layout = (GroupLayout)endTimePanel.getLayout();
		
		SequentialGroup hGroup = layout.createSequentialGroup();//水平分组
		SequentialGroup vGroup = layout.createSequentialGroup();//垂直分组
		
		hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.endTimeDcr));//将日期控件添加到布局中
		vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.endTimeDcr));//将日期控件添加到布局中
		
		layout.setVerticalGroup(vGroup);//水平
		layout.setHorizontalGroup(hGroup);//垂直
		
	}
	
	/**
	 * 提交时间
	 */
	private void initPostDatePanel(){
		//添加日期控件到面板中
		Date date=new Date();
		postDateDcr = new DateChooser(date,"yyyy-MM-dd");
		GroupLayout layout = (GroupLayout)postDatePanel.getLayout();
		
		SequentialGroup hGroup = layout.createSequentialGroup();//水平分组
		SequentialGroup vGroup = layout.createSequentialGroup();//垂直分组
		
		hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.postDateDcr));//将日期控件添加到布局中
		vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.postDateDcr));//将日期控件添加到布局中
		
		layout.setVerticalGroup(vGroup);//水平
		layout.setHorizontalGroup(hGroup);//垂直
		
	}
	
	/**
	 * 公布成绩时间
	 */
	private void initShowScorePanel(){
		//添加日期控件到面板中
		Date date=new Date();
		showScoreDcr = new DateChooser(date,"yyyy-MM-dd");
		GroupLayout layout = (GroupLayout)showScorePanel.getLayout();
		
		SequentialGroup hGroup = layout.createSequentialGroup();//水平分组
		SequentialGroup vGroup = layout.createSequentialGroup();//垂直分组
		
		hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.showScoreDcr));//将日期控件添加到布局中
		vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.showScoreDcr));//将日期控件添加到布局中
		
		layout.setVerticalGroup(vGroup);//水平
		layout.setHorizontalGroup(hGroup);//垂直
		
	}
	
	/**
	 * 保存按钮监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				//创建信息提示框对象
				MessageDialog messageDialog=new MessageDialog();
				
				SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");//日期格式化对象
				SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//日期格式化对象
				
				String paperName = paperNameTxf.getText();//试卷名称
				if(paperName==null||paperName.equals("")){
					messageDialog.setMsg("试卷名称不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				Date startDate = startTimeDcr.getDate();//开始时间
				String startHour = startHourTxf.getText();//开始时间的小时部分
				if(startHour==null||startHour.equals("")){
					messageDialog.setMsg("开始时间的小时部分不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				String startMinute=startMinuteTxf.getText();//开始时间的分钟部分
				if(startMinute==null||startMinute.equals("")){
					messageDialog.setMsg("开始时间的分钟部分不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				String startTimeStr=sdf1.format(startDate);//将日期对象格式化为字符串对象
				Date startTimeDate = sdf2.parse(startTimeStr+" "+startHour+":"+startMinute+":00");
				Timestamp startTime=new Timestamp(startTimeDate.getTime());//开始时间
				
				Date endDate = endTimeDcr.getDate();//结束时间
				String endHour = endHourTxf.getText();//结束时间的小时部分
				if(endHour==null||endHour.equals("")){
					messageDialog.setMsg("结束时间的小时部分不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				String endMinute = endMinuteTxf.getText();//结束时间的分钟部分
				if(endMinute==null||endMinute.equals("")){
					messageDialog.setMsg("结束时间的分钟部分不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				String endTimeStr=sdf1.format(endDate);//将日期对象格式化为字符串对象
				Date endTimeDate = sdf2.parse(endTimeStr+" "+endHour+":"+endMinute+":00");
				Timestamp endTime=new Timestamp(endTimeDate.getTime());//结束时间
				
				Date postDate = postDateDcr.getDate();//提交时间
				Timestamp postTime=new Timestamp(postDate.getTime());
				
				Date showDate = showScoreDcr.getDate();//公布时间
				Timestamp showScore=new Timestamp(showDate.getTime());
				
				String paperMinute = paperMinuteTxf.getText();//考试总分钟
				if(paperMinute==null||paperMinute.equals("")){
					messageDialog.setMsg("考试总分钟不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				String totalScore = totalScoreTxf.getText();//试卷总分数
				if(totalScore==null||totalScore.equals("")){
					messageDialog.setMsg("考试总分数不能为空");
					messageDialog.setVisible(true);
					return;
				}
				
				String status=statusCbb.getSelectedItem().equals("开放")?"1":"-1";//试卷状态
				String qorder = (String)qorderCbb.getSelectedItem();//试题排序号
				if(qorder.equals("升序")){
					qorder="0";
				}else if(qorder.equals("降序")){
					qorder="1";
				}else{//随机
					qorder="2";
				}
				
				String remark = remarkTxf.getText();//备注
				
				//创建试卷实体类对象
				Paper paper=new Paper();
				paper.setAdmin(Main.admin);//关联管理员
				paper.setPaper_name(paperName);//试卷名称
				paper.setStart_time(startTime);//开始时间
				paper.setEnd_time(endTime);//结束时间
				paper.setPaper_minute(Integer.valueOf(paperMinute));//考试总分钟
				paper.setTotal_score(Integer.valueOf(totalScore));//试卷总分数
				paper.setPost_date(postTime);//提交时间
				paper.setShow_score(showScore);//公布时间
				paper.setQorder(Integer.valueOf(qorder));//试题排序号
				paper.setStatus(status);//试卷状态
				paper.setRemark(remark);//备注
				
				//处理章节
				List<PaperSection> paperSections=new ArrayList<PaperSection>();
				Component[] components = panel.getComponents();
				for(int i=0;i<components.length;i++){
					//从章节面板中拿出数据
					PaperSectionPanel paperSectionPanel=(PaperSectionPanel) components[i];
					String sectionName = paperSectionPanel.getSectionNameTxf().getText();//章节名称
					String perScore = paperSectionPanel.getPerScoreTxf().getText();//章节分数
					String sectionRemark = paperSectionPanel.getRemarkTxf().getText();//备注
					
					//创建章节实体类对象
					PaperSection paperSection=new PaperSection();
					paperSection.setSection_name(sectionName);//章节名称
					paperSection.setPer_score(Integer.valueOf(perScore));//章节分数
					paperSection.setRemark(sectionRemark);//备注
					
					paperSections.add(paperSection);//把章节对象添加到集合中
				}
				
				paper.setPaperSections(paperSections);//把章节对象集合添加到试卷中
				//从容器中获取业务逻辑层对象
				PaperService paperService=Main.lookUp("paperService");
				boolean flag = paperService.add(paper);//调用业务逻辑层对象的方法添加数据
				
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					AddPaperDialog.this.dispose();//销毁窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

}
