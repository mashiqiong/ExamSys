package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.QuestionDb;
import com.examsys.service.Main;
import com.examsys.service.QuestionDbService;
/**
 * 题库管理面板
 * @author edu-1
 *
 */
public class QuestionDbManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public QuestionDbManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("题库名称：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建查询按钮监听器对象
		ActionListener queryActionListener=new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);//给查询按钮绑定监听器对象
		
		JButton updateBtn = new JButton("修改");
		//创建修改按钮监听器对象
		ActionListener updateActionListener=new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//给修改按钮绑定监听器对象
				
		JButton deleteBtn = new JButton("删除");
		//创建删除按钮监听器对象
		ActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给删除按钮绑定监听器对象
				
		JButton addBtn = new JButton("添加");
		//创建添加按钮监听器对象
		ActionListener addActionListener=new AddActionListener();
		addBtn.addActionListener(addActionListener);//给添加按钮绑定监听器对象
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn))
					.addGap(25))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//设置自动创建排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		
		initDatas();//调用初始化表格数据的方法

	}
	
	/**
	 * 添加按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			//创建添加题库对话框对象
			AddQuestionDbDialog addQuestionDbDialog=new AddQuestionDbDialog();
			addQuestionDbDialog.setVisible(true);
			initDatas();//刷新表格数据
		}
	}
	
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					QuestionDbService questionDbService=Main.lookUp("questionDbService");
					QuestionDb questionDb = questionDbService.get(id);//通过编号找到对象
					
					//创建修改对话框对象
					UpdateQuestionDbDialog updateConfigDialog=new UpdateQuestionDbDialog();
					updateConfigDialog.setOldQuestionDb(questionDb);//把实体类对象传给对话框用于显示与修改
					updateConfigDialog.setVisible(true);
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					QuestionDbService questionDbService=Main.lookUp("questionDbService");
					boolean flag = questionDbService.delete(id);//通过 编号删除对象
					//创建信息提示框
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
					
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	/**
	 * 查询按钮监听器类
	 * @author edu-1
	 *
	 */
	private class QueryActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			Object[][] datas = new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			};
			
			String[] columnNames = new String[] {
				"编号", "管理员", "题库名称", "创建时间", "状态", "备注"
			};
			
			//创建查询条件实类体对象
			String name = keywordTxf.getText();
			QuestionDb questionDb1=new QuestionDb();
			questionDb1.setName(name);
			
			try {
				//从容器中获取业务逻辑层对象
				QuestionDbService questionDbService=Main.lookUp("questionDbService");
				List<QuestionDb> list = questionDbService.getList(questionDb1);//调用业务逻辑层对象的方法 获取数据
				
				if(list.size()>0){//如果拿到数据
					datas = new Object[list.size()][6];
					for(int i=0;i<list.size();i++){
						QuestionDb questionDb = list.get(i);//从集合中拿出实体类对象
						
						datas[i][0]=questionDb.getId();//编号
						datas[i][1]=questionDb.getAdmin().getUser_name();//管理员
						datas[i][2]=questionDb.getName();//题库名称
						datas[i][3]=questionDb.getCreate_date();//创建时间
						datas[i][4]=questionDb.getStatus().equals("1")?"正常":"锁定";//状态
						datas[i][5]=questionDb.getRemark();//备注
					}
				}
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
	}
	
	/*
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "管理员", "题库名称", "创建时间", "状态", "备注"
		};
		
		try {
			//从容器中获取业务逻辑层对象
			QuestionDbService questionDbService=Main.lookUp("questionDbService");
			List<QuestionDb> list = questionDbService.getList();//调用业务逻辑层对象的方法 获取数据
			
			if(list.size()>0){//如果拿到数据
				datas = new Object[list.size()][6];
				for(int i=0;i<list.size();i++){
					QuestionDb questionDb = list.get(i);//从集合中拿出实体类对象
					
					datas[i][0]=questionDb.getId();//编号
					datas[i][1]=questionDb.getAdmin().getUser_name();//管理员
					datas[i][2]=questionDb.getName();//题库名称
					datas[i][3]=questionDb.getCreate_date();//创建时间
					datas[i][4]=questionDb.getStatus().equals("1")?"正常":"锁定";//状态
					datas[i][5]=questionDb.getRemark();//备注
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));
	}

}
