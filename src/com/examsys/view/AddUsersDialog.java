package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.UserGroups;
import com.examsys.po.Users;
import com.examsys.service.Main;
import com.examsys.service.UserGroupsService;
import com.examsys.service.UsersService;
/**
 * 添加会员窗口
 * @author edu-1
 *
 */
public class AddUsersDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField userNameTxf;//会员名输入框
	private JPasswordField userPassTxf;//密码框
	private JTextField userNoTxf;//学号输入框
	private JTextField realNameTxf;//真实姓名输入 框
	private JTextField emailTxf;//邮箱输入 框
	private JTextField phoneTxf;//电话号码输入 框
	private JTextField remarkTxf;//备注
	private JComboBox userGroupCbb;//会员组下拉框
	private JComboBox statusCbb;//状态下拉框
	/**
	 * Create the dialog.
	 */
	public AddUsersDialog() {
		setModal(true);
		setTitle("会员添加");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("会员名：");
		
		userNameTxf = new JTextField();
		userNameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("所属组：");
		
		userGroupCbb = new JComboBox();
		
		JLabel label_2 = new JLabel("密码：");
		
		userPassTxf = new JPasswordField();
		userPassTxf.setColumns(10);
		
		JLabel label_3 = new JLabel("学号：");
		
		userNoTxf = new JTextField();
		userNoTxf.setColumns(10);
		
		JLabel label_4 = new JLabel("真实姓名：");
		
		realNameTxf = new JTextField();
		realNameTxf.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("邮箱：");
		
		emailTxf = new JTextField();
		emailTxf.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("手机号码：");
		
		phoneTxf = new JTextField();
		phoneTxf.setColumns(10);
		
		JLabel label_5 = new JLabel("状态：");
		
		statusCbb = new JComboBox();
		statusCbb.setModel(new DefaultComboBoxModel(new String[] {"启用", "禁用"}));
		
		JLabel label_6 = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label)
							.addGap(18)
							.addComponent(userNameTxf, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_2)
								.addComponent(label_4))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(realNameTxf, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
								.addComponent(userPassTxf, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_1)
								.addComponent(label_6))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(remarkTxf, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
								.addComponent(phoneTxf))))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_contentPanel.createSequentialGroup()
								.addComponent(lblNewLabel)
								.addGap(18)
								.addComponent(emailTxf))
							.addGroup(gl_contentPanel.createSequentialGroup()
								.addComponent(label_1)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(userGroupCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_contentPanel.createSequentialGroup()
								.addComponent(label_3)
								.addGap(18)
								.addComponent(userNoTxf, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_5)
							.addGap(18)
							.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(63, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(label_1)
						.addComponent(userGroupCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(userNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(userPassTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3)
						.addComponent(userNoTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_4)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(realNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblNewLabel)
							.addComponent(emailTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(21)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(phoneTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_6)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(35, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建监听器对象
		ActionListener saveActionListener = new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给保存按钮绑定监听器对象
		saveBtn.setActionCommand("OK");
		
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddUsersDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		buttonPane.add(cancelBtn);
		
		initUserGroupCbb();//调用初始化用户组下拉框选项的方法 
	}
	
	/**
	 * 保存按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userName = userNameTxf.getText();//会员名
			SelectItem selectedItem =(SelectItem) userGroupCbb.getSelectedItem();
			Integer groupId = selectedItem.getId();//组编号
			String userPass=new String(userPassTxf.getPassword());//密码
			String userNo = userNoTxf.getText();//学号
			String realName = realNameTxf.getText();//真实姓名
			String email = emailTxf.getText();//邮箱
			String phone = phoneTxf.getText();//电话号码
			String status = (String)statusCbb.getSelectedItem();//状态
			String remark = remarkTxf.getText();//备注
			//创建提示信息框对象
			MessageDialog messageDialog=new MessageDialog();
			//判断会员名
			if(userName==null||"".equals(userName)){
				messageDialog.setMsg("会员名不能空");
				messageDialog.setVisible(true);
				return;
			}
			
			//判断密码
			if(userPass==null||"".equals(userPass)){
				messageDialog.setMsg("密码不能空");
				messageDialog.setVisible(true);
				return;
			}
			
			//创建实体类对象 
			Users users=new Users();
			users.setUser_name(userName);//会员名
			users.setUser_pass(userPass);//密码
			users.setUser_no(userNo);//学号
			users.setReal_name(realName);//真实姓名
			users.setEmail(email);//邮箱
			users.setPhone(phone);//电话号码
			users.setRemark(remark);//备注
			
			long currentTimeMillis = System.currentTimeMillis();//当前系统时间毫秒数
			Date create_date = new Date(currentTimeMillis);//创建日期对象
			users.setCreate_date(create_date);//注册日期
			users.setLogin_date(create_date);//登录日期
			users.setLogin_times(0);//登录次数
//			users.setStatus("启用".equals(status)?"1":"0");
			if("启用".equals(status)){
				users.setStatus("1");
			}else{
				users.setStatus("0");
			}
			
			//创建会员组对象
			UserGroups userGroups=new UserGroups();
			userGroups.setId(groupId);
			
			users.setUserGroups(userGroups);//将会员与用户组关联
			
			try {
				//从容器中获取业务逻辑层对象
				UsersService usersService=Main.lookUp("usersService");
				boolean flag = usersService.add(users);//调用业务逻辑层对象的添加方法保存数据
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					AddUsersDialog.this.dispose();//销毁窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * 初始化用户组下拉框选项的方法 
	 */
	private void initUserGroupCbb(){
		try {
			//从容器中获取业务逻辑层对象
			UserGroupsService userGroupsService=Main.lookUp("userGroupsService");
			List<UserGroups> list = userGroupsService.getList();//调用业务逻辑层对象的方法获取数据对象
			
			for(int i=0;i<list.size();i++){
				UserGroups userGroups = list.get(i);//从集合中获取实体类对象
				//构建下拉选项对象
				SelectItem selectItem=new SelectItem();
				selectItem.setId(userGroups.getId());
				selectItem.setName(userGroups.getGroup_name());
				userGroupCbb.addItem(selectItem);//往下拉框中添加选项
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
