package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.SysTips;
import com.examsys.service.Main;
import com.examsys.service.SysTipsService;
/**
 * 系统提示信息管理面板
 * @author edu-1
 *
 */
public class SysTipsManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public SysTipsManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("代码：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建查询按钮监听器对象
		ActionListener queryActionListener=new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);//给查询按钮绑定监听器对象
		
		JButton updateBtn = new JButton("修改");
		//创建修改按钮监听器对象
		ActionListener updateActionListener=new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//给修改按钮绑定监听器对象
				
		JButton deleteBtn = new JButton("删除");
		//创建删除按钮监听器对象
		ActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给删除按钮绑定监听器对象
				
		JButton addBtn = new JButton("添加");
		//创建添加按钮监听器对象
		ActionListener addActionListener=new AddActionListener();
		addBtn.addActionListener(addActionListener);//给添加按钮绑定监听器对象
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn)
						.addComponent(addBtn))
					.addGap(25))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//设置自动生成排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		
		initDatas();//调用初始化表格数据的方法

	}
	
	/**
	 * 添加按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//创建添加系统提示信息对话框对象
			AddSysTipsDialog addSysTipsDialog=new AddSysTipsDialog();
			addSysTipsDialog.setVisible(true);
			initDatas();//刷新表格数据
			
		}
	};
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {

			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					SysTipsService sysTipsService=Main.lookUp("sysTipsService");
					SysTips sysTips = sysTipsService.get(id);//通过编号找到对象
					
					//创建修改对话框对象
					UpdateSysTipsDialog updateConfigDialog=new UpdateSysTipsDialog();
					updateConfigDialog.setOldSysTips(sysTips);//把实体类对象传给对话框用于显示与修改
					updateConfigDialog.setVisible(true);
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
			
		}
	}
	
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					SysTipsService sysTipsService=Main.lookUp("sysTipsService");
					boolean flag = sysTipsService.delete(id);//通过 编号删除对象
					//创建信息提示框
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
					
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	/**
	 * 查询按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class QueryActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			Object[][] datas = new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
			};
			
			String[] columnNames = new String[] {
				"编号", "代码", "信息内容"
			};
			
			//构造查询条件对象
			String scode = keywordTxf.getText();
			SysTips sysTips1=new SysTips();
			sysTips1.setScode(scode);
			
			try {
				//从容器中获取业务逻辑层对象
				SysTipsService sysTipsService=Main.lookUp("sysTipsService");
				List<SysTips> list = sysTipsService.getList(sysTips1);//调用业务逻辑层对象的方法拿数据
				if(list.size()>0){
					datas = new Object[list.size()][3];
					for(int i=0;i<list.size();i++){
						SysTips sysTips = list.get(i);
						datas[i][0]=sysTips.getId();
						datas[i][1]=sysTips.getScode();
						datas[i][2]=sysTips.getSdesc();
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
	}
	
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		Object[][] datas = new Object[][] {
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "代码", "信息内容"
		};
		
		try {
			//从容器中获取业务逻辑层对象
			SysTipsService sysTipsService=Main.lookUp("sysTipsService");
			List<SysTips> list = sysTipsService.getList();//调用业务逻辑层对象的方法拿数据
			if(list.size()>0){
				datas = new Object[list.size()][3];
				for(int i=0;i<list.size();i++){
					SysTips sysTips = list.get(i);
					datas[i][0]=sysTips.getId();
					datas[i][1]=sysTips.getScode();
					datas[i][2]=sysTips.getSdesc();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));
	}

}
