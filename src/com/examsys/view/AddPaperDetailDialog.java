package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.Paper;
import com.examsys.po.PaperDetail;
import com.examsys.po.PaperSection;
import com.examsys.po.Question;
import com.examsys.po.QuestionDb;
import com.examsys.service.Main;
import com.examsys.service.PaperDetailService;
import com.examsys.service.QuestionDbService;
import com.examsys.service.QuestionService;
/**
 * 添加试卷题目的窗口
 * @author mashiqiong
 *
 */
public class AddPaperDetailDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable paperDetailTable;
	private JTable questionTable;
	private JComboBox paperSectionCbb;//章节下拉框
	private JComboBox questionDbCbb;//题库下拉框
	private JComboBox qLevelCbb;//难易度下拉框
	private JComboBox qTypeCbb;//试题类型下拉框
	private Paper paper;//试卷实体类对象
	private String notInIds="";//试题编号
	/**
	 * 外部程序调用，传入一个试卷实体类对象
	 * @param paper
	 */
	public void setPaper(Paper paper) {
		this.paper=paper;
		List<PaperSection> paperSections = paper.getPaperSections();
		for(PaperSection paperSection:paperSections){
			//创建章节项对象
			SelectItem selectItem=new SelectItem();
			selectItem.setId(paperSection.getId());
			selectItem.setName(paperSection.getSection_name());
			
			paperSectionCbb.addItem(selectItem);//给章节下拉框添加项
		}
		
		paperSectionCbb.setSelectedItem(0);
		try {
			//从容器里获得业务逻辑层对象
			QuestionDbService questionDbService=Main.lookUp("questionDbService");
			List<QuestionDb> questionDbList = questionDbService.getList();//通过编号获得对象
			System.out.println("-----------------------------------"+questionDbList.size());
			for(QuestionDb questionDb:questionDbList){
				//创建章节项对象
				SelectItem selectItem=new SelectItem();
				selectItem.setId(questionDb.getId());
				selectItem.setName(questionDb.getName());
				
				questionDbCbb.addItem(selectItem);//给题库下拉框添加项
			}
			questionDbCbb.setSelectedItem(0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		initQTypeCbb();//初始类型下拉框
		initQLevelCbb();//初始难度下拉框
		initPaperDetailTableDatas(null);//调用初始试卷题目表格的方法
		initQuestionTableDatas();//调用初始试题表格的方法
	}

	/**
	 * 初始类型下拉框的方法
	 */
	private void initQTypeCbb(){
		//从容器中获取业务逻辑层
		for(int i=1;i<6;i++){
			SelectItem selectItem=new SelectItem();
			selectItem.setId(i);
			switch(i){
				case 1:
					selectItem.setName("单选");
					break;
				case 2:
					selectItem.setName("多选");
					break;
				case 3:
					selectItem.setName("填空");
					break;
				case 4:
					selectItem.setName("判断");
					break;
				case 5:
					selectItem.setName("问答");
					break;
			}
			
			qTypeCbb.addItem(selectItem);
		}
		qTypeCbb.setSelectedItem(0);
			
	}
	
	/**
	 * 初始难度下拉框的方法
	 */
	private void initQLevelCbb(){
		
		//从容器中获取业务逻辑层
		for(int i=1;i<4;i++){
			SelectItem selectItem=new SelectItem();
			selectItem.setId(i);
			switch(i){
				case 1:
					selectItem.setName("易");
					break;
				case 2:
					selectItem.setName("正常");
					break;
				case 3:
					selectItem.setName("难");
					break;
			}
			
			qLevelCbb.addItem(selectItem);
		}
		qLevelCbb.setSelectedItem(0);	
	}
	
	/**
	 * Create the dialog.
	 */
	public AddPaperDetailDialog() {
		setModal(true);
		setTitle("添加试卷题目");
		setBounds(100, 100, 929, 480);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JScrollPane paperDetailScrollPane = new JScrollPane();
		
		JScrollPane questionScrollPane = new JScrollPane();
		JPanel buttonPane = new JPanel();
		
		JLabel label = new JLabel("章节：");
		
		paperSectionCbb = new JComboBox();
		ItemListener PaperSectionCbbItemListener=new PaperSectionCbbItemListener();
		paperSectionCbb.addItemListener(PaperSectionCbbItemListener);
		
		JLabel label_1 = new JLabel("题库：");
		
		questionDbCbb = new JComboBox();
		ItemListener questionDbCbbItemListener=new QuestionDbCbbItemListener();
		questionDbCbb.addItemListener(questionDbCbbItemListener);
		
		JButton addBtn = new JButton("添加");
		ActionListener addActionListener=new AddActionListener();
		addBtn.addActionListener(addActionListener);
		
		addBtn.setActionCommand("OK");
		getRootPane().setDefaultButton(addBtn);
		
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddPaperDetailDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		
		JLabel label_2 = new JLabel("试题类型：");
		
		qTypeCbb = new JComboBox();
		ItemListener qTypeCbbItemListener=new QTypeCbbItemListener();
		qTypeCbb.addItemListener(qTypeCbbItemListener);
		
		JLabel label_3 = new JLabel("难易级别：");
		
		qLevelCbb = new JComboBox();
		ItemListener qLevelCbbItemListener=new QLevelCbbItemListener();
		qLevelCbb.addItemListener(qLevelCbbItemListener);
		
		JButton deleteBtn = new JButton("删除");
		ActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);
		
		GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
		gl_buttonPane.setHorizontalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addGap(213)
					.addComponent(label)
					.addGap(5)
					.addComponent(paperSectionCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(label_1)
					.addGap(5)
					.addComponent(questionDbCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(qTypeCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(qLevelCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(addBtn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(deleteBtn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(cancelBtn)
					.addContainerGap(214, Short.MAX_VALUE))
		);
		gl_buttonPane.setVerticalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_buttonPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(4)
							.addComponent(label))
						.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(1)
							.addComponent(paperSectionCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(4)
							.addComponent(label_1))
						.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(1)
							.addComponent(questionDbCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_buttonPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(addBtn)
							.addComponent(label_2)
							.addComponent(qTypeCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(label_3)
							.addComponent(qLevelCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_buttonPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(cancelBtn)
							.addComponent(deleteBtn)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		buttonPane.setLayout(gl_buttonPane);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(paperDetailScrollPane, GroupLayout.DEFAULT_SIZE, 506, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(questionScrollPane, GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE))
				.addComponent(buttonPane, GroupLayout.DEFAULT_SIZE, 903, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(questionScrollPane, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
						.addComponent(paperDetailScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(buttonPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		questionTable = new JTable();
		questionTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);//设置允许多行选择
		questionTable.setAutoCreateRowSorter(true);//设置自动创建排序器
		questionScrollPane.setViewportView(questionTable);
		
		paperDetailTable = new JTable();
		paperDetailTable.setAutoCreateRowSorter(true);//设置自动创建排序器
		paperDetailScrollPane.setViewportView(paperDetailTable);
		contentPanel.setLayout(gl_contentPanel);
	}
	
	/**
	 * 初始试卷明细表格的数据
	 */
	private void initPaperDetailTableDatas(PaperSection paperSection){
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "试卷", "章节", "题目", "本题分数", "排序号"
		};
		
		try {
			//从窗口中获取业务逻辑层对象
			PaperDetailService paperDetailService=Main.lookUp("paperDetailService");
			//创建实体类对象
			PaperDetail paperDetail=new PaperDetail();
			paperDetail.setPaper(paper);
			//默认查找试卷第一个章节的试题出来显示到表格中
			if(paperSection==null){//如没有传章节对象过来时,从试卷对象中拿出章节与试题明细对象关联
				List<PaperSection> paperSections = paper.getPaperSections();
				
				if(paperSections!=null&&paperSections.size()>0){
					paperDetail.setPaperSection(paperSections.get(0));//关联章节
				}
			}else{
				paperDetail.setPaperSection(paperSection);//关联章节
			}
			
			List<PaperDetail> list = paperDetailService.getList(paperDetail);
			if(list.size()>0){
				datas = new Object[list.size()][6];
				notInIds="";
				for(int i=0;i<list.size();i++){
					PaperDetail paperDetail2 = list.get(i);
					datas[i][0]=paperDetail2.getId();//编号
					datas[i][1]=paperDetail2.getPaper().getPaper_name();//试卷
					datas[i][2]=paperDetail2.getPaperSection().getSection_name();//章节
					datas[i][3]=paperDetail2.getQuestion().getContent();//题目
					datas[i][4]=paperDetail2.getScore().toString();//本题分数
					datas[i][5]=paperDetail2.getPorder().toString();//排序号
					
					notInIds=paperDetail2.getQuestion().getId()+",";
				}
				
				//构造试题编号，用于not in语句
				if(notInIds.length()>0){
					notInIds=notInIds.substring(0, notInIds.length()-1);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//创建表格数据模型对象
		DefaultTableModel dataModel = new DefaultTableModel(datas,columnNames);
		//创建表格模型监听器对象
		TableModelListener paperDetailTableModelListener=new PaperDetailTableModelListener();
		dataModel.addTableModelListener(paperDetailTableModelListener);//给表格数据模型对象绑定监听器对象
		paperDetailTable.setModel(dataModel);//给表格数据模型
	}
	
	/**
	 * 初始试题表格数据的方法
	 */
	private void initQuestionTableDatas(){
		
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
			{null, null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "题库", "试题类型", "难度级别", "题干内容", "状态"
		};
		
		//创建实体类对象
		Question question1=new Question();
		
		QuestionDb questionDb=new QuestionDb();
		//从难题库下拉框中拿出选中的项
		SelectItem selectItemDb =(SelectItem)questionDbCbb.getSelectedItem();
		if(selectItemDb!=null){
			questionDb.setId(selectItemDb.getId());//设置题库编号
			question1.setQuestionDb(questionDb);//关联题库
		}
		//从题型下拉框中拿出选中的项
		SelectItem selectItemType =(SelectItem)qTypeCbb.getSelectedItem();
		if(selectItemType!=null){
			question1.setQtype(selectItemType.getId());
		}
		//从难易度下拉框中拿出选中的项
		SelectItem selectItemLevel=(SelectItem) qLevelCbb.getSelectedItem();
		if(selectItemLevel!=null){
			question1.setQlevel(selectItemLevel.getId());
		}
		
		try {
			//从容器中获取业务逻辑层对象
			QuestionService questionService=Main.lookUp("questionService");
			List<Question> list = questionService.getList(notInIds,question1);//调用业务逻辑层对象的方法拿数据
			if(list.size()>0){
				datas=new Object[list.size()][6];
				for(int i=0;i<list.size();i++){
					Question question = list.get(i);
					datas[i][0]=question.getId();//编号
					datas[i][1]=question.getQuestionDb().getName();//题库名称
					switch(question.getQtype()){//题型判断
						case 1:
							datas[i][2]="单选";
							break;
						case 2:
							datas[i][2]="多选";
							break;
						case 3:
							datas[i][2]="填空";
							break;
						case 4:
							datas[i][2]="判断";
							break;
						case 5:
							datas[i][2]="问答";
							break;
					}
					
					switch(question.getQlevel()){//难度级别
						case 1:
							datas[i][3]="易";
							break;
						case 2:
							datas[i][3]="正常";
							break;
						case 3:
							datas[i][3]="难";
							break;
						
					}
					datas[i][4]=question.getContent();
					datas[i][5]=question.getStatus().equals("0")?"不完全开放":"完全开放";//状态
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		questionTable.setModel(new DefaultTableModel(datas,columnNames));
	}
	
	/**
	 * 章节下拉框监听器
	 * @author edu-1
	 *
	 */
	private class PaperSectionCbbItemListener implements ItemListener{
		
        public void itemStateChanged(ItemEvent event){
        	if(event.getStateChange()==ItemEvent.SELECTED){
        		SelectItem selectItem=(SelectItem)event.getItem();
        		PaperSection paperSection =new PaperSection();
        		paperSection.setId(selectItem.getId());
        		initPaperDetailTableDatas(paperSection);//刷新试卷试题表格
        		initQuestionTableDatas();//刷新试题表格
        	}
        	
        }
           
    };
    
    /**
	 * 题库下拉框监听器
	 * @author edu-1
	 *
	 */
	private class QuestionDbCbbItemListener implements ItemListener{
		
        public void itemStateChanged(ItemEvent event){
        	if(event.getStateChange()==ItemEvent.SELECTED){
        		initQuestionTableDatas();//刷新试题表格
        	}
        	
        }
           
    };
    
    /**
	 * 试题类型下拉框监听器
	 * @author edu-1
	 *
	 */
	private class QTypeCbbItemListener implements ItemListener{
		
        public void itemStateChanged(ItemEvent event){
        	if(event.getStateChange()==ItemEvent.SELECTED){
        		initQuestionTableDatas();//刷新试题表格
        	}
        	
        }
           
    };
    
    /**
	 * 试题难易度下拉框监听器
	 * @author edu-1
	 *
	 */
	private class QLevelCbbItemListener implements ItemListener{
		
        public void itemStateChanged(ItemEvent event){
        	if(event.getStateChange()==ItemEvent.SELECTED){
        		initQuestionTableDatas();//刷新试题表格
        	}
        	
        }
           
    };
    
    /**
     * 添加按钮的监听器类
     * @author edu-1
     *
     */
    private class AddActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = questionTable.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = questionTable.getSelectedRows();//当前选择行号数组
			
			List<PaperDetail> list=new ArrayList<PaperDetail>();//创建集合用于存放实体类对象
			
			if(selectedRows!=null&&selectedRows.length>0){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				PaperDetail paperDetail=new PaperDetail();//试卷明细
				Question question=new Question();//创建试题对象
				question.setId(id);
				paperDetail.setQuestion(question);//关联试题对象
				
				paperDetail.setPaper(paper);//关联试卷对象
				
				SelectItem selectItem = (SelectItem)paperSectionCbb.getSelectedItem();
				PaperSection paperSection=new PaperSection();
				paperSection.setId(selectItem.getId());
				paperDetail.setPaperSection(paperSection);//关联章节对象
				
				paperDetail.setScore(0.0);//本题分数
				paperDetail.setPorder(0);//排序号
				
				list.add(paperDetail);//将对象添加到集合中
				
			}
			
			try {
				//从容器里获得业务逻辑层对象
				PaperDetailService paperDetailService=Main.lookUp("paperDetailService");
				boolean flag = paperDetailService.add(list);//调用对象
				//创建提示信息框对象
				MessageDialog messageDialog=new MessageDialog();
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					initPaperDetailTableDatas(null);//调用初始试卷题目表格的方法
					initQuestionTableDatas();//调用初始试题表格的方法
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
    	
    }
    
    
    /**
     * 删除按钮的监听器类
     * @author edu-1
     *
     */
    private class DeleteActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = paperDetailTable.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = paperDetailTable.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					//从容器里获得业务逻辑层对象
					PaperDetailService paperDetailService=Main.lookUp("paperDetailService");
					boolean flag = paperDetailService.delete(id);//通过编号删除对象
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
						initPaperDetailTableDatas(null);//调用初始试卷题目表格的方法
						initQuestionTableDatas();//调用初始试题表格的方法
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
			
			
		}
    	
    }
    
    /**
     * 试卷表格监听器类，用于监听在表格中修改分数或排序号后，保存修改后相关数据到数据
     * @author mashiqiong
     *
     */
    private class PaperDetailTableModelListener implements TableModelListener{

		@Override
		public void tableChanged(TableModelEvent e) {
			if(e.getType() == TableModelEvent.UPDATE){
				RowSorter<? extends TableModel> rowSorter = paperDetailTable.getRowSorter();//拿到行排序器
				TableModel model = rowSorter.getModel();//表格的数据模型
				int index = rowSorter.convertRowIndexToModel(e.getLastRow());//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				String score = (String)model.getValueAt(index, 4);//获取选中第五列数据，分数
				System.out.println(score);
				String porder = (String)model.getValueAt(index, 5);//获取选中第六列数据，排序
				//获得编辑后单元格得值
		        //String newvalue = paperDetailTable.getValueAt(e.getLastRow(),e.getColumn()).toString();
				
				try {
					//从容器里获得业务逻辑层对象
					PaperDetailService paperDetailService=Main.lookUp("paperDetailService");
					PaperDetail paperDetail = paperDetailService.get(id);//调用对象
					paperDetail.setScore(Double.valueOf(score));//分数
					paperDetail.setPorder(Integer.valueOf(porder));//排序
					boolean flag = paperDetailService.update(paperDetail);
					//创建提示信息框对象
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("修改成功");
						messageDialog.setVisible(true);
						initPaperDetailTableDatas(null);//调用初始试卷题目表格的方法
						initQuestionTableDatas();//调用初始试题表格的方法
					}else{
						messageDialog.setMsg("修改失败");
						messageDialog.setVisible(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
		        
		     }
		}
    	
    }
}
