package com.examsys.view;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
/**
 * 多选题选项面板
 * @author edu-1
 *
 */
public class MultiItemPanel extends JPanel {
	private JTextArea contentTaa;//选项描述输入 框
	private JCheckBox itemCkb;//选项单选按钮
	private JCheckBox deleteCkb;//选中删除复选框
	private JLabel label;
	private JTextField itemTxf;//选项编号输入框
	private JLabel label_1;
	
	public JTextArea getContentTaa() {
		return contentTaa;
	}

	public JCheckBox getItemCkb() {
		return itemCkb;
	}

	public JCheckBox getDeleteCkb() {
		return deleteCkb;
	}

	public JTextField getItemTxf() {
		return itemTxf;
	}

	/**
	 * Create the panel.
	 */
	public MultiItemPanel() {
		
		itemCkb = new JCheckBox("标准答案");
		
		contentTaa = new JTextArea();
		contentTaa.setRows(5);
		
		deleteCkb = new JCheckBox("选中删除");
		
		label = new JLabel("选项编号：");
		
		itemTxf = new JTextField();
		itemTxf.setToolTipText("编号可以是A,B,C,D或者数字，自己看着办");
		itemTxf.setColumns(10);
		
		label_1 = new JLabel("选项描述：");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(deleteCkb)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(itemCkb)
					.addGap(6)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(itemTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
					.addGap(97))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(37)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(itemCkb)
									.addComponent(label)
									.addComponent(itemTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(label_1))
								.addComponent(deleteCkb)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(16)
							.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(18, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}
}
