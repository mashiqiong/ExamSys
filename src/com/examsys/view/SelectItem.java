package com.examsys.view;
/**
 * 此类供菜单项或下拉框中的项使用
 * @author edu-1
 *
 */
public class SelectItem {
	private Integer id;//编号
	private String name;//项名称
	
	public SelectItem() {
		super();
	}

	public SelectItem(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * 获取编号
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置编号
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SelectItem other = (SelectItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
