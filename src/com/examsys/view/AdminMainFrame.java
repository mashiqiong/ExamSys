package com.examsys.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
/**
 * 管理员主窗口
 * @author edu-1
 *
 */
public class AdminMainFrame extends JFrame {

	private JPanel contentPane;

	private AdminCenterPanel adminCenterPanel;//中间面板
	
	public AdminCenterPanel getAdminCenterPanel() {
		return adminCenterPanel;
	}

	/**
	 * Create the frame.
	 */
	public AdminMainFrame() {
		setTitle("管理员主窗口");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		//顶部面板
		contentPane.add(new AdminTopPanel(), BorderLayout.NORTH);
		//左边面板
		contentPane.add(new AdminLeftPanel(this), BorderLayout.WEST);
		
		//中间面板
		adminCenterPanel = new AdminCenterPanel();
		contentPane.add(adminCenterPanel, BorderLayout.CENTER);
		
		//右边面板
		//contentPane.add(new AdminRightPanel(), BorderLayout.EAST);
		
		setContentPane(contentPane);
		
		repaint();
	}

}
