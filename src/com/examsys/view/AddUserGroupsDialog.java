package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.examsys.po.UserGroups;
import com.examsys.service.Main;
import com.examsys.service.UserGroupsService;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 会员组添加窗口
 * @author edu-1
 *
 */
public class AddUserGroupsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField groupNameTxf;//组名输入框
	private JTextField remarkTxf;//备注输入框

	/**
	 * Create the dialog.
	 */
	public AddUserGroupsDialog() {
		setModal(true);
		setTitle("会员组添加");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("组名：");
		
		groupNameTxf = new JTextField();
		groupNameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(groupNameTxf, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(remarkTxf)))
					.addContainerGap(230, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(32)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(groupNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(33)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(118, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建监听器对象
		ActionListener saveActionListener = new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮绑定监听器对象
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		
		getRootPane().setDefaultButton(saveBtn);
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddUserGroupsDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		
		buttonPane.add(cancelBtn);
	}

	/**
	 * 保存按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			String groupName = groupNameTxf.getText();//组名
			String remark = remarkTxf.getText();//备注
			
			//创建信息提示框对象
			MessageDialog messageDialog=new MessageDialog();
			if(groupName==null||"".equals(groupName)){
				messageDialog.setMsg("组名不能为空");
				messageDialog.setVisible(true);
				return;
			}
			//创建实体类对象
			UserGroups userGroups=new UserGroups();
			userGroups.setGroup_name(groupName);
			userGroups.setRemark(remark);
			
			try {
				//从容器中拿到业务逻辑层对象
				UserGroupsService userGroupsService=Main.lookUp("userGroupsService");
				boolean flag = userGroupsService.add(userGroups);//调用业务逻辑层对象的添加方法保存数据
				
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					AddUserGroupsDialog.this.dispose();//销毁窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
