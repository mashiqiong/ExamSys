package com.examsys.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
/**
 * 考试者主窗口
 * @author edu-1
 *
 */
public class UsersMainFrame extends JFrame {

	private JPanel contentPane;
	private UserCenterPanel userCenterPanel;
	/**
	 * Create the frame.
	 */
	public UsersMainFrame() {
		setTitle("考试者主窗口");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 678, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		
		//顶部面板
		contentPane.add(new UserTopPanel(), BorderLayout.NORTH);
		
		//左边面板
		contentPane.add(new UserLeftPanel(this), BorderLayout.WEST);
		
		//中间面板
		userCenterPanel=new UserCenterPanel();
		contentPane.add(userCenterPanel, BorderLayout.CENTER);
		
		setContentPane(contentPane);
	}

}
