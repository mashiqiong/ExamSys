package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.AdminRolesSettings;
import com.examsys.service.AdminRolesSettingsService;
import com.examsys.service.Main;

/**
 * 系统功能修改对话框
 * @author edu-1
 *
 */
public class UpdateAdminRolesSettingsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nameTxf;//功能名称输入框
	private JTextField codeTxf;//功能代码输入框
	private JTextField porderTxf;//排序号输入框
	private JComboBox parentIdCbb;//父级功能下拉框
	private AdminRolesSettings oldAdminRolesSettings;//待修改的功能对象
	
	/**
	 * 供外部程序传一个系统功能对象 过来
	 * @param oldAdminRolesSettings
	 */
	public void setOldAdminRolesSettings(AdminRolesSettings oldAdminRolesSettings) {
		this.oldAdminRolesSettings = oldAdminRolesSettings;
		nameTxf.setText(oldAdminRolesSettings.getName());//功能名称
		codeTxf.setText(oldAdminRolesSettings.getCode());//功能代码
		porderTxf.setText(oldAdminRolesSettings.getPorder().toString());//排序号
		
		//创建下拉框默认选中项对象
		SelectItem selectItem=new SelectItem();
		selectItem.setId(oldAdminRolesSettings.getAdminRolesSettings().getId());
		selectItem.setName(oldAdminRolesSettings.getAdminRolesSettings().getName());
		
		parentIdCbb.setSelectedItem(selectItem);//设置下拉框默认选中项
		
	}

	/**
	 * Create the dialog.
	 */
	public UpdateAdminRolesSettingsDialog() {
		setModal(true);
		setTitle("修改系统功能");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("功能名称：");
		
		nameTxf = new JTextField();
		nameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("功能代码：");
		
		codeTxf = new JTextField();
		codeTxf.setColumns(10);
		
		JLabel label_2 = new JLabel("父级功能：");
		
		parentIdCbb = new JComboBox();
		
		JLabel label_3 = new JLabel("排序号：");
		
		porderTxf = new JTextField();
		porderTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(nameTxf, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(codeTxf))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(parentIdCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_3)
							.addGap(18)
							.addComponent(porderTxf)))
					.addContainerGap(235, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(nameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(codeTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_2)
						.addComponent(parentIdCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(22)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_3)
						.addComponent(porderTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(73, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton saveBtn = new JButton("保存");
		
		//创建监听器对象
		ActionListener saveActionListener = new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮添加监听器
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		
		getRootPane().setDefaultButton(saveBtn);
		JButton cancelButton = new JButton("取消");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateAdminRolesSettingsDialog.this.dispose();//销毁窗口
			}
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		
		initParentIdCbb();//调用初始化父级功能下拉框方法
	}

	/**
	 * 初始化父级功能下拉框的方法
	 */
	private void initParentIdCbb(){
		
		try {
			//从容器中获取业务逻辑层对象
			AdminRolesSettingsService adminRolesSettingsService = Main.lookUp("adminRolesSettingsService");
			
			//创建系统功能对象
			AdminRolesSettings adminRolesSettings=new AdminRolesSettings();
			//创建它的父级功能对象
			AdminRolesSettings parentAdminRolesSettings=new AdminRolesSettings();
			parentAdminRolesSettings.setId(null);//编号为零
			
			adminRolesSettings.setAdminRolesSettings(parentAdminRolesSettings);//关联父级
			List<AdminRolesSettings> list = adminRolesSettingsService.getList(adminRolesSettings);//获取所有系统功能项
			
			//创建一个空项
			SelectItem emptyItem=new SelectItem();
			emptyItem.setId(null);
			emptyItem.setName("");
			parentIdCbb.addItem(emptyItem);//把空的项添加到下拉框中
			
			for(AdminRolesSettings ars:list){
				//创建系统功能下拉框中所要的项对象
				SelectItem selectItem=new SelectItem();
				selectItem.setId(ars.getId());
				selectItem.setName(ars.getName());
				
				parentIdCbb.addItem(selectItem);//把项添加到下拉框中
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * 专门给保存按钮用的一个监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener {
		/**
		 * 当保存按钮被 点击时调用些方法
		 */
		public void actionPerformed(ActionEvent e) {
			String name = nameTxf.getText();//功能名称
			String code = codeTxf.getText();//功能代码
			SelectItem selectItem = (SelectItem)parentIdCbb.getSelectedItem();
			Integer porder = Integer.valueOf(porderTxf.getText());//排序号
			
			//父级功能对象
			AdminRolesSettings parentAdminRolesSettings=new AdminRolesSettings();
			parentAdminRolesSettings.setId(selectItem.getId());
			
			//回填新数据
			oldAdminRolesSettings.setName(name);
			oldAdminRolesSettings.setCode(code);
			oldAdminRolesSettings.setPorder(porder);
			oldAdminRolesSettings.setAdminRolesSettings(parentAdminRolesSettings);//关联父级功能
			
			try {
				//从容器中获取业务逻辑层对象
				AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
				//调用业务逻辑层对象的添加方法，保存系统功能对象
				adminRolesSettingsService.update(oldAdminRolesSettings);
				//创建信息提示框
				MessageDialog messageDialog=new MessageDialog();
				messageDialog.setMsg("修改成功");
				messageDialog.setVisible(true);
				
				UpdateAdminRolesSettingsDialog.this.dispose();//保存成功，则销毁添加窗口
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
