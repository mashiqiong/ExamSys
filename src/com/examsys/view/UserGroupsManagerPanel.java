package com.examsys.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.UserGroups;
import com.examsys.service.Main;
import com.examsys.service.UserGroupsService;
/**
 * 会员组管理面板
 * @author edu-1
 *
 */
public class UserGroupsManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public UserGroupsManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("组名：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建查询按钮监听器对象
		ActionListener queryActionListener = new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);//给查询按钮绑定监听器
		
		JButton addBtn = new JButton("添加");
		//创建添加按钮监听器对象
		ActionListener addActionListener = new AddActionListener();
		addBtn.addActionListener(addActionListener);//给添加按钮绑定监听器
		
		JButton updateBtn = new JButton("修改");
		//创建修改按钮监听器对象
		ActionListener updateActionListener = new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//给修改按钮绑定监听器
		
		JButton deleteBtn = new JButton("删除");
		//创建删除按钮监听器对象
		ActionListener deleteActionListener = new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给删除按钮绑定监听器
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(deleteBtn)
							.addGap(83))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)
					.addGap(26)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label)
						.addComponent(updateBtn)
						.addComponent(addBtn)
						.addComponent(queryBtn)
						.addComponent(deleteBtn))
					.addContainerGap())
		);
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {updateBtn, deleteBtn});
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//设置自动创建行排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);

		initDatas();//调用初始化表格数据的方法
	}
	
	/**
	 * 添加按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//创建添加会员组对话框对象
			AddUserGroupsDialog addUserGroupsDialog=new AddUserGroupsDialog();
			addUserGroupsDialog.setVisible(true);
			initDatas();//刷新表格数据
			
		}
	};
	
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {

			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					UserGroupsService configService=Main.lookUp("userGroupsService");
					UserGroups userGroups = configService.get(id);//通过编号找到对象
					
					//创建修改系统参数对话框对象
					UpdateUserGroupsDialog updateUserGroupsDialog=new UpdateUserGroupsDialog();
					updateUserGroupsDialog.setOldUserGroup(userGroups);//把实体类对象传给对话框用于显示与修改
					updateUserGroupsDialog.setVisible(true);
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
			
		}
	}
	
	/**
	 * 查询按钮监听器类
	 * @author edu-1
	 *
	 */
	private class QueryActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			Object[][] datas = new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
			};
			String[] columnNames = new String[] {
				"编号", "组名", "备注"
			};
			
			String groupName = keywordTxf.getText();//获取查询内容
			
			//创建查询条件对象
			UserGroups groups=new UserGroups();
			groups.setGroup_name(groupName);
			
			try {
				//获得业务逻辑层对象
				UserGroupsService userGroupsService= Main.lookUp("userGroupsService");
				
				List<UserGroups> list = userGroupsService.getList(groups);//调用业务逻辑层对象的方法获取数据
				
				if(list.size()>0){
					datas = new Object[list.size()][3];
					
					for(int i=0;i<list.size();i++){
						UserGroups userGroups = list.get(i);//从集合中拿出数据项
						
						datas[i][0]=userGroups.getId();//编号
						datas[i][1]=userGroups.getGroup_name();//组名
						datas[i][2]=userGroups.getRemark();//备注
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			table.setModel(new DefaultTableModel(datas,columnNames));//更新表格的数据模型
		}
	}
	
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					UserGroupsService userGroupsService=Main.lookUp("userGroupsService");
					boolean flag = userGroupsService.delete(id);//通过 编号删除对象
					//创建信息提示框
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
					
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		Object[][] datas = new Object[][] {
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
		};
		String[] columnNames = new String[] {
			"编号", "组名", "备注"
		};
		
		
		try {
			//获得业务逻辑层对象
			UserGroupsService userGroupsService= Main.lookUp("userGroupsService");
			
			List<UserGroups> list = userGroupsService.getList();//调用业务逻辑层对象的方法获取数据
			
			if(list.size()>0){
				datas = new Object[list.size()][3];
				
				for(int i=0;i<list.size();i++){
					UserGroups userGroups = list.get(i);//从集合中拿出数据项
					
					datas[i][0]=userGroups.getId();//编号
					datas[i][1]=userGroups.getGroup_name();//组名
					datas[i][2]=userGroups.getRemark();//备注
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));//更新表格的数据模型
	}

}
