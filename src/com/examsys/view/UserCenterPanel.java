package com.examsys.view;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
/**
 * 考试者中间面板
 * @author mashiqiong
 *
 */
public class UserCenterPanel extends JPanel {

	private JTabbedPane tabbedPane;//卡片面板
	
	/**
	 * 供外部程序调用卡片面板
	 * @return
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}
	
	/**
	 * Create the panel.
	 */
	public UserCenterPanel() {
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
					.addGap(4))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE)
					.addGap(4))
		);
		setLayout(groupLayout);

		tabbedPane.add("考试试卷", new UserPaperListPanel());
	}
}
