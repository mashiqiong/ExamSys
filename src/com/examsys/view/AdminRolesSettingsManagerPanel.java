package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.AdminRolesSettings;
import com.examsys.service.AdminRolesSettingsService;
import com.examsys.service.Main;
/**
 * 系统功能管理面板
 * @author edu-1
 *
 */
public class AdminRolesSettingsManagerPanel extends JPanel {
	private JTable table;
	private JTextField keyWordTxf;

	/**
	 * Create the panel.
	 */
	public AdminRolesSettingsManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("功能名称：");
		
		keyWordTxf = new JTextField();
		keyWordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建查询按钮监听器对象
		ActionListener queryActionListener = new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);//给查询按钮绑定监听器
		
		JButton addBtn = new JButton("添加");
		//创建添加按钮监听器对象
		ActionListener addActionListener = new AddActionListener();
		addBtn.addActionListener(addActionListener);//给添加按钮绑定监听器
		
		JButton updateBtn = new JButton("修改");
		//创建修改按钮监听器对象
		ActionListener updateActionListener = new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//给修改按钮添加监听器
		
		JButton deleteBtn = new JButton("删除");
		//创建删除按钮监听器对象
		ActionListener deleteActionListener = new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给按钮添加监听 器
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keyWordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(keyWordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn))
					.addGap(20))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//自动创建排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		initDatas();//初始表格数据

	}
	
	/**
	 * 查询按钮监听器类
	 * @author edu-1
	 *
	 */
	private class QueryActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String[] columnNames = new String[] {
					"\u7F16\u53F7", "\u529F\u80FD\u540D\u79F0", "\u529F\u80FD\u4EE3\u7801", "\u7236\u7EA7\u83DC\u5355", "\u6392\u5E8F\u53F7"
			};
			//默认数据
			Object[][] datas = new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			};
				
			String name = keyWordTxf.getText();
			AdminRolesSettings adminRolesSettings1=new AdminRolesSettings();
			adminRolesSettings1.setName(name);
			try {
				AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
				List<AdminRolesSettings> list = adminRolesSettingsService.getList(adminRolesSettings1);
				if(list.size()>0){//如果拿到功能项则构建表数据
					datas = new Object[list.size()][5];//创建一个新数据数组对象
					for(int i=0;i<list.size();i++){
						//从集合中拿出一个对象
						AdminRolesSettings adminRolesSettings = list.get(i);
						datas[i][0]=adminRolesSettings.getId();//编号
						datas[i][1]=adminRolesSettings.getName();//功能名称
						datas[i][2]=adminRolesSettings.getCode();//代码
						datas[i][3]=adminRolesSettings.getAdminRolesSettings().getName();//父级菜单
						datas[i][4]=adminRolesSettings.getPorder();//排序
					}
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			//填充数据到表格
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
	};
	
	/**
	 * 添加按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				//创建添加系统功能对话框
				AddAdminRolesSettingsDialog adminRolesSettingsDialog=new AddAdminRolesSettingsDialog();
				adminRolesSettingsDialog.setVisible(true);
				initDatas();//刷新表数据
			}
		};
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
					adminRolesSettingsService.delete(id);//通过系统功能 编号删除对象
					
					//创建信息提示框
					MessageDialog messageDialog=new MessageDialog();
					messageDialog.setMsg("删除成功");
					messageDialog.setVisible(true);
					
					initDatas();//刷新表数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	};
	
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
					AdminRolesSettings adminRolesSettings = adminRolesSettingsService.get(id);//通过系统功能 编号找到对象
					
					UpdateAdminRolesSettingsDialog adminRolesSettingsDialog=new UpdateAdminRolesSettingsDialog();
					adminRolesSettingsDialog.setOldAdminRolesSettings(adminRolesSettings);
					adminRolesSettingsDialog.setVisible(true);
					initDatas();//刷新表数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	};
	
	/**
	 * 初始化表格数据的方法 
	 */
	private void initDatas(){
		String[] columnNames = new String[] {
			"\u7F16\u53F7", "\u529F\u80FD\u540D\u79F0", "\u529F\u80FD\u4EE3\u7801", "\u7236\u7EA7\u83DC\u5355", "\u6392\u5E8F\u53F7"
		};
		//默认数据
		Object[][] datas = new Object[][] {
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
		};
		try {
			//系统功能业务逻辑层对象
			AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
		
			//调用业务逻辑层对象获取所有功能项
			List<AdminRolesSettings> list = adminRolesSettingsService.getList();
			if(list.size()>0){//如果拿到功能项则构建表数据
				datas = new Object[list.size()][5];//创建一个新数据数组对象
				for(int i=0;i<list.size();i++){
					//从集合中拿出一个对象
					AdminRolesSettings adminRolesSettings = list.get(i);
					datas[i][0]=adminRolesSettings.getId();//编号
					datas[i][1]=adminRolesSettings.getName();//功能名称
					datas[i][2]=adminRolesSettings.getCode();//代码
					datas[i][3]=adminRolesSettings.getAdminRolesSettings().getName();//父级菜单
					datas[i][4]=adminRolesSettings.getPorder();//排序
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//填充数据到表格
		table.setModel(new DefaultTableModel(datas,columnNames));
	}
}
