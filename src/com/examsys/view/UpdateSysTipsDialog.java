package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.examsys.po.SysTips;
import com.examsys.service.Main;
import com.examsys.service.SysTipsService;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 修改系统提示信息窗口
 * @author edu-1
 *
 */
public class UpdateSysTipsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField scodeTxf;//代码输入框
	private JTextField sdescTxf;//信息内容输入框
	private SysTips oldSysTips;//待修改的对象
	
	/**
	 * 供外部程序传入一个实体类对象过来用于显示并修改
	 * @param oldSysTips
	 */
	public void setOldSysTips(SysTips oldSysTips) {
		this.oldSysTips = oldSysTips;
		scodeTxf.setText(oldSysTips.getScode());//代码
		sdescTxf.setText(oldSysTips.getSdesc());//信息内容
	}

	/**
	 * Create the dialog.
	 */
	public UpdateSysTipsDialog() {
		setModal(true);
		setTitle("修改系统提示信息");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("代码：");
		
		scodeTxf = new JTextField();
		scodeTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("提示信息：");
		
		sdescTxf = new JTextField();
		sdescTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(36)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_1)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(scodeTxf)
						.addComponent(sdescTxf, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE))
					.addContainerGap(208, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(48)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(scodeTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(sdescTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(108, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建保存按钮监听器对象
		ActionListener saveActionListener=new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮绑定监听器对象 
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		
		getRootPane().setDefaultButton(saveBtn);//设置当前窗口中默认获得焦点按钮
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateSysTipsDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		buttonPane.add(cancelBtn);
	}
	
	/**
	 * 保存按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String scode = scodeTxf.getText();//代码
			String sdesc = sdescTxf.getText();//信息内容
			
			//创建信息提示框对象
			MessageDialog messageDialog=new MessageDialog();
			if(scode==null||"".equals(scode)){
				messageDialog.setMsg("代码不能为空");
				messageDialog.setVisible(true);
				return;
			}
			//创建实体类对象
			SysTips sysTips=new SysTips();
			sysTips.setId(oldSysTips.getId());
			sysTips.setScode(scode);
			sysTips.setSdesc(sdesc);
			
			try {
				//从容器中获取业务逻辑层对象
				SysTipsService sysTipsService=Main.lookUp("sysTipsService");
				boolean flag = sysTipsService.update(sysTips);//调用业务逻辑层对象保存数据
				if(flag){
					messageDialog.setMsg("修改成功");
					messageDialog.setVisible(true);
					UpdateSysTipsDialog.this.dispose();//销毁窗口
				}else{
					messageDialog.setMsg("修改失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
	}
	

}
