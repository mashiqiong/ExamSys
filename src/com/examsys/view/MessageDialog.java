package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 系统统一信息提示对话框
 * @author edu-1
 *
 */
public class MessageDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel msgLbl;//显示提示信息的标签
	
	/**
	 * Create the dialog.
	 */
	public MessageDialog() {
		setModal(true);
		setTitle("信息提示");
		setBounds(100, 100, 310, 185);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		msgLbl = new JLabel("");
		msgLbl.setFont(new Font("宋体", Font.PLAIN, 14));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(msgLbl, GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(msgLbl, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
		);
		contentPanel.setLayout(gl_contentPanel);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton okButton = new JButton("确定");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MessageDialog.this.dispose();//销毁窗口
			}
		});
		
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	}
	
	/**
	 * 设置提示信息
	 * @param msg
	 */
	public void setMsg(String msg){
		msgLbl.setText(msg);//将传过来的提示信息填到标签进行显示
	}

}
