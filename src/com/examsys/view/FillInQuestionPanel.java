package com.examsys.view;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
/**
 * 填空题面板
 * @author edu-1
 *
 */
public class FillInQuestionPanel extends JPanel {
	
	private JPanel panel;
	private JTextArea contentTaa;
	private JTextArea keyDescTaa;
	
	public JPanel getPanel() {
		return panel;
	}

	public JTextArea getContentTaa() {
		return contentTaa;
	}

	public JTextArea getKeyDescTaa() {
		return keyDescTaa;
	}

	/**
	 * Create the panel.
	 */
	public FillInQuestionPanel() {
		
		JLabel label = new JLabel("题干内容：");
		
		contentTaa = new JTextArea();
		contentTaa.setRows(5);
		
		JButton addBtn = new JButton("添加选项");
		ActionListener addItemActionListener=new AddItemActionListener();
		addBtn.addActionListener(addItemActionListener);
		
		JButton deleteBtn = new JButton("删除选项");
		ActionListener deleteItemActionListener=new DeleteItemActionListener();
		deleteBtn.addActionListener(deleteItemActionListener);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label_1 = new JLabel("试题解析：");
		
		keyDescTaa = new JTextArea();
		keyDescTaa.setRows(5);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(label)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(27)
									.addComponent(addBtn)
									.addGap(18)
									.addComponent(deleteBtn))
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 683, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED, 113, Short.MAX_VALUE))
						.addGroup(Alignment.LEADING, groupLayout.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
								.addComponent(label_1)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(keyDescTaa))
							.addComponent(scrollPane, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 769, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
							.addGap(26)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(addBtn)
								.addComponent(deleteBtn)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(label)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 278, GroupLayout.PREFERRED_SIZE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
							.addComponent(label_1)
							.addGap(36))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keyDescTaa, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));//把面反的布局设置为网格布局
		setLayout(groupLayout);

	}
	
	/**
	 * 添加选项按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddItemActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			//创建选项面板对象
			FillInItemPanel fillInItemPanel=new FillInItemPanel();
			panel.add(fillInItemPanel);
			panel.updateUI();//刷新界面
		}
	}
	
	/**
	 * 删除选项按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteItemActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			Component[] components = panel.getComponents();
			for(int i=0;i<components.length;i++){
				//从数组中拿出选项面板对象
				FillInItemPanel fillInItemPanel=(FillInItemPanel)components[i];
				if(fillInItemPanel.getDeleteCkb().isSelected()){//如果此选面板中的复选框被选中则删除
					panel.remove(fillInItemPanel);
				}
			}
			panel.updateUI();//刷新界面
		}
	}

}
