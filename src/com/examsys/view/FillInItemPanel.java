package com.examsys.view;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
/**
 * 填空题选项面板
 * @author edu-1
 *
 */
public class FillInItemPanel extends JPanel {
	private JTextArea contentTaa;//选项描述输入 框
	private JCheckBox deleteCkb;//选中删除复选框
	private JLabel label;
	
	public JTextArea getContentTaa() {
		return contentTaa;
	}

	public JCheckBox getDeleteCkb() {
		return deleteCkb;
	}

	/**
	 * Create the panel.
	 */
	public FillInItemPanel() {
		
		contentTaa = new JTextArea();
		contentTaa.setRows(5);
		
		deleteCkb = new JCheckBox("选中删除");
		
		label = new JLabel("标准答案：");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(deleteCkb)
					.addGap(18)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
					.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
					.addGap(207))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(32)
							.addComponent(deleteCkb))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(26)
									.addComponent(label)))))
					.addContainerGap(17, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}
}
