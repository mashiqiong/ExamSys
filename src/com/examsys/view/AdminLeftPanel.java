package com.examsys.view;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import com.examsys.po.AdminRoles;
import com.examsys.po.AdminRolesSettings;
import com.examsys.service.AdminRolesSettingsService;
import com.examsys.service.Main;
/**
 * 管理员菜单面板
 * @author edu-1
 *
 */
public class AdminLeftPanel extends JPanel {
	private AdminMainFrame adminMainFrame;
	private JTree tree;//树对象放在全局这里
	/**
	 * Create the panel.
	 */
	public AdminLeftPanel(final AdminMainFrame adminMainFrame) {
		this.adminMainFrame=adminMainFrame;
		
		//创建滚动条对象
		JScrollPane scrollPane = new JScrollPane();
		
		//根节点对象
		DefaultMutableTreeNode root =new DefaultMutableTreeNode("功能");
		//创建树对象
		tree = new JTree(root);
		
		//给树添加选择监听器
		TreeSelectionListener treeSelectionListener =new MyTreeSelectionListener();
		tree.addTreeSelectionListener(treeSelectionListener);
		
		tree.setRootVisible(true);//设置根节点不显示
		//获取管理员角色
		AdminRoles adminRoles = Main.admin.getAdminRoles();
		//所拥有的功能
		String role_privelege = adminRoles.getRole_privelege();
		//因为之前存在数据中是一串用逗号隔开的功能编号
		String[] priveleges = role_privelege.split(",");
		
		try {
			
			AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
			for(String id:priveleges){
				//通编号去获取系统功能对象
				AdminRolesSettings adminRolesSettings = adminRolesSettingsService.get(Integer.valueOf(id));
				
				//只将父级编号为0的功能项添加到树中
				if(adminRolesSettings.getAdminRolesSettings().getId()==0){
					//创建树节点对象
					DefaultMutableTreeNode childNode =new DefaultMutableTreeNode(adminRolesSettings.getName());
					root.add(childNode);//把树节点添加到根节点中
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		scrollPane.setViewportView(tree);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
	
	/**
	 * 树的监听器实现类
	 * @author edu-1
	 *
	 */
	class MyTreeSelectionListener implements TreeSelectionListener{
		
		//当树被选中的时候执行些方法
		@Override
		public void valueChanged(TreeSelectionEvent e) {
			DefaultMutableTreeNode note = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		    String name = note.toString();//获得这个结点的名称

		    //中间面板的卡片面板
		    JTabbedPane tabbedPane = adminMainFrame.getAdminCenterPanel().getTabbedPane();
		    tabbedPane.removeAll();//清理中间卡片面板中的内容
		    if("系统功能管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new AdminRolesSettingsManagerPanel());
		    }
		    
		    if("管理员管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new AdminManagerPanel());
		    }
		    
		    if("管理员角色管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new AdminRolesManagerPanel());
		    }
		    
		    if("系统参数管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new ConfigManagerPanel());
		    }
		    
		    if("会员组管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new UserGroupsManagerPanel());
		    }
		    
		    if("会员管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new UsersManagerPanel());
		    }
		    
		    if("系统提示信息管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new SysTipsManagerPanel());
		    }
		    
		    if("题库管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new QuestionDbManagerPanel());
		    }
		    
		    if("试题管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new QuestionManagerPanel());
		    }
		    
		    if("试卷管理".equals(name)){//往中间面板中添加管理面板
		    	tabbedPane.add(name, new PaperManagerPanel());
		    }
		}
	};

}
