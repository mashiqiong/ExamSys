package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.Config;
import com.examsys.service.ConfigService;
import com.examsys.service.Main;
/**
 * 添加系统参数对话框
 * @author edu-1
 *
 */
public class AddConfigDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nameTxf;//参数名称输入框
	private JTextField configKeyTxf;//参数代码输入框
	private JTextField configValueTxf;//参数值输入框
	private JTextField remarkTxf;//备注输入框

	/**
	 * Create the dialog.
	 */
	public AddConfigDialog() {
		setModal(true);
		setTitle("系统参数添加");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("参数名称：");
		
		nameTxf = new JTextField();
		nameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("参数代码：");
		
		configKeyTxf = new JTextField();
		configKeyTxf.setColumns(10);
		
		JLabel label_2 = new JLabel("参数值：");
		
		configValueTxf = new JTextField();
		configValueTxf.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(nameTxf, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_2)
							.addGap(18)
							.addComponent(configValueTxf)))
					.addGap(32)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_1)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblNewLabel)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(remarkTxf)
						.addComponent(configKeyTxf, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
					.addContainerGap(34, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(nameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1)
						.addComponent(configKeyTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(40)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2)
								.addComponent(configValueTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(38)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(121, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton saveBtn = new JButton("保存");
		
		//创建保存按钮的监听器对象
		ActionListener saveActionListener = new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮添加监听器对象
		saveBtn.setActionCommand("OK");
		
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddConfigDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		buttonPane.add(cancelBtn);
	}

	/**
	 * 保存按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			String name = nameTxf.getText();
			String configKey = configKeyTxf.getText();
			String configValue = configValueTxf.getText();
			String remark = remarkTxf.getText();
			
			MessageDialog messageDialog =new MessageDialog();//创建信息提示框对象
			if(name==null||"".equals(name)){
				messageDialog.setMsg("参数名称不能为空");
				messageDialog.setVisible(true);
				return;
			}
			
			Config config=new Config();//创建系统参数实体类对象
			config.setName(name);
			config.setConfig_key(configKey);
			config.setConfig_value(configValue);
			config.setRemark(remark);
			
			try {
				//从容器中获取业务逻辑层对象
				ConfigService configService = Main.lookUp("configService");
				//调用业务逻辑层对象的添加方法，保存系统参数对象
				boolean flag = configService.add(config);
				
				if(flag){//判断是否添加成功
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					AddConfigDialog.this.dispose();//保存成功，则销毁添加窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}
	};
}
