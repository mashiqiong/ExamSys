package com.examsys.view;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.Component;
/**
 * 问答题
 * @author edu-1
 *
 */
public class QAnswerQuestionPanel extends JPanel {
	private JTextArea contentTaa;//题干活内容 文本域
	
	private JTextArea keyDescTaa;//试题解析文本域
	
	private JTextArea keyTaa;//标准答案文本域 
	
	public JTextArea getContentTaa() {
		return contentTaa;
	}

	public JTextArea getKeyDescTaa() {
		return keyDescTaa;
	}

	public JTextArea getKeyTaa() {
		return keyTaa;
	}

	/**
	 * Create the panel.
	 */
	public QAnswerQuestionPanel() {
		
		JLabel label = new JLabel("题干内容：");
		
		contentTaa = new JTextArea();
		
		JLabel label_1 = new JLabel("试题解析：");
		
		keyDescTaa = new JTextArea();
		
		JLabel label_2 = new JLabel("标准答案：");
		
		keyTaa = new JTextArea();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(label, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(label_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(label_2, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(contentTaa, GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)
						.addComponent(keyTaa, GroupLayout.PREFERRED_SIZE, 726, GroupLayout.PREFERRED_SIZE)
						.addComponent(keyDescTaa, GroupLayout.PREFERRED_SIZE, 726, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(40)
							.addComponent(label)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(52)
							.addComponent(keyTaa, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(82)
							.addComponent(label_2)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(39)
							.addComponent(keyDescTaa, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(69)
							.addComponent(label_1)))
					.addGap(206))
		);
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {label, label_1, label_2});
		setLayout(groupLayout);

	}

}
