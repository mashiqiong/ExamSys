package com.examsys.view;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * 考试者左边面板
 * @author mashiqiong
 *
 */
public class UserLeftPanel extends JPanel {
	
	private UsersMainFrame usersMainFrame;
	private JTree tree;//树对象放在全局这里
	/**
	 * Create the panel.
	 */
	public UserLeftPanel(UsersMainFrame usersMainFrame) {
		this.usersMainFrame=usersMainFrame;
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 600, Short.MAX_VALUE)
					.addGap(6))
		);
		
		//根节点对象
		DefaultMutableTreeNode root =new DefaultMutableTreeNode("功能");
				
		//创建树对象
		tree = new JTree(root);
		
		//创建树节点对象
		DefaultMutableTreeNode childNode1 =new DefaultMutableTreeNode("考试试卷");
		root.add(childNode1);//把树节点添加到根节点中
		
		//创建树节点对象
		DefaultMutableTreeNode childNode2 =new DefaultMutableTreeNode("考试成绩");
		root.add(childNode2);//把树节点添加到根节点中
				
		//给树添加选择监听器
		TreeSelectionListener treeSelectionListener =new MyTreeSelectionListener();
		tree.addTreeSelectionListener(treeSelectionListener);
		tree.setRootVisible(true);//设置根节点不显示
		
		scrollPane.setViewportView(tree);
		setLayout(groupLayout);
	}
	
	/**
	 * 树的监听器实现类
	 * @author edu-1
	 *
	 */
	class MyTreeSelectionListener implements TreeSelectionListener{
		
		//当树被选中的时候执行些方法
		@Override
		public void valueChanged(TreeSelectionEvent e) {
			DefaultMutableTreeNode note = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		    String name = note.toString();//获得这个结点的名称
		}
	}

}
