package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeModel;

import com.examsys.po.AdminRoles;
import com.examsys.po.AdminRolesSettings;
import com.examsys.service.AdminRolesService;
import com.examsys.service.AdminRolesSettingsService;
import com.examsys.service.Main;
/**
 * 修改角色对话框
 * @author edu-1
 *
 */
public class UpdateAdminRolesDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField roleNameTxf;//角色名称输入框
	private JTextField remarkTxf;//备注输入框
	private JTree tree;//功能树
	private CheckBoxTreeNode rootNode;//树根节点
	private AdminRoles oldAdminRoles;//待修改的角色对象
	
	/**
	 * 把原来数据中的数据显示到界面上供进一步修改
	 * @param oldAdminRoles
	 */
	public void setOldAdminRoles(AdminRoles oldAdminRoles) {
		this.oldAdminRoles = oldAdminRoles;
		
		roleNameTxf.setText(oldAdminRoles.getRole_name());//角色名称
		remarkTxf.setText(oldAdminRoles.getRemark());//备注
		
		String role_privelege = oldAdminRoles.getRole_privelege();//功能项
		String[] rolePriveleges = role_privelege.split(",");//分割字符串
		Arrays.sort(rolePriveleges);
		
		for(int i=0;i<rootNode.getChildCount();i++){
			//二级节点
			CheckBoxTreeNode childNode =(CheckBoxTreeNode)rootNode.getChildAt(i);
			SelectItem childItem=(SelectItem)childNode.getUserObject();
			int index=Arrays.binarySearch(rolePriveleges, childItem.getId().toString());
			if(index>=0){
				childNode.setSelected(true);//把拥有的项设置为选中
			}
			for(int j=0;j<childNode.getChildCount();j++){
				//三级节点
				CheckBoxTreeNode cNode =(CheckBoxTreeNode)childNode.getChildAt(j);
				SelectItem cItem=(SelectItem)cNode.getUserObject();
				
				index=Arrays.binarySearch(rolePriveleges, cItem.getId().toString());
				if(index>=0){
					cNode.setSelected(true);//把拥有的项设置为选中
				}
			}
		}
		
	}

	/**
	 * Create the dialog.
	 */
	public UpdateAdminRolesDialog() {
		setModal(true);
		setTitle("修改角色");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("角色名称：");
		
		roleNameTxf = new JTextField();
		roleNameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("备    注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		
		JLabel label_2 = new JLabel("分配功能：");
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(label_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(label, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(remarkTxf)
								.addComponent(roleNameTxf, GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 343, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(roleNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(32)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_2)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		tree = new JTree();
		scrollPane.setViewportView(tree);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建保存监听器
		ActionListener myActionListener = new MyActionListener();
		saveBtn.addActionListener(myActionListener);//给保存按钮添加监听
		saveBtn.setActionCommand("OK");
		
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		JButton cancelButton = new JButton("取消");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateAdminRolesDialog.this.dispose();//销毁窗口
			}
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		
		initTreeNode();//调用初始化功能树节点的方法
	}
	
	/**
	 * 初始化功能树节点的方法
	 */
	private void initTreeNode(){
		rootNode = new CheckBoxTreeNode("功能");//树的根节点
		
		try {
			AdminRolesSettingsService adminRolesSettingsService=Main.lookUp("adminRolesSettingsService");
			
			//构造查找父级功能项的条件
			AdminRolesSettings adminRolesSettings=new AdminRolesSettings();
			AdminRolesSettings pAdminRolesSettings=new AdminRolesSettings();
			pAdminRolesSettings.setId(0);//零表示顶级功能
			adminRolesSettings.setAdminRolesSettings(pAdminRolesSettings);
			
			//通过条件去查找
			List<AdminRolesSettings> parentList = adminRolesSettingsService.getList(adminRolesSettings);//先把父级功能找出来
			for(AdminRolesSettings p:parentList){
				SelectItem pItem =new SelectItem();//项
				pItem.setId(p.getId());
				pItem.setName(p.getName());
				CheckBoxTreeNode pNode = new CheckBoxTreeNode(pItem);//父级树节点
				
				//构造查找子级功能项的条件
				AdminRolesSettings cAdminRolesSettings=new AdminRolesSettings();
				cAdminRolesSettings.setAdminRolesSettings(p);
				
				//通过条件去查找
				List<AdminRolesSettings> childList = adminRolesSettingsService.getList(cAdminRolesSettings);//把它的子功能找出来
				for(AdminRolesSettings c:childList){
					SelectItem cItem =new SelectItem();
					cItem.setId(c.getId());
					cItem.setName(c.getName());
					
					CheckBoxTreeNode cNode = new CheckBoxTreeNode(cItem);//子级树节点
					pNode.add(cNode);//将子级放父级树节点中
				}
				
				rootNode.add(pNode); //将父级树节放入树的根节点中
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		DefaultTreeModel model = new DefaultTreeModel(rootNode); 
        tree.addMouseListener(new CheckBoxTreeNodeSelectionListener()); 
        tree.setModel(model);
        tree.setCellRenderer(new CheckBoxTreeCellRenderer()); 
	}
	
	/*
	 * 保存按钮的监听器类
	 */
	class MyActionListener implements ActionListener {
		/**
		 * 当按钮被点击时调用此方法
		 */
		public void actionPerformed(ActionEvent e) {
			MessageDialog messageDialog=new MessageDialog();//创建信息提示框
			String roleName = roleNameTxf.getText();//角色名称
			String remark = remarkTxf.getText();//备注
			String rolePrivelege="";//功能项
			//Date createDate=new Date(System.currentTimeMillis());//创建日期
			
			if(roleName==null||"".equals(roleName)){
				messageDialog.setMsg("名称不能为空");
				messageDialog.setVisible(true);//设置提示框为可见
			}
			
			for(int i=0;i<rootNode.getChildCount();i++){
				//二级节点
				CheckBoxTreeNode childNode =(CheckBoxTreeNode)rootNode.getChildAt(i);
				if(childNode.isSelected){
					SelectItem childItem=(SelectItem)childNode.getUserObject();
					rolePrivelege+=childItem.getId()+",";//拼接功能项的编号
				}
				
				for(int j=0;j<childNode.getChildCount();j++){
					//三级节点
					CheckBoxTreeNode cNode =(CheckBoxTreeNode)childNode.getChildAt(j);
					if(cNode.isSelected){
						SelectItem cItem=(SelectItem)cNode.getUserObject();
						rolePrivelege+=cItem.getId()+",";//拼接功能项的编号
					}
				}
			}
			
			if(rolePrivelege.length()>0){//把最后一个逗号去掉
				rolePrivelege=rolePrivelege.substring(0, rolePrivelege.length()-1);
			}
			
			AdminRoles adminRoles =new AdminRoles();//构建实体类对象
			adminRoles.setId(oldAdminRoles.getId());
			adminRoles.setRole_name(roleName);
			adminRoles.setRole_privelege(rolePrivelege);
			adminRoles.setRemark(remark);
			adminRoles.setCreate_date(oldAdminRoles.getCreate_date());
			
			try {
				//从容器中获取业务逻辑层对象
				AdminRolesService adminRolesService=Main.lookUp("adminRolesService");
				boolean flag = adminRolesService.update(adminRoles);//调用业务逻辑层的方法添加对象到数据库
				
				if (flag) {
					messageDialog.setMsg("修改成功");
					messageDialog.setVisible(true);//设置提示框为可见
					UpdateAdminRolesDialog.this.dispose();//销毁添加窗口
				}else{
					messageDialog.setMsg("修改失败");
					messageDialog.setVisible(true);//设置提示框为可见
				}
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	};
}
