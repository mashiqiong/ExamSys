package com.examsys.view;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JCheckBox;
import javax.swing.JSplitPane;
import javax.swing.JSeparator;
/**
 * 试卷章节面板
 * @author edu-1
 *
 */
public class PaperSectionPanel extends JPanel {
	private JTextField sectionNameTxf;//
	private JTextField perScoreTxf;
	private JTextField remarkTxf;
	private JCheckBox deleteCkb;
	private JSeparator separator;
	private Integer sectionId;//存放章节编号
	
	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	public JCheckBox getDeleteCkb() {
		return deleteCkb;
	}

	public JTextField getSectionNameTxf() {
		return sectionNameTxf;
	}

	public JTextField getPerScoreTxf() {
		return perScoreTxf;
	}

	public JTextField getRemarkTxf() {
		return remarkTxf;
	}

	/**
	 * Create the panel.
	 */
	public PaperSectionPanel() {
		
		JLabel label = new JLabel("章节名称：");
		
		sectionNameTxf = new JTextField();
		sectionNameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("本章节分数：");
		
		perScoreTxf = new JTextField();
		perScoreTxf.setColumns(10);
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		
		JLabel label_2 = new JLabel("备注：");
		
		 deleteCkb = new JCheckBox("勾选删除");
		
		separator = new JSeparator();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(2)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(26)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(label)
								.addComponent(label_1)
								.addComponent(label_2))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(remarkTxf)
								.addComponent(perScoreTxf)
								.addComponent(sectionNameTxf, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(deleteCkb))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(separator, GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(sectionNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(32)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(perScoreTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(deleteCkb))
					.addGap(33)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(label_2)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		setLayout(groupLayout);

	}
}
