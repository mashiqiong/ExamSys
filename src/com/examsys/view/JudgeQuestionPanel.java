package com.examsys.view;

import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
/**
 * 
 * 判断题
 * @author edu-1
 *
 */
public class JudgeQuestionPanel extends JPanel {

	private JTextArea contentTaa;//题干内容文本域
	private JTextArea keyDescTaa;//试题解析文本域
	private ButtonGroup buttonGroup;
	
	public ButtonGroup getButtonGroup() {
		return buttonGroup;
	}

	public JTextArea getContentTaa() {
		return contentTaa;
	}

	public JTextArea getKeyDescTaa() {
		return keyDescTaa;
	}

	/**
	 * Create the panel.
	 */
	public JudgeQuestionPanel() {
		
		JLabel label = new JLabel("题干内容：");
		
		contentTaa = new JTextArea();
		buttonGroup=new ButtonGroup();
		
		JRadioButton trueBtn = new JRadioButton("正确");
		trueBtn.setActionCommand("true");
		trueBtn.setSelected(true);
		
		JRadioButton falseBtn = new JRadioButton("错误");
		falseBtn.setActionCommand("false");
		
		buttonGroup.add(trueBtn);
		buttonGroup.add(falseBtn);
		
		JLabel label_1 = new JLabel("标准答案：");
		
		JLabel label_2 = new JLabel("试题解析：");
		
		keyDescTaa = new JTextArea();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(25)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label_2)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(keyDescTaa, GroupLayout.PREFERRED_SIZE, 706, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(label)
								.addComponent(label_1))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(trueBtn)
									.addGap(45)
									.addComponent(falseBtn))
								.addComponent(contentTaa, GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE))))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(label))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)))
					.addGap(32)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(trueBtn)
						.addComponent(falseBtn)
						.addComponent(label_1))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(70)
							.addComponent(label_2))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(36)
							.addComponent(keyDescTaa, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(251, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}

}
