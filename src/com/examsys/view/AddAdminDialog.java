package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.Admin;
import com.examsys.po.AdminRoles;
import com.examsys.service.AdminRolesService;
import com.examsys.service.AdminService;
import com.examsys.service.Main;
/**
 * 添加管理员的对话框
 * @author edu-1
 *
 */
public class AddAdminDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField userNameTxf;//用户名输入框
	private JPasswordField passwordPdf;//密码框
	private JTextField phoneTxf;//手机号码输入框
	private JTextField remarkTxf;//备注输入框
	private JComboBox rolesCbb;//角色选择下拉框
	private JComboBox statusCbb;//状态选择下拉框
	
	/**
	 * Create the dialog.
	 */
	public AddAdminDialog() {
		setModal(true);
		setTitle("添加管理员");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("用户名：");
		
		userNameTxf = new JTextField();
		userNameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("密码：");
		
		passwordPdf = new JPasswordField();
		
		JLabel label_2 = new JLabel("角色：");
		
		rolesCbb = new JComboBox();
		
		JLabel label_3 = new JLabel("手机号码：");
		
		phoneTxf = new JTextField();
		phoneTxf.setColumns(10);
		
		JLabel label_4 = new JLabel("状态：");
		
		statusCbb = new JComboBox();
		statusCbb.setModel(new DefaultComboBoxModel(new String[] {"启用", "禁用"}));
		
		JLabel label_5 = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(label)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(label_4)
							.addComponent(label_2)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(userNameTxf, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
						.addComponent(rolesCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_3, Alignment.TRAILING)
						.addComponent(label_1, Alignment.TRAILING)
						.addComponent(label_5, Alignment.TRAILING))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(remarkTxf, 135, 135, Short.MAX_VALUE)
						.addComponent(passwordPdf, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
						.addComponent(phoneTxf))
					.addContainerGap(36, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(userNameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1)
						.addComponent(passwordPdf, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(rolesCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3)
						.addComponent(phoneTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(33)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_4)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(95, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建保存按钮的监听器对象
		ActionListener saveActionListener =new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮绑定监听器
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		
		JButton cancelButton = new JButton("取消");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAdminDialog.this.dispose();//销毁窗口
			}
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		
		initRolesCbb();//调用初始化角色下拉框的方法
	}
	
	/**
	 * 初始化角色下拉框的方法
	 */
	private void initRolesCbb(){
		try {
			//从容器中拿业务逻辑层对象
			AdminRolesService adminRolesService=Main.lookUp("adminRolesService");
			List<AdminRoles> list = adminRolesService.getList();//调用业务逻辑层对象的方法拿数据
			for(AdminRoles adminRoles:list){
				//构造下拉框项
				SelectItem item=new SelectItem();
				item.setId(adminRoles.getId());
				item.setName(adminRoles.getRole_name());
				
				rolesCbb.addItem(item);//把角色项添加到下拉框
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 保存按钮的监听器类
	 * @author edu-1
	 *
	 */
	class SaveActionListener implements ActionListener {
		/**
		 * 当按钮被点击时执行此方法
		 */
		public void actionPerformed(ActionEvent e) {
			String userName = userNameTxf.getText();//用户名
			String password=new String(passwordPdf.getPassword());//密码
			String phone = phoneTxf.getText();//电话号码
			String remark = remarkTxf.getText();//备注
			String status=(String)statusCbb.getSelectedItem();//状态
			SelectItem roles =(SelectItem)rolesCbb.getSelectedItem();//角色
			
			MessageDialog messageDialog=new MessageDialog();//创建信息提示框对象
			
			//验证用户名
			if(userName==null||"".equals(userName)){
				messageDialog.setMsg("用户不能空");
				messageDialog.setVisible(true);//设置信息提示框可见
				return;
			}
			
			//验证密码
			if(password==null||"".equals(password)){
				messageDialog.setMsg("密码不能空");
				messageDialog.setVisible(true);//设置信息提示框可见
				return;
			}
			
			Admin admin=new Admin();//创建管理员对象
			admin.setUser_name(userName);//用户名
			admin.setPhone(phone);//电话号码
			admin.setUser_pass(password);//密码
			admin.setRemark(remark);//备注
			admin.setStatus(status.equals("禁用")?"0":"1");//状态
			//创建角色对象
			AdminRoles adminRoles=new AdminRoles();
			adminRoles.setId(roles.getId());
			admin.setAdminRoles(adminRoles);
			
			//获取当前系统日期为管理员的创建日期
			Date create_date = new Date(System.currentTimeMillis());
			admin.setCreate_date(create_date);//创建日期
			admin.setLogin_date(create_date);//登录日期
			admin.setLogin_times(0);//登录次数
			
			try {
				//从容器中拿业务逻辑层对象
				AdminService adminService=Main.lookUp("adminService");
				boolean flag = adminService.add(admin);//调用业务逻辑层添加角色对象到数据库
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);//设置信息提示框可见
					dispose();//销毁窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);//设置信息提示框可见
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

}
