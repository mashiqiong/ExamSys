package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.Users;
import com.examsys.service.Main;
import com.examsys.service.UsersService;
/**
 * 会员管理面板
 * @author edu-1
 *
 */
public class UsersManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public UsersManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("会员名：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建监听器类对象
		ActionListener queryActionListener=new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);//按钮绑定监听器对象 
		
		JButton addBtn = new JButton("添加");
		//创建监听器类对象
		ActionListener addActionListener=new AddActionListener();
		addBtn.addActionListener(addActionListener);//按钮绑定监听器对象 
		
		JButton updateBtn = new JButton("修改");
		//创建监听器类对象
		ActionListener updateActionListener=new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//按钮绑定监听器对象 
		
		JButton deleteBtn = new JButton("删除");
		//创建监听器类对象
		ActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//按钮绑定监听器对象 
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn))
					.addContainerGap(18, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//设置自动创建表格排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		
		initDatas();//调用初始化表格数据的方法
	}
	
	/**
	 * 添加按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AddUsersDialog addUsersDialog=new AddUsersDialog();
			addUsersDialog.setVisible(true);
			initDatas();//刷新表格数据
		}
	}
	
	/**
	 * 修改按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					UsersService usersService=Main.lookUp("usersService");
					Users users = usersService.get(id);//通过编号找到对象
					
					//创建修改系统参数对话框对象
					UpdateUsersDialog updateUserGroupsDialog=new UpdateUsersDialog();
					updateUserGroupsDialog.setOldUsers(users);//把实体类对象传给对话框用于显示与修改
					updateUserGroupsDialog.setVisible(true);
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	
	/**
	 * 删除按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					UsersService usersService=Main.lookUp("usersService");
					boolean flag = usersService.delete(id);//通过 编号删除对象
					//创建信息提示框
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
					
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	
	/**
	 * 查询按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class QueryActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			Object[][] datas = new Object[][] {
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null, null},
			};
			
			String[] columnNames = new String[] {
				"编号", "会员组", "会员名", "密码", "学号", "真实姓名", "邮箱", "手机号码", "注册时间", "登录时间", "登录次数", "状态", "备注"
			};
			
			//构造查询条件
			String userName = keywordTxf.getText();
			Users users1=new Users();//创建对象
			users1.setUser_name(userName);
			
			try {
				//从容器中获取业务逻辑层对象
				UsersService usersService=Main.lookUp("usersService");
				List<Users> list = usersService.getList(users1);//调用业务逻辑层对象获取表中数据
				
				if(list.size()>0){
					datas = new Object[list.size()][13];
					for(int i=0;i<list.size();i++){
						Users users = list.get(i);//从集合中拿出对象
						datas[i][0]=users.getId();//编号
						datas[i][1]=users.getUserGroups().getGroup_name();//会员组
						datas[i][2]=users.getUser_name();//会员名
						datas[i][3]=users.getUser_pass();//密码
						datas[i][4]=users.getUser_no();//学号
						datas[i][5]=users.getReal_name();//真实姓名
						datas[i][6]=users.getEmail();//邮箱
						datas[i][7]=users.getPhone();//手机号码
						datas[i][8]=users.getCreate_date();//注册时间
						datas[i][9]=users.getLogin_date();//登录时间
						datas[i][10]=users.getLogin_times();//登录次数
						datas[i][11]=users.getStatus();//状态
						datas[i][12]=users.getRemark();//备注
					}
				}
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
	}
	
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "会员组", "会员名", "密码", "学号", "真实姓名", "邮箱", "手机号码", "注册时间", "登录时间", "登录次数", "状态", "备注"
		};
		
		try {
			//从容器中获取业务逻辑层对象
			UsersService usersService=Main.lookUp("usersService");
			List<Users> list = usersService.getList();//调用业务逻辑层对象获取表中数据
			
			if(list.size()>0){
				datas = new Object[list.size()][13];
				for(int i=0;i<list.size();i++){
					Users users = list.get(i);//从集合中拿出对象
					datas[i][0]=users.getId();//编号
					datas[i][1]=users.getUserGroups().getGroup_name();//会员组
					datas[i][2]=users.getUser_name();//会员名
					datas[i][3]=users.getUser_pass();//密码
					datas[i][4]=users.getUser_no();//学号
					datas[i][5]=users.getReal_name();//真实姓名
					datas[i][6]=users.getEmail();//邮箱
					datas[i][7]=users.getPhone();//手机号码
					datas[i][8]=users.getCreate_date();//注册时间
					datas[i][9]=users.getLogin_date();//登录时间
					datas[i][10]=users.getLogin_times();//登录次数
					datas[i][11]=users.getStatus();//状态
					datas[i][12]=users.getRemark();//备注
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));
	}

}
