package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.Paper;
import com.examsys.po.PaperUserGroup;
import com.examsys.po.UserGroups;
import com.examsys.service.Main;
import com.examsys.service.PaperUserGroupService;
import com.examsys.service.UserGroupsService;
/**
 * 添加试卷用户组的窗口
 * @author mashiqiong
 *
 */
public class AddPaperUserGroupDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable paperUserGroupTable;
	private JTable userGroupTable;

	private Paper paper;//试卷实体类对象
	
	private String notInIds="";//用于存放排除用户组编号
	
	public void setPaper(Paper paper) {
		this.paper=paper;
		initPaperUserGroupTableDatas();//初始化试卷用户组表格数据的方法
		initUserGroupTableDatas();//初始化用户组表格数据的方法
	}

	/**
	 * Create the dialog.
	 */
	public AddPaperUserGroupDialog() {
		setModal(true);
		setTitle("添加参与考试的用户");
		setBounds(100, 100, 645, 406);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JScrollPane paperUserGroupScrollPane = new JScrollPane();
		paperUserGroupScrollPane.setToolTipText("参与考试的用户组");
		
		JScrollPane userGroupScrollPane = new JScrollPane();
		userGroupScrollPane.setToolTipText("待添加到可参与考试的用户组");
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(paperUserGroupScrollPane, GroupLayout.PREFERRED_SIZE, 297, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(userGroupScrollPane, GroupLayout.PREFERRED_SIZE, 317, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(paperUserGroupScrollPane, GroupLayout.PREFERRED_SIZE, 323, GroupLayout.PREFERRED_SIZE)
						.addComponent(userGroupScrollPane, GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton addBtn = new JButton("添加");
		//创建监听器对象
		ActionListener addActionListener = new AddActionListener();
		addBtn.addActionListener(addActionListener);//给按钮绑定监听器
		
		addBtn.setActionCommand("OK");
		getRootPane().setDefaultButton(addBtn);
		
		JButton deleteBtn = new JButton("删除");
		//创建监听器对象
		ActionListener deleteActionListener = new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给按钮绑定监听器
		
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddPaperUserGroupDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
		gl_buttonPane.setHorizontalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addGap(224)
					.addComponent(addBtn)
					.addGap(5)
					.addComponent(deleteBtn)
					.addGap(5)
					.addComponent(cancelBtn)
					.addContainerGap(224, Short.MAX_VALUE))
		);
		gl_buttonPane.setVerticalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_buttonPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_buttonPane.createParallelGroup(Alignment.LEADING)
						.addComponent(addBtn)
						.addComponent(deleteBtn)
						.addComponent(cancelBtn))
					.addContainerGap())
		);
		buttonPane.setLayout(gl_buttonPane);
		
		userGroupTable = new JTable();//试卷用户组表格
		userGroupTable.setAutoCreateRowSorter(true);//设置自动创建排序器
		userGroupScrollPane.setViewportView(userGroupTable);
		
		paperUserGroupTable = new JTable();//用户组表格
		paperUserGroupTable.setAutoCreateRowSorter(true);//设置自动创建排序器
		paperUserGroupScrollPane.setViewportView(paperUserGroupTable);
	}
	
	/**
	 * 初始化试卷用户组表格数据的方法
	 */
	private void initPaperUserGroupTableDatas(){
		
		Object[][] datas = new Object[][] {
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "试卷", "用户组"
		};
		
		//创建查询条件
		PaperUserGroup paperUserGroup1=new PaperUserGroup();
		paperUserGroup1.setPaper(paper);//关联试卷
		
		try {
			//从容器里获得业务逻辑层对象
			PaperUserGroupService paperUserGroupService=Main.lookUp("paperUserGroupService");
			List<PaperUserGroup> list = paperUserGroupService.getList(paperUserGroup1);//调用务逻辑层对象的方法获取数据
			notInIds="";
			if(list.size()>0){
				datas = new Object[list.size()][3];
				for(int i=0;i<list.size();i++){
					PaperUserGroup paperUserGroup = list.get(i);
					datas[i][0]=paperUserGroup.getId();//编号
					datas[i][1]=paperUserGroup.getPaper().getPaper_name();//试卷
					datas[i][2]=paperUserGroup.getUserGroups().getGroup_name();//用户组
					notInIds+=paperUserGroup.getUserGroups().getId()+",";
				}
				
				//构造试题编号，用于not in语句
				if(notInIds.length()>0){
					notInIds=notInIds.substring(0, notInIds.length()-1);
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		paperUserGroupTable.setModel(new DefaultTableModel(datas,columnNames));
	}

	/**
	 * 初始化用户组表格数据的方法
	 */
	private void initUserGroupTableDatas(){
		Object[][] datas = new Object[][] {
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
			{null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "用户组", "备注"
		};
		
		
		try {
			//从容器里获得业务逻辑层对象
			UserGroupsService userGroupsService=Main.lookUp("userGroupsService");
			List<UserGroups> list = userGroupsService.getList(notInIds);//调用务逻辑层对象的方法获取数据
			if(list.size()>0){
				datas = new Object[list.size()][3];
				for(int i=0;i<list.size();i++){
					UserGroups userGroups = list.get(i);
					datas[i][0]=userGroups.getId();//编号
					datas[i][1]=userGroups.getGroup_name();//用户组
					datas[i][2]=userGroups.getRemark();//备注
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		userGroupTable.setModel(new DefaultTableModel(datas,columnNames));
	}
	
	 /**
     * 添加按钮的监听器类
     * @author edu-1
     *
     */
    private class AddActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = userGroupTable.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = userGroupTable.getSelectedRows();//当前选择行号数组
			
			List<PaperUserGroup> list=new ArrayList<PaperUserGroup>();//创建集合用于存放实体类对象
			
			if(selectedRows!=null&&selectedRows.length>0){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				//创建试卷用户组对象
				PaperUserGroup paperUserGroup=new PaperUserGroup();
				//创建用户组对象
				UserGroups userGroups=new UserGroups();
				userGroups.setId(id);
				
				paperUserGroup.setPaper(paper);//关联试卷对象
				paperUserGroup.setUserGroups(userGroups);//关联用户组对象
				
				list.add(paperUserGroup);//将对象添加到集合中
				
			}
			
			try {
				//从容器里获得业务逻辑层对象
				PaperUserGroupService paperUserGroupService=Main.lookUp("paperUserGroupService");
				boolean flag = paperUserGroupService.add(list);//调用对象
				//创建提示信息框对象
				MessageDialog messageDialog=new MessageDialog();
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					initPaperUserGroupTableDatas();//调用初始试卷题目表格的方法
					initUserGroupTableDatas();//调用初始试题表格的方法
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
    
    /**
     * 删除按钮的监听器类
     * @author edu-1
     *
     */
    private class DeleteActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = paperUserGroupTable.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = paperUserGroupTable.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				System.out.println("----------------------"+id);
				try {
					//从容器里获得业务逻辑层对象
					PaperUserGroupService paperUserGroupService=Main.lookUp("paperUserGroupService");
					boolean flag = paperUserGroupService.delete(id);//调用务逻辑层对象的方法删除数据
					//创建提示信息框对象
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
						initPaperUserGroupTableDatas();//调用初始试卷题目表格的方法
						initUserGroupTableDatas();//调用初始试题表格的方法
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
			
		}
	}
}
