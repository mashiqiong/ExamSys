package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.Question;
import com.examsys.service.Main;
import com.examsys.service.QuestionService;
/**
 * 试题管理面板
 * @author edu-1
 *
 */
public class QuestionManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public QuestionManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblNewLabel = new JLabel("题干内容：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建添加按钮监听器对象
		ActionListener queryActionListener=new QueryActionlistener();
		queryBtn.addActionListener(queryActionListener);
		
		JButton addBtn = new JButton("添加");//给按钮绑定监听器
		//创建添加按钮监听器对象
		ActionListener addActionListener = new AddActionListener();
		addBtn.addActionListener(addActionListener);//给按钮绑定监听器
		
		JButton updateBtn = new JButton("修改");
		ActionListener updateActionListener = new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);
		
		JButton deleteBtn = new JButton("删除");
		ActionListener deleteActionListener = new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn))
					.addContainerGap(23, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//设置自动创建排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		
		initDatas();//调用初始化表格数据的方法

	}
	
	/**
	 * 添加按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//创建添加试题对话框对象
			AddQuestionDialog addQuestionDialog=new AddQuestionDialog();
			addQuestionDialog.setVisible(true);
			initDatas();//刷新表格数据
		}
	}
	
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					QuestionService questionService = Main.lookUp("questionService");
					Question question = questionService.get(id);//通过编号找到对象
					
					//创建添加试题对话框对象
					UpdateQuestionDialog updateQuestionDialog=new UpdateQuestionDialog();
					updateQuestionDialog.setOldQuestion(question);//把实体类对象传给对话框用于显示与修改
					updateQuestionDialog.setVisible(true);
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
			
		}
	}
	
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				try {
					QuestionService questionService=Main.lookUp("questionService");
					boolean flag = questionService.delete(id);//通过 编号删除对象
					//创建信息提示框
					MessageDialog messageDialog=new MessageDialog();
					if(flag){
						messageDialog.setMsg("删除成功");
						messageDialog.setVisible(true);
					}else{
						messageDialog.setMsg("删除失败");
						messageDialog.setVisible(true);
					}
					
					initDatas();//刷新表格数据
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	
	/**
	 * 查询按钮的监听器
	 * @author edu-1
	 *
	 */
	private class QueryActionlistener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object[][] datas = new Object[][] {
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
			};
			
			String[] columnNames = new String[] {
				"编号", "题库", "管理员", "试题类型", "难度级别", "来源", "题干内容", "标准答案", "试题解析", "创建时间", "状态", "备注"
			};
			
			//构造查询条件对象
			String content = keywordTxf.getText();
			Question question1=new Question();
			question1.setContent(content);
			
			try {
				//从容器中获取业务逻辑层对象
				QuestionService questionService=Main.lookUp("questionService");
				List<Question> list = questionService.getList(question1);//调用业务逻辑层对象的方法获取数据
				
				if(list.size()>0){//如果拿到数据
					datas = new Object[list.size()][12];
					for(int i=0;i<list.size();i++){
						Question question = list.get(i);//从集合中拿对象
						datas[i][0]=question.getId();//编号
						datas[i][1]=question.getQuestionDb().getName();//题库
						datas[i][2]=question.getAdmin().getUser_name();//管理员
						switch(question.getQtype()){//题型判断
							case 1:
								datas[i][3]="单选";
								break;
							case 2:
								datas[i][3]="多选";
								break;
							case 3:
								datas[i][3]="填空";
								break;
							case 4:
								datas[i][3]="判断";
								break;
							case 5:
								datas[i][3]="问答";
								break;
						}
						
						switch(question.getQtype()){//难度级别
							case 1:
								datas[i][4]="易";
								break;
							case 2:
								datas[i][4]="正常";
								break;
							case 3:
								datas[i][4]="难";
								break;
							
						}
						
						datas[i][5]=question.getQfrom();//来源
						datas[i][6]=question.getContent();//题干内容
						datas[i][7]=question.getSkey();//标准答案
						datas[i][8]=question.getKey_desc();//试题解析
						datas[i][9]=question.getCreate_date();//创建时间
						datas[i][10]=question.getStatus().equals("0")?"不完全开放":"完全开放";//备注
						datas[i][11]=question.getRemark();//备注
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
		
	}
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "题库", "管理员", "试题类型", "难度级别", "来源", "题干内容", "标准答案", "试题解析", "创建时间", "状态", "备注"
		};
		
		try {
			//从容器中获取业务逻辑层对象
			QuestionService questionService=Main.lookUp("questionService");
			List<Question> list = questionService.getList();//调用业务逻辑层对象的方法获取数据
			
			if(list.size()>0){//如果拿到数据
				datas = new Object[list.size()][12];
				for(int i=0;i<list.size();i++){
					Question question = list.get(i);//从集合中拿对象
					datas[i][0]=question.getId();//编号
					datas[i][1]=question.getQuestionDb().getName();//题库
					datas[i][2]=question.getAdmin().getUser_name();//管理员
					switch(question.getQtype()){//题型判断
						case 1:
							datas[i][3]="单选";
							break;
						case 2:
							datas[i][3]="多选";
							break;
						case 3:
							datas[i][3]="填空";
							break;
						case 4:
							datas[i][3]="判断";
							break;
						case 5:
							datas[i][3]="问答";
							break;
					}
					
					switch(question.getQlevel()){//难度级别
						case 1:
							datas[i][4]="易";
							break;
						case 2:
							datas[i][4]="正常";
							break;
						case 3:
							datas[i][4]="难";
							break;
						
					}
					
					datas[i][5]=question.getQfrom();//来源
					datas[i][6]=question.getContent();//题干内容
					datas[i][7]=question.getSkey();//标准答案
					datas[i][8]=question.getKey_desc();//试题解析
					datas[i][9]=question.getCreate_date();//创建时间
					datas[i][10]=question.getStatus().equals("0")?"不完全开放":"完全开放";//备注
					datas[i][11]=question.getRemark();//备注
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));
	}

}
