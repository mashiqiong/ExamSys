package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.Question;
import com.examsys.po.QuestionDb;
import com.examsys.po.QuestionOptions;
import com.examsys.service.Main;
import com.examsys.service.QuestionDbService;
import com.examsys.service.QuestionService;
/**
 * 添加试题窗口
 * @author edu-1
 *
 */
public class AddQuestionDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField qfromTxf;//来源输入框
	private JPanel panel;
	private JComboBox questionDbCbb;//题库下拉框
	private JComboBox qTypeCbb;//题型下拉框
	private JComboBox qLevelCbb;//难易度下拉框
	private JComboBox statusCbb;//状态下拉框
	private JScrollPane scrollPane;//滚动条
	private JTextField remarkTxf;//备注
	/**
	 * Create the dialog.
	 */
	public AddQuestionDialog() {
		setModal(true);
		setTitle("添加试题");
		setBounds(100, 100, 847, 634);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("题库：");
		
		questionDbCbb = new JComboBox();
		
		JLabel label_1 = new JLabel("类型：");
		
		qTypeCbb = new JComboBox();
		//创建试题类型下拉框选择监听器对象
		ItemListener qTypeCbbItemListener = new QTypeCbbItemListener();
		qTypeCbb.addItemListener(qTypeCbbItemListener);//给下拉框绑定监听器
		
		JLabel label_2 = new JLabel("难度：");
		
		qLevelCbb = new JComboBox();
		
		JLabel label_3 = new JLabel("状态：");
		
		statusCbb = new JComboBox();
		statusCbb.setModel(new DefaultComboBoxModel(new String[] {"开放", "不开放"}));
		
		JLabel label_4 = new JLabel("来源：");
		
		qfromTxf = new JTextField();
		qfromTxf.setColumns(10);
		
		scrollPane = new JScrollPane();
		
		JLabel label_5 = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(label)
								.addComponent(label_2))
							.addPreferredGap(ComponentPlacement.RELATED, 218, Short.MAX_VALUE)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(questionDbCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addComponent(qLevelCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addPreferredGap(ComponentPlacement.RELATED, 279, Short.MAX_VALUE)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_1, Alignment.TRAILING)
								.addComponent(label_3, Alignment.TRAILING)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label_4)
							.addGap(18)
							.addComponent(qfromTxf, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 410, Short.MAX_VALUE)
							.addComponent(label_5)))
					.addGap(28)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(qTypeCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE))
					.addGap(40))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 811, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(questionDbCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(17)
							.addComponent(qLevelCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGap(3)
									.addComponent(label))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGap(6)
									.addComponent(label_1)))
							.addGap(20)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(label_2)
								.addComponent(label_3)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(qTypeCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(17)
							.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(21)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_4)
						.addComponent(qfromTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE))
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建保存按钮监听器对象
		ActionListener saveActionListener=new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮绑定监听器对象
		
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddQuestionDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		buttonPane.add(cancelBtn);
		
		initQuestionDbCbb();//调用初始题库下拉框的方法
		initQTypeCbb();//调用初始类型下拉框的方法
		initQLevelCbb();//调用初始难度下拉框的方法
	}
	
	/**
	 * 试题类型监听器
	 * @author edu-1
	 *
	 */
	private class QTypeCbbItemListener implements ItemListener{
		
        public void itemStateChanged(ItemEvent event){
        	if(event.getStateChange()==ItemEvent.SELECTED){
        		SelectItem selectItem=(SelectItem)event.getItem();
        		panel.removeAll();//清空题型面板
        		switch(selectItem.getId()){
	        		case 1:
	        			panel.add(new SingleQuestionPanel());//单选面板
	        			
						break;
					case 2:
	        			panel.add(new MultiQuestionPanel());//多选面板
						
						break;
					case 3:
	        			panel.add(new FillInQuestionPanel());//填空面板
						
						break;
					case 4:
						panel.add(new JudgeQuestionPanel());//判断面板
						break;
					case 5:
						panel.add(new QAnswerQuestionPanel());//问答面板
						break;
	        	}
        		
        		panel.updateUI();//刷新界面
        	}
        	
        }
           
    };
    
	/**
	 * 初始题库下拉框的方法
	 */
	private void initQuestionDbCbb(){
		try {
			//从容器中获取业务逻辑层
			QuestionDbService questionDbService=Main.lookUp("questionDbService");
			List<QuestionDb> list = questionDbService.getList();//调用业务逻辑层的方法获取数据
			
			for(QuestionDb questionDb:list){
				SelectItem selectItem=new SelectItem();
				selectItem.setId(questionDb.getId());
				selectItem.setName(questionDb.getName());
				questionDbCbb.addItem(selectItem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 初始类型下拉框的方法
	 */
	private void initQTypeCbb(){
		try {
			//从容器中获取业务逻辑层
			for(int i=1;i<6;i++){
				SelectItem selectItem=new SelectItem();
				selectItem.setId(i);
				switch(i){
					case 1:
						selectItem.setName("单选");
						break;
					case 2:
						selectItem.setName("多选");
						break;
					case 3:
						selectItem.setName("填空");
						break;
					case 4:
						selectItem.setName("判断");
						break;
					case 5:
						selectItem.setName("问答");
						break;
				}
				
				qTypeCbb.addItem(selectItem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 初始难度下拉框的方法
	 */
	private void initQLevelCbb(){
		try {
			//从容器中获取业务逻辑层
			for(int i=1;i<4;i++){
				SelectItem selectItem=new SelectItem();
				selectItem.setId(i);
				switch(i){
					case 1:
						selectItem.setName("易");
						break;
					case 2:
						selectItem.setName("正常");
						break;
					case 3:
						selectItem.setName("难");
						break;
				}
				
				qLevelCbb.addItem(selectItem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 保存按钮监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			SelectItem questionDbItem =(SelectItem) questionDbCbb.getSelectedItem();
			Integer questionDbId = questionDbItem.getId();//题库编号
			SelectItem qTypeItem=(SelectItem)qTypeCbb.getSelectedItem();
			Integer qTypeId = qTypeItem.getId();//题型编号
			SelectItem qLevelItem=(SelectItem)qLevelCbb.getSelectedItem();
			Integer qLevelId = qLevelItem.getId();//难易度等级
			String status = statusCbb.getSelectedItem().equals("开放")?"1":"0";//状态
			String qfrom = qfromTxf.getText();//来源
			String remark = remarkTxf.getText();//备注
			String content="";//题干活内容
			String keyDesc="";//试题解析
			String skey="";//标准答案
			List<QuestionOptions> questionOptionsList=new ArrayList<QuestionOptions>();//用于存放试题选项对象
			Component component = panel.getComponent(0);
			switch(qTypeId){
				case 1://单选
					SingleQuestionPanel singleQuestionPanel=(SingleQuestionPanel)component;
					content = singleQuestionPanel.getContentTaa().getText();//题干活内容
					keyDesc = singleQuestionPanel.getKeyDescTaa().getText();//试题解析
					Component[] components = singleQuestionPanel.getPanel().getComponents();
					for(int i=0;i<components.length;i++){
						SingleItemPanel singleItemPanel=(SingleItemPanel)components[i];
						String soption = singleItemPanel.getContentTaa().getText();//选项描述
						String salisa = singleItemPanel.getItemTxf().getText();//选项编号
						if(singleItemPanel.getItemRbt().isSelected()){//是否为标准答案
							skey=salisa;//如果是，则赋值给skey
						}
						
						//创建试题选项实体类对象
						QuestionOptions questionOptions=new QuestionOptions();
						questionOptions.setSalisa(salisa);
						questionOptions.setSoption(soption);
						questionOptionsList.add(questionOptions);
					}
					break;
				case 2://多选
					MultiQuestionPanel multiQuestionPanel=(MultiQuestionPanel)component;
					content = multiQuestionPanel.getContentTaa().getText();//题干活内容
					keyDesc = multiQuestionPanel.getKeyDescTaa().getText();//试题解析
					Component[] components1 = multiQuestionPanel.getPanel().getComponents();
					for(int i=0;i<components1.length;i++){
						MultiItemPanel multiItemPanel=(MultiItemPanel)components1[i];
						String soption = multiItemPanel.getContentTaa().getText();//选项描述
						String salisa = multiItemPanel.getItemTxf().getText();//选项编号
						if(multiItemPanel.getItemCkb().isSelected()){//是否为标准答案
							skey+=salisa+",";//如果是，则赋值给skey
						}
						
						//创建试题选项实体类对象
						QuestionOptions questionOptions=new QuestionOptions();
						questionOptions.setSalisa(salisa);
						questionOptions.setSoption(soption);
						questionOptionsList.add(questionOptions);
					}
					break;
				case 3:
					FillInQuestionPanel fillInQuestionPanel=(FillInQuestionPanel)component;
					content = fillInQuestionPanel.getContentTaa().getText();//题干活内容
					keyDesc = fillInQuestionPanel.getKeyDescTaa().getText();//试题解析
					Component[] components2 = fillInQuestionPanel.getPanel().getComponents();
					for(int i=0;i<components2.length;i++){
						FillInItemPanel fillInItemPanel=(FillInItemPanel)components2[i];
						String soption = "";//选项描述
						String salisa = fillInItemPanel.getContentTaa().getText();//填空项
						skey+=salisa+"___";//拼接项给skey
						
						//创建试题选项实体类对象
						QuestionOptions questionOptions=new QuestionOptions();
						questionOptions.setSalisa(salisa);
						questionOptions.setSoption(soption);
						questionOptionsList.add(questionOptions);
					}
					break;
				case 4://判断题
					JudgeQuestionPanel judgeQuestionPanel=(JudgeQuestionPanel)component;
					content = judgeQuestionPanel.getContentTaa().getText();//题干活内容
					keyDesc = judgeQuestionPanel.getKeyDescTaa().getText();//试题解析
					
					Enumeration<AbstractButton> elements = judgeQuestionPanel.getButtonGroup().getElements();
					while(elements.hasMoreElements()){
						JRadioButton button=(JRadioButton)elements.nextElement();
						if(button.isSelected()){
							skey=button.getActionCommand();//把里面存的true或false赋给skey
						}
						String soption = "";//选项描述
						String salisa = button.getActionCommand();//填空项
						
						//创建试题选项实体类对象
						QuestionOptions questionOptions=new QuestionOptions();
						questionOptions.setSalisa(salisa);
						questionOptions.setSoption(soption);
						questionOptionsList.add(questionOptions);
					}
					
					break;
				case 5://问答题
					QAnswerQuestionPanel qAnswerQuestionPanel=(QAnswerQuestionPanel)component;
					content = qAnswerQuestionPanel.getContentTaa().getText();//题干活内容
					keyDesc = qAnswerQuestionPanel.getKeyDescTaa().getText();//试题解析
					skey=qAnswerQuestionPanel.getKeyTaa().getText();//标准答案
					break;
			}
			
			//创建信息提示框
			MessageDialog messageDialog=new MessageDialog();
			
			if(content==null||content.equals("")){
				messageDialog.setMsg("请输入题干");
				messageDialog.setVisible(true);
				return;
			}
			
			if(skey==null||skey.equals("")){
				messageDialog.setMsg("请设置标准答案");
				messageDialog.setVisible(true);
				return;
			}
			
			if(skey.length()>0&&qTypeId==2){//多选题时
				skey=skey.substring(0, skey.length()-1);//把最后一个逗号去掉
			}
			
			if(skey.length()>0&&qTypeId==3){//填空题时
				skey=skey.substring(0, skey.length()-3);//把最后三个下划线去掉
			}
			
			//试题实体类对象
			Question question=new Question();
			question.setContent(content);//题干内容
			question.setRemark(remark);//备注
			question.setKey_desc(keyDesc);//试题解析
			question.setSkey(skey);//准答案
			question.setQtype(qTypeId);//题型
			question.setQlevel(qLevelId);//难度
			question.setStatus(status);//状态
			question.setQfrom(qfrom);//来源
			question.setAdmin(Main.admin);//关联管理员
			question.setCreate_date(new Date(System.currentTimeMillis()));//创建时间
			//创建题库对象
			QuestionDb questionDb=new QuestionDb();
			questionDb.setId(questionDbId);
			question.setQuestionDb(questionDb);//关联题库
			
			question.setQuestionOptionss(questionOptionsList);//把试题选项存入实体类对象
			
			try {
				//从容器中获取业务逻辑层对象
				QuestionService questionService=Main.lookUp("questionService");
				boolean flag = questionService.add(question);//调用业务逻辑层对象的方法保存数据
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					AddQuestionDialog.this.dispose();//销毁窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
