package com.examsys.view;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.AdminRoles;
import com.examsys.service.AdminRolesService;
import com.examsys.service.Main;

import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
/**
 * 角色管理面板类
 * @author edu-1
 *
 */
public class AdminRolesManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public AdminRolesManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("角色名称:");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		ActionListener queryActionListener = new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);
		
		JButton addBtn = new JButton("添加");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAdminRolesDialog addAdminRolesDialog=new AddAdminRolesDialog();
				addAdminRolesDialog.setVisible(true);
				
				initDatas();//刷新表格数据
			}
		});
		
		JButton updateBtn = new JButton("修改");
		//创建修改按钮监听器对象
		ActionListener updateActionListener = new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//给修改按钮绑定监听器
		
		JButton deleteBtn = new JButton("删除");
		//创建删除按钮监听器对象
		ActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给删除按钮绑定监听器
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 494, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn))
					.addGap(19))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);
		
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		
		initDatas();//调用初始化表格数据的方法

	}
	
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	class UpdateActionListener implements ActionListener {
		/**
		 * 当修改按钮被点击的时候执行此方法
		 */
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					AdminRolesService adminRolesService=Main.lookUp("adminRolesService");
					AdminRoles adminRoles = adminRolesService.get(id);//通过编号获得角色对象
					
					//创建修改角色对话框
					UpdateAdminRolesDialog adminRolesDialog=new UpdateAdminRolesDialog();
					adminRolesDialog.setOldAdminRoles(adminRoles);//将实体类对象传给对话显示数据
					adminRolesDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	};
	
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	class DeleteActionListener implements ActionListener {
		/**
		 * 当修改按钮被点击的时候执行此方法
		 */
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					AdminRolesService adminRolesService=Main.lookUp("adminRolesService");
					boolean flag = adminRolesService.delete(id);//通过编号删除角色对象
					MessageDialog messageDialog=new MessageDialog();//创建提示信息框对象
					if(flag){
						messageDialog.setMsg("删除成功");
					}else{
						messageDialog.setMsg("删除失败");
					}
					messageDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	};
	
	/**
	 * 查询按钮监听器类
	 * @author edu-1
	 *
	 */
	class QueryActionListener implements ActionListener{

		/**
		 * 当按钮被点击时候执行此方法
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			Object[][] datas = new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			};
			
			String[] columnNames = new String[] {
				"\u7F16\u53F7", "\u89D2\u8272\u540D\u79F0", "\u529F\u80FD\u9879", "\u521B\u5EFA\u65F6\u95F4", "\u5907\u6CE8"
			};
			
			try {
				//获得业务逻辑层对象
				AdminRolesService adminRolesService=Main.lookUp("adminRolesService");
				//构造查询对象
				AdminRoles qAdminRoles=new AdminRoles();
				qAdminRoles.setRole_name(keywordTxf.getText());//查询内容
				
				List<AdminRoles> list = adminRolesService.getList(qAdminRoles);//调用业务逻辑层对象的方法获取数据
				
				if(list.size()>0){
					datas = new Object[list.size()][5];
					int i=0;
					for(AdminRoles adminRoles:list){//从集合中取出数据
						datas[i][0]=adminRoles.getId();//编号
						datas[i][1]=adminRoles.getRole_name();//角色名称
						datas[i][2]=adminRoles.getRole_privelege();//功能项
						datas[i][3]=adminRoles.getCreate_date();//创建日期
						datas[i][4]=adminRoles.getRemark();//备注
						i++;
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
		
	}
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		Object[][] datas = new Object[][] {
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
			{null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"\u7F16\u53F7", "\u89D2\u8272\u540D\u79F0", "\u529F\u80FD\u9879", "\u521B\u5EFA\u65F6\u95F4", "\u5907\u6CE8"
		};
		
		try {
			//获得业务逻辑层对象
			AdminRolesService adminRolesService=Main.lookUp("adminRolesService");
			
			List<AdminRoles> list = adminRolesService.getList();//调用业务逻辑层对象的方法获取数据
			
			if(list.size()>0){
				datas = new Object[list.size()][5];
				int i=0;
				for(AdminRoles adminRoles:list){//从集合中取出数据
					datas[i][0]=adminRoles.getId();//编号
					datas[i][1]=adminRoles.getRole_name();//角色名称
					datas[i][2]=adminRoles.getRole_privelege();//功能项
					datas[i][3]=adminRoles.getCreate_date();//创建日期
					datas[i][4]=adminRoles.getRemark();//备注
					i++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		table.setModel(new DefaultTableModel(datas,columnNames));
	}

}
