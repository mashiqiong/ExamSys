package com.examsys.view;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
/**
 * 单选题
 * @author edu-1
 *
 */
public class SingleQuestionPanel extends JPanel {

	private JPanel panel;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private JTextArea keyDescTaa;//试题解析文本域
	private JTextArea contentTaa;//题干内容文本域
	
	/**
	 * 提供外部程序访问面板
	 * @return
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * 提供外部程序访问试题解析文本域
	 * @return
	 */
	public JTextArea getKeyDescTaa() {
		return keyDescTaa;
	}

	/**
	 * 提供外部程序访问题干内容文本域
	 * @return
	 */
	public JTextArea getContentTaa() {
		return contentTaa;
	}

	public ButtonGroup getButtonGroup() {
		return buttonGroup;
	}

	public void setButtonGroup(ButtonGroup buttonGroup) {
		this.buttonGroup = buttonGroup;
	}

	/**
	 * Create the panel.
	 */
	public SingleQuestionPanel() {
		
		JLabel label = new JLabel("题干内容:");
		
		contentTaa = new JTextArea();
		contentTaa.setRows(5);
		
		JButton addBtn = new JButton("添加选项");
		ActionListener addItemActionListener=new AddItemActionListener();
		addBtn.addActionListener(addItemActionListener);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JButton deleteBtn = new JButton("删除选项");
		ActionListener deleteItemActionListener=new DeleteItemActionListener();
		deleteBtn.addActionListener(deleteItemActionListener);
		
		JLabel label_1 = new JLabel("试题解析：");
		
		keyDescTaa = new JTextArea();
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(addBtn)
									.addGap(18)
									.addComponent(deleteBtn))
								.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 349, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 799, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(label_1)
							.addGap(1)
							.addComponent(keyDescTaa, GroupLayout.DEFAULT_SIZE, 733, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(contentTaa, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(addBtn)
						.addComponent(deleteBtn))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(35)
							.addComponent(label_1))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(16)
							.addComponent(keyDescTaa, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)))
					.addGap(18))
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));//把面反的布局设置为网格布局
		setLayout(groupLayout);

	}

	/**
	 * 添加选项按钮监听器类
	 * @author edu-1
	 *
	 */
	private class AddItemActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			//创建选项面板对象
			SingleItemPanel singleItemPanel=new SingleItemPanel();
			buttonGroup.add(singleItemPanel.getItemRbt());//把选项面板中的单选按钮放入按钮组中进行管理
			panel.add(singleItemPanel);
			panel.updateUI();//刷新界面
		}
	}
	
	/**
	 * 删除选项按钮监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteItemActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			Component[] components = panel.getComponents();
			for(int i=0;i<components.length;i++){
				//从数组中拿出选项面板对象
				SingleItemPanel singleItemPanel=(SingleItemPanel)components[i];
				if(singleItemPanel.getDeleteCkb().isSelected()){//如果此选面板中的复选框被选中则删除
					buttonGroup.remove(singleItemPanel.getItemRbt());//把选项面板中的单选按钮移除出按钮组中
					panel.remove(singleItemPanel);
				}
			}
			panel.updateUI();//刷新界面
		}
	}
}
