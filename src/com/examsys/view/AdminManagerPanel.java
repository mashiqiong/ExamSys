package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.Admin;
import com.examsys.service.AdminService;
import com.examsys.service.Main;
/**
 * 管理员管理面板
 * @author edu-1
 *
 */
public class AdminManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public AdminManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("用户名：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		//创建查询按钮监听器对象
		QueryActionListener queryActionListener=new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);//给查询按钮绑定监听器
		
		JButton addBtn = new JButton("添加");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//创建添加管理员对话框
				AddAdminDialog addAdminDialog=new AddAdminDialog();
				addAdminDialog.setVisible(true);
				initDatas();//刷新表格数据
			}
		});
		
		JButton updateBtn = new JButton("修改");
		//创建修改按钮监听器对象
		UpdateActionListener updateActionListener=new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);//给修改按钮绑定监听器
		
		JButton deleteBtn = new JButton("删除");
		//创建删除按钮监听器对象
		DeleteActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);//给删除按钮绑定监听器
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn))
					.addGap(20))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//自动创建排序器
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		initDatas();//调用初始化表格数据的方法
	}
	
	/**
	 * 修改按钮监听器类
	 * @author edu-1
	 *
	 */
	class UpdateActionListener implements ActionListener {
		/**
		 * 当修改按钮被点击的时候执行此方法
		 */
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					AdminService adminService=Main.lookUp("adminService");
					Admin admin = adminService.get(id);//通过编号获得角色对象
					
					//创建修改角色对话框
					UpdateAdminDialog adminDialog=new UpdateAdminDialog();
					adminDialog.setOldAdmin(admin);//将实体类对象传给对话显示数据
					adminDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	};
	
	/**
	 * 删除按钮监听器类
	 * @author edu-1
	 *
	 */
	class DeleteActionListener implements ActionListener {
		/**
		 * 当修改按钮被点击的时候执行此方法
		 */
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					AdminService adminService=Main.lookUp("adminService");
					boolean flag = adminService.delete(id);//通过编号删除角色对象
					MessageDialog messageDialog=new MessageDialog();//创建提示信息框对象
					if(flag){
						messageDialog.setMsg("删除成功");
					}else{
						messageDialog.setMsg("删除失败");
					}
					messageDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	};
	
	/**
	 * 查询按钮监听器类
	 * @author edu-1
	 *
	 */
	class QueryActionListener implements ActionListener{

		/**
		 * 当按钮被点击时候执行此方法
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			String[] columnNames = new String[] {
				"编号", "角色", "用户名", "密码", "手机号码", "登录次数", "创建时间", "最后登录日期", "状态","备注"
			};
			
			Object[][] datas = new Object[][] {
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
			};
			
			try {
				//获得业务逻辑层对象
				AdminService adminService=Main.lookUp("adminService");
				//构造查询对象
				Admin qAdmin=new Admin();
				qAdmin.setUser_name(keywordTxf.getText());//查询内容
				
				List<Admin> list = adminService.getList(qAdmin);//调用业务逻辑层对象的方法获取数据
				
				if(list.size()>0){
					datas = new Object[list.size()][10];
					for(int i=0;i<list.size();i++){//从集合中拿出数据
						Admin admin = list.get(i);
						datas[i][0]=admin.getId();//编号
						datas[i][1]=admin.getAdminRoles().getRole_name();//角色
						datas[i][2]=admin.getUser_name();//用户名
						datas[i][3]=admin.getUser_pass();//密码
						datas[i][4]=admin.getPhone();//手机号码
						datas[i][5]=admin.getLogin_times();//登录次数
						datas[i][6]=admin.getCreate_date();//创建时间
						datas[i][7]=admin.getLogin_date();//最后登录日期
						datas[i][8]=admin.getStatus().equals("0")?"禁用":"启用";//状态
						datas[i][9]=admin.getRemark();//备注
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
		
	}
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		String[] columnNames = new String[] {
			"编号", "角色", "用户名", "密码", "手机号码", "登录次数", "创建时间", "最后登录日期", "状态","备注"
		};
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null},
		};
		
		try {
			//从容器中获取业务逻辑层对象
			AdminService adminService=Main.lookUp("adminService");
			List<Admin> list = adminService.getList();//调用业务逻辑层对象的方法拿数据
			
			if(list.size()>0){
				datas = new Object[list.size()][10];
				for(int i=0;i<list.size();i++){//从集合中拿出数据
					Admin admin = list.get(i);
					datas[i][0]=admin.getId();//编号
					datas[i][1]=admin.getAdminRoles().getRole_name();//角色
					datas[i][2]=admin.getUser_name();//用户名
					datas[i][3]=admin.getUser_pass();//密码
					datas[i][4]=admin.getPhone();//手机号码
					datas[i][5]=admin.getLogin_times();//登录次数
					datas[i][6]=admin.getCreate_date();//创建时间
					datas[i][7]=admin.getLogin_date();//最后登录日期
					datas[i][8]=admin.getStatus().equals("0")?"禁用":"启用";//状态
					datas[i][9]=admin.getRemark();//备注
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));
	}
}
