package com.examsys.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.examsys.po.Paper;
import com.examsys.service.Main;
import com.examsys.service.PaperService;
/**
 * 试卷管理面板
 * @author edu-1
 *
 */
public class PaperManagerPanel extends JPanel {
	private JTable table;
	private JTextField keywordTxf;

	/**
	 * Create the panel.
	 */
	public PaperManagerPanel() {
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblNewLabel = new JLabel("试卷名称：");
		
		keywordTxf = new JTextField();
		keywordTxf.setColumns(10);
		
		JButton queryBtn = new JButton("查询");
		ActionListener queryActionListener=new QueryActionListener();
		queryBtn.addActionListener(queryActionListener);
		
		JButton addBtn = new JButton("添加");
		ActionListener addActionListener=new AddActionListener();
		addBtn.addActionListener(addActionListener);
		
		JButton updateBtn = new JButton("修改");
		ActionListener updateActionListener=new UpdateActionListener();
		updateBtn.addActionListener(updateActionListener);
		
		JButton deleteBtn = new JButton("删除");
		ActionListener deleteActionListener=new DeleteActionListener();
		deleteBtn.addActionListener(deleteActionListener);
		
		JButton addPerDetailBtn = new JButton("添加题目");
		ActionListener addPerDetailActionListener=new AddPerDetailActionListener();
		addPerDetailBtn.addActionListener(addPerDetailActionListener);
		
		JButton addUserGroupBtn = new JButton("添加用户组");
		ActionListener addUserGroupActionListener=new AddUserGroupActionListener();
		addUserGroupBtn.addActionListener(addUserGroupActionListener);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(queryBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(updateBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(deleteBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addPerDetailBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addUserGroupBtn)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(keywordTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(queryBtn)
						.addComponent(addBtn)
						.addComponent(updateBtn)
						.addComponent(deleteBtn)
						.addComponent(addPerDetailBtn)
						.addComponent(addUserGroupBtn))
					.addGap(23))
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);//设置自动创建行排序器
		
		scrollPane.setViewportView(table);
		setLayout(groupLayout);

		initDatas();//调用初始化表格数据的方法
	}
	
	/**
	 * 添加试卷题目按钮监听器类
	 * @author mashiqiong
	 *
	 */
	private class AddPerDetailActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					PaperService paperService=Main.lookUp("paperService");
					Paper paper = paperService.get(id);//通过编号获得角色对象
					
					//创建修改角色对话框
					AddPaperDetailDialog addPaperDetailDialog=new AddPaperDetailDialog();
					addPaperDetailDialog.setPaper(paper);//将实体类对象传给对话显示数据
					addPaperDetailDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
			
		}
	}
	
	/**
	 * 添加用户组按钮监听器类
	 * @author mashiqiong
	 *
	 */
	private class AddUserGroupActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					PaperService paperService=Main.lookUp("paperService");
					Paper paper = paperService.get(id);//通过编号获得角色对象
					
					//创建修改角色对话框
					AddPaperUserGroupDialog addPaperUserGroupDialog=new AddPaperUserGroupDialog();
					addPaperUserGroupDialog.setPaper(paper);//将实体类对象传给对话显示数据
					addPaperUserGroupDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
			
			
		}
	}
	
	/**
	 * 添加按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class AddActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//创建添加试卷窗口对象
			AddPaperDialog addPaperDialog=new AddPaperDialog();
			addPaperDialog.setVisible(true);
			initDatas();//刷新表格数据
		}
	}
	
	/**
	 * 修改按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class UpdateActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					PaperService paperService=Main.lookUp("paperService");
					Paper paper = paperService.get(id);//通过编号获得角色对象
					
					//创建修改角色对话框
					UpdatePaperDialog updatePaperDialog=new UpdatePaperDialog();
					updatePaperDialog.setOldPaper(paper);//将实体类对象传给对话显示数据
					updatePaperDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	
	/**
	 * 删除按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			RowSorter<? extends TableModel> rowSorter = table.getRowSorter();//拿到行排序器
			TableModel model = rowSorter.getModel();//表格的数据模型
			int[] selectedRows = table.getSelectedRows();//当前选择行号数组
			
			if(selectedRows!=null&&selectedRows.length>0&&selectedRows.length==1){
				int index = rowSorter.convertRowIndexToModel(selectedRows[0]);//转换成真正的行号
				Integer id = (Integer)model.getValueAt(index, 0);//获取选中第一列数据
				
				try {
					//从容器里获得业务逻辑层对象
					PaperService paperService=Main.lookUp("paperService");
					boolean flag = paperService.delete(id);//通过编号删除角色对象
					MessageDialog messageDialog=new MessageDialog();//创建提示信息框对象
					if(flag){
						messageDialog.setMsg("删除成功");
					}else{
						messageDialog.setMsg("删除失败");
					}
					messageDialog.setVisible(true);
					initDatas();//刷新表格数据
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				
			}
		}
	}
	
	/**
	 * 查询按钮的监听器类
	 * @author edu-1
	 *
	 */
	private class QueryActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Object[][] datas = new Object[][] {
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
			};
			
			String[] columnNames = new String[] {
				"编号", "管理员", "试卷名称", "开始考试时间", "结束考试时间", "考试总时间", "考试总分数", "提交时间", "公布成绩时间", "题目顺序", "试卷状态", "备注"
			};
			
			//创建查询条件
			String paper_name = keywordTxf.getText();//获得输入框的内容
			Paper paper1=new Paper();
			paper1.setPaper_name(paper_name);
			
			try {
				//从容器中获取业务逻辑层对象
				PaperService paperService=Main.lookUp("paperService");
				List<Paper> list = paperService.getList(paper1);//调用业务逻辑层对象的 方法获取数据
				
				if(list.size()>0){
					datas = new Object[list.size()][12];
					for(int i=0;i<list.size();i++){
						Paper paper = list.get(i);
						
						datas[i][0]=paper.getId();//编号
						datas[i][1]=paper.getAdmin().getUser_name();//管理员
						datas[i][2]=paper.getPaper_name();//试卷名称
						datas[i][3]=paper.getStart_time();//开始考试时间
						datas[i][4]=paper.getEnd_time();//结束考试时间
						datas[i][5]=paper.getPaper_minute();//考试总时间
						datas[i][6]=paper.getTotal_score();//考试总分数
						datas[i][7]=paper.getPost_date();//提交时间
						datas[i][8]=paper.getShow_score();//公布成绩时间
						datas[i][9]=paper.getQorder();//题目顺序
						datas[i][10]=paper.getStatus().equals("1")?"开放":"不开放";//试卷状态
						datas[i][11]=paper.getRemark();//备注
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			table.setModel(new DefaultTableModel(datas,columnNames));
		}
	}
	
	/**
	 * 初始化表格数据的方法
	 */
	private void initDatas(){
		
		Object[][] datas = new Object[][] {
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
			{null, null, null, null, null, null, null, null, null, null, null, null},
		};
		
		String[] columnNames = new String[] {
			"编号", "管理员", "试卷名称", "开始考试时间", "结束考试时间", "考试总时间", "考试总分数", "提交时间", "公布成绩时间", "题目顺序", "试卷状态", "备注"
		};
		
		try {
			//从容器中获取业务逻辑层对象
			PaperService paperService=Main.lookUp("paperService");
			List<Paper> list = paperService.getList();//调用业务逻辑层对象的 方法获取数据
			
			if(list.size()>0){
				datas = new Object[list.size()][12];
				for(int i=0;i<list.size();i++){
					Paper paper = list.get(i);
					
					datas[i][0]=paper.getId();//编号
					datas[i][1]=paper.getAdmin().getUser_name();//管理员
					datas[i][2]=paper.getPaper_name();//试卷名称
					datas[i][3]=paper.getStart_time();//开始考试时间
					datas[i][4]=paper.getEnd_time();//结束考试时间
					datas[i][5]=paper.getPaper_minute();//考试总时间
					datas[i][6]=paper.getTotal_score();//考试总分数
					datas[i][7]=paper.getPost_date();//提交时间
					datas[i][8]=paper.getShow_score();//公布成绩时间
					datas[i][9]=paper.getQorder();//题目顺序
					datas[i][10]=paper.getStatus().equals("1")?"开放":"不开放";//试卷状态
					datas[i][11]=paper.getRemark();//备注
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		table.setModel(new DefaultTableModel(datas,columnNames));
	}
}
