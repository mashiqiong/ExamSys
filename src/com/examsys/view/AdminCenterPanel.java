package com.examsys.view;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
/**
 * 中间面板
 * @author edu-1
 *
 */
public class AdminCenterPanel extends JPanel {

	private JTabbedPane tabbedPane;//卡片面板
	
	/**
	 * 供外部程序调用卡片面板
	 * @return
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	/**
	 * Create the panel.
	 */
	public AdminCenterPanel() {
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		AdminManagerPanel adminManagerPanel = new AdminManagerPanel();
		tabbedPane.addTab("管理员管理", null, adminManagerPanel, null);
		setLayout(groupLayout);

	}
}
