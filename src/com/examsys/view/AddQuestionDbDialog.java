package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.examsys.po.QuestionDb;
import com.examsys.service.Main;
import com.examsys.service.QuestionDbService;
/**
 * 添加题库窗口
 * @author edu-1
 *
 */
public class AddQuestionDbDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nameTxf;//题 库名称输入框
	private JTextField remarkTxf;//备注输入框
	private JComboBox statusCbb;//状态下拉框
	/**
	 * Create the dialog.
	 */
	public AddQuestionDbDialog() {
		setModal(true);
		setTitle("添加题库");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("题库名称：");
		
		nameTxf = new JTextField();
		nameTxf.setColumns(10);
		
		JLabel label_1 = new JLabel("状态：");
		
		statusCbb = new JComboBox();
		statusCbb.setModel(new DefaultComboBoxModel(new String[] {"正常", "锁定"}));
		
		JLabel label_2 = new JLabel("备注：");
		
		remarkTxf = new JTextField();
		remarkTxf.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label)
						.addComponent(label_1)
						.addComponent(label_2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(remarkTxf, GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
						.addComponent(nameTxf))
					.addContainerGap(230, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(nameTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(statusCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(remarkTxf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(91, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton saveBtn = new JButton("保存");
		//创建监听器对象
		ActionListener saveActionListener=new SaveActionListener();
		saveBtn.addActionListener(saveActionListener);//给按钮绑定监听器
		
		saveBtn.setActionCommand("OK");
		buttonPane.add(saveBtn);
		getRootPane().setDefaultButton(saveBtn);
		
		JButton cancelBtn = new JButton("取消");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddQuestionDbDialog.this.dispose();//销毁窗口
			}
		});
		cancelBtn.setActionCommand("Cancel");
		buttonPane.add(cancelBtn);
	}
	
	/**
	 * 保存按钮监听器类
	 * @author edu-1
	 *
	 */
	private class SaveActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			String name = nameTxf.getText();
			String status = (String)statusCbb.getSelectedItem();
			String remark = remarkTxf.getText();
			
			//创建信息提示框对象
			MessageDialog messageDialog=new MessageDialog();
			if(name==null||"".equals(name)){
				messageDialog.setMsg("题库名称不能为空");
				messageDialog.setVisible(true);
				return;
			}
			
			//创建实体类对象
			QuestionDb questionDb=new QuestionDb();
			questionDb.setName(name);
			questionDb.setRemark(remark);
			questionDb.setStatus(status.equals("正常")?"1":"-1");
			
			long currentTimeMillis = System.currentTimeMillis();//当前系统时间毫秒数
			Date create_date = new Date(currentTimeMillis);//创建日期对象
			questionDb.setCreate_date(create_date);//注册时间
			
			questionDb.setAdmin(Main.admin);//把当前登录的管理员对象存入这里
			
			try {
				//从容器中获取业务逻辑层对象
				QuestionDbService questionDbService=Main.lookUp("questionDbService");
				boolean flag = questionDbService.add(questionDb);//调用业务逻辑层对象的方法保存数据
				
				if(flag){
					messageDialog.setMsg("添加成功");
					messageDialog.setVisible(true);
					AddQuestionDbDialog.this.dispose();//销毁窗口
				}else{
					messageDialog.setMsg("添加失败");
					messageDialog.setVisible(true);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

}
