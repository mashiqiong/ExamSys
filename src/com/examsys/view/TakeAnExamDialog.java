package com.examsys.view;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.examsys.po.ExamMain;
import com.examsys.po.PaperDetail;
import com.examsys.po.PaperSection;
import com.examsys.po.Question;
import com.examsys.po.QuestionOptions;
import com.examsys.service.Main;
import com.examsys.service.QuestionService;
/**
 * 考生参与考试答题窗口
 * @author edu-1
 *
 */
public class TakeAnExamDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JPanel panel;
	private JComboBox paperSectionCbb;//章节下拉框
	private ExamMain examMain;//答题卡主实体类对象
	
	public void setExamMain(ExamMain examMain) {
		this.examMain = examMain;
		
		//从试卷对象中拿出章节列表
		List<PaperSection> paperSections = examMain.getPaper().getPaperSections();
		PaperSection paperSection = paperSections.get(0);//获得集合中的第一个章节
		
		//创建下拉框选中项对象
		SelectItem selectItem=new SelectItem();
		selectItem.setId(paperSection.getId());//编号
		selectItem.setName(paperSection.getSection_name());//章节名称，用于显示在下拉框中
		paperSectionCbb.setSelectedItem(selectItem);//设置下拉框中的默认选中章节
		
		List<PaperDetail> paperDetails = paperSection.getPaperDetails();
		PaperDetail paperDetail = paperDetails.get(0);
		Question question = paperDetail.getQuestion();
		
		try {
			QuestionService questionService=Main.lookUp("questionService");
			question = questionService.get(question.getId());
			
			initDatas(question);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 初始化答题卡界面
	 * @param question
	 */
	private void initDatas(Question question) {
		panel.removeAll();//清除面板中的内容
		//试题明细项集合
		List<QuestionOptions> questionOptionss = question.getQuestionOptionss();
		switch(question.getQtype()){//根据不同题型来显示下拉默认项内容
			case 1://单选题
				SingleQuestionPanel singleQuestionPanel = new SingleQuestionPanel();
				singleQuestionPanel.getContentTaa().setText(question.getContent());//题干活内容
				
				for(QuestionOptions qo:questionOptionss){//处理明细项
					//创建选项面板对象
					SingleItemPanel singleItemPanel=new SingleItemPanel();
					singleItemPanel.getItemTxf().setText(qo.getSalisa());//选项编号
					singleItemPanel.getContentTaa().setText(qo.getSoption());//选项描述
					if(question.getSkey().equals(qo.getSalisa())){//设置默认答案
						singleItemPanel.getItemRbt().setSelected(true);//设置单选按钮选中
					}
					singleQuestionPanel.getButtonGroup().add(singleItemPanel.getItemRbt());//把选项面板中的单选按钮放入按钮组中进行管理
					singleQuestionPanel.getPanel().add(singleItemPanel);
				}
				panel.add(singleQuestionPanel);//单选面板
				
				break;
			case 2://多选题
				//创建选项面板对象
				MultiQuestionPanel multiQuestionPanel = new MultiQuestionPanel();
				multiQuestionPanel.getContentTaa().setText(question.getContent());//题干活内容
				multiQuestionPanel.getKeyDescTaa().setText(question.getKey_desc());//试题解析
				
				for(QuestionOptions qo:questionOptionss){//处理明细项
					//创建选项面板对象
					MultiItemPanel multiItemPanel=new MultiItemPanel();
					multiItemPanel.getItemTxf().setText(qo.getSalisa());//选项编号
					multiItemPanel.getContentTaa().setText(qo.getSoption());//选项描述
					if(question.getSkey().contains(qo.getSalisa())){//如果标准答案中包含此项
						multiItemPanel.getItemCkb().setSelected(true);//设置复选框选中
					}
					multiQuestionPanel.getPanel().add(multiItemPanel);
				}
				panel.add(multiQuestionPanel);//多选面板
				
				break;
			case 3://真空题
				//创建选项面板对象
				FillInQuestionPanel fillInQuestionPanel = new FillInQuestionPanel();
				fillInQuestionPanel.getContentTaa().setText(question.getContent());//题干活内容
				fillInQuestionPanel.getKeyDescTaa().setText(question.getKey_desc());//试题解析
				
				for(QuestionOptions qo:questionOptionss){//处理明细项
					//创建选项面板对象
					FillInItemPanel fillInItemPanel=new FillInItemPanel();
					fillInItemPanel.getContentTaa().setText(qo.getSalisa());//答案项
					fillInQuestionPanel.getPanel().add(fillInItemPanel);
				}
				panel.add(fillInQuestionPanel);//填空面板
				
				break;
			case 4://判断题
				//创建选项面板对象
				JudgeQuestionPanel judgeQuestionPanel = new JudgeQuestionPanel();
				judgeQuestionPanel.getContentTaa().setText(question.getContent());//题干活内容
				judgeQuestionPanel.getKeyDescTaa().setText(question.getKey_desc());//试题解析
				
				Enumeration<AbstractButton> elements = judgeQuestionPanel.getButtonGroup().getElements();
				while(elements.hasMoreElements()){
					JRadioButton button=(JRadioButton)elements.nextElement();
					
					for(QuestionOptions qo:questionOptionss){//处理明细项
						if(question.getSkey().equals("true")
								&&button.getActionCommand().equals("true")){
							button.setSelected(true);//把里面存的true或false赋给skey
						}
						
						if(question.getSkey().equals("false")
								&&button.getActionCommand().equals("false")){
							button.setSelected(true);//把里面存的true或false赋给skey
						}
					}
				}
				
				panel.add(judgeQuestionPanel);//判断面板
				break;
			case 5://问答题
				//创建选项面板对象
				QAnswerQuestionPanel qAnswerQuestionPanel=new QAnswerQuestionPanel();
				qAnswerQuestionPanel.getContentTaa().setText(question.getContent());//题干活内容
				qAnswerQuestionPanel.getKeyDescTaa().setText(question.getKey_desc());//试题解析
				qAnswerQuestionPanel.getKeyTaa().setText(question.getSkey());//标准答案
				panel.add(qAnswerQuestionPanel);//问答面板
				break;
		}

		panel.updateUI();//刷新界面
	}

	/**
	 * Create the dialog.
	 */
	public TakeAnExamDialog() {
		setTitle("考生答题主窗口");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		panel = new JPanel();
		scrollPane.setViewportView(panel);
		contentPanel.setLayout(gl_contentPanel);
		
		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton previousBtn = new JButton("上一题");
		
		JButton nextBtn = new JButton("下一题");
		
		JButton submitBtn = new JButton("提交");
		submitBtn.setActionCommand("OK");
		getRootPane().setDefaultButton(submitBtn);
		
		JButton cancelButton = new JButton("取消");
		cancelButton.setActionCommand("Cancel");
		
		JLabel label = new JLabel("章节：");
		
		paperSectionCbb = new JComboBox();
		
		GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
		gl_buttonPane.setHorizontalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addGap(220)
					.addComponent(label)
					.addGap(5)
					.addComponent(paperSectionCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(previousBtn)
					.addGap(5)
					.addComponent(nextBtn)
					.addGap(5)
					.addComponent(submitBtn)
					.addGap(5)
					.addComponent(cancelButton)
					.addContainerGap(219, Short.MAX_VALUE))
		);
		gl_buttonPane.setVerticalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_buttonPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_buttonPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(4)
							.addComponent(label))
						.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(1)
							.addComponent(paperSectionCbb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(previousBtn)
						.addComponent(nextBtn)
						.addComponent(submitBtn)
						.addComponent(cancelButton))
					.addContainerGap())
		);
		buttonPane.setLayout(gl_buttonPane);
	}
	
	/**
	 * 试题类型监听器
	 * @author edu-1
	 *
	 */
	private class PaperSectionCbbItemListener implements ItemListener{
		
        public void itemStateChanged(ItemEvent event){
        	if(event.getStateChange()==ItemEvent.SELECTED){
        		SelectItem selectItem=(SelectItem)event.getItem();
        		panel.removeAll();//清空题型面板
        		switch(selectItem.getId()){
	        		case 1:
	        			panel.add(new UserSingleQuestionPanel());//单选面板
	        			
						break;
					case 2:
	        			panel.add(new UserMultiQuestionPanel());//多选面板
						
						break;
					case 3:
	        			panel.add(new UserFillInQuestionPanel());//填空面板
						
						break;
					case 4:
						panel.add(new UserJudgeQuestionPanel());//判断面板
						break;
					case 5:
						panel.add(new UserQAnswerQuestionPanel());//问答面板
						break;
	        	}
        		
        		panel.updateUI();//刷新界面
        	}
        	
        }
           
    };
    
    /**
	 * 初始章节下拉框的方法
	 */
	private void initPaperSectionCbb(){
		try {
			//从容器中获取业务逻辑层
			List<PaperSection> paperSections = examMain.getPaper().getPaperSections();
			for(int i=0;i<paperSections.size();i++){
				PaperSection paperSection = paperSections.get(i);
				//创建章节下拉框选项
				SelectItem selectItem=new SelectItem();
				selectItem.setId(paperSection.getId());//编号
				selectItem.setName(paperSection.getSection_name());//章节名称，用于显示在下拉框中
				paperSectionCbb.addItem(selectItem);//将章节下拉框选项添加到下拉框中
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
