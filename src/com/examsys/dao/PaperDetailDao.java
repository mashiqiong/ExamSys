package com.examsys.dao;

import java.util.List;

import com.examsys.po.PaperDetail;

/**
 * 试卷明细数据访问层接口
 * @author edu-1
 *
 */
public interface PaperDetailDao extends IBaseDao<PaperDetail, Integer> {
	/**
	 * 通过试卷编号删除试卷明细
	 */
	public void deleteByPaperId(Integer id) throws Exception;
	
	/**
	 * 添加多条信息
	 * @param paperDetails
	 * @throws Exception
	 */
	public void add(List<PaperDetail> paperDetails) throws Exception;
}
