package com.examsys.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.Admin;
import com.examsys.po.AdminRoles;

/**
 * 管理员数据访问层实现类
 * @author edu-1
 *
 */
public class AdminDaoImpl extends AbstractBaseDao<Admin, Integer> implements AdminDao {

	/**
	 * 添加管理员
	 */
	@Override
	public void add(Admin obj) throws Exception {
		//构建插入语句
		String sql="Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (ADMIN_ID_SEQ.nextval,?,?,?,?,?,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getAdminRoles().getId(),obj.getUser_name(),obj.getUser_pass(),obj.getPhone(),obj.getLogin_times(),obj.getCreate_date(),obj.getLogin_date(),obj.getStatus(),obj.getRemark()});
	}

	/**
	 * 更新管理员
	 */
	@Override
	public void update(Admin obj) throws Exception {
		//构建更新语句
		String sql="UPDATE ADMIN SET ROLE_ID=?,USER_NAME=?,USER_PASS=?,PHONE=?,LOGIN_TIMES=?,CREATE_DATE=?,LOGIN_DATE=?,STATUS=?,REMARK=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getAdminRoles().getId(),obj.getUser_name(),obj.getUser_pass(),obj.getPhone(),obj.getLogin_times(),obj.getCreate_date(),obj.getLogin_date(),obj.getStatus(),obj.getRemark(),obj.getId()});
	}

	/**
	 * 删除管理员
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM ADMIN WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取管理员
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public Admin get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.ROLE_ID,a.USER_NAME,a.USER_PASS,a.PHONE,a.LOGIN_TIMES,a.CREATE_DATE,a.LOGIN_DATE,a.STATUS,a.REMARK,b.ROLE_NAME,b.ROLE_PRIVELEGE FROM ADMIN a LEFT JOIN ADMIN_ROLES b ON a.ROLE_ID=b.id WHERE a.ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String user_name = (String)m.get("USER_NAME");//用户名
		String user_pass = (String)m.get("USER_PASS");//密码
		String phone = (String)m.get("PHONE");//手机号码
		Integer login_times = (Integer)m.get("LOGIN_TIMES");//登录次数
		
		//由于数据返回的日期类型是Timestamp, 得强转
		Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
		Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
		
		//由于数据返回的日期类型是Timestamp, 得强转
		Timestamp login_date = (Timestamp)m.get("LOGIN_DATE");//最后登录日期
		Date login_date2 = new Date(login_date.getTime());//需要转换一下时间
		String status = (String)m.get("STATUS");//状态
		String remark = (String)m.get("REMARK");//备注
		
		Integer role_id = (Integer)m.get("ROLE_ID");//编号
		String role_name = (String)m.get("ROLE_NAME");//角色名称
		String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
		
		Admin admin=new Admin();//创建实体类对象
		admin.setId(idd);
		admin.setUser_name(user_name);
		admin.setUser_pass(user_pass);
		admin.setPhone(phone);
		admin.setLogin_times(login_times);
		admin.setLogin_date(login_date2);
		admin.setCreate_date(create_date2);
		admin.setStatus(status);
		admin.setRemark(remark);
		
		AdminRoles adminRoles=new AdminRoles();//创建实体类对象
		//将数据回填到实体类对象中
		adminRoles.setId(role_id);
		adminRoles.setRole_name(role_name);
		adminRoles.setRole_privelege(role_privelege);
		
		admin.setAdminRoles(adminRoles);//设置关联对象
		
		return admin;
	}

	/**
	 * 获得管理员表所有记录
	 */
	@Override
	public List<Admin> getList() throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.ROLE_ID,a.USER_NAME,a.USER_PASS,a.PHONE,a.LOGIN_TIMES,a.CREATE_DATE,a.LOGIN_DATE,a.STATUS,a.REMARK,b.ROLE_NAME,b.ROLE_PRIVELEGE FROM ADMIN a LEFT JOIN ADMIN_ROLES b ON a.ROLE_ID=b.ID";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<Admin> list=new ArrayList<Admin>();//存放Admin类型对象的集合
		for(Map m:resultList){
			
			Integer idd = (Integer)m.get("ID");//编号
			String user_name = (String)m.get("USER_NAME");//用户名
			String user_pass = (String)m.get("USER_PASS");//密码
			String phone = (String)m.get("PHONE");//手机号码
			Integer login_times = (Integer)m.get("LOGIN_TIMES");//登录次数
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
			Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp login_date = (Timestamp)m.get("LOGIN_DATE");//最后登录日期
			Date login_date2 = new Date(login_date.getTime());//需要转换一下时间
			String status = (String)m.get("STATUS");//状态
			String remark = (String)m.get("REMARK");//备注
			
			Integer role_id = (Integer)m.get("ROLE_ID");//编号
			String role_name = (String)m.get("ROLE_NAME");//角色名称
			String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
			
			Admin admin=new Admin();//创建实体类对象
			admin.setId(idd);
			admin.setUser_name(user_name);
			admin.setUser_pass(user_pass);
			admin.setPhone(phone);
			admin.setLogin_times(login_times);
			admin.setLogin_date(login_date2);
			admin.setCreate_date(create_date2);
			admin.setStatus(status);
			admin.setRemark(remark);
			
			AdminRoles adminRoles=new AdminRoles();//创建实体类对象
			//将数据回填到实体类对象中
			adminRoles.setId(role_id);
			adminRoles.setRole_name(role_name);
			adminRoles.setRole_privelege(role_privelege);
			
			admin.setAdminRoles(adminRoles);//设置关联对象
			
			list.add(admin);//对象存入集合
		}
		return list;
	}

	/**
	 * 带条件获取管理员记录
	 */
	@Override
	public List<Admin> getList(Admin obj) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.ROLE_ID,a.USER_NAME,a.USER_PASS,a.PHONE,a.LOGIN_TIMES,a.CREATE_DATE,a.LOGIN_DATE,a.STATUS,a.REMARK,b.ROLE_NAME,b.ROLE_PRIVELEGE FROM ADMIN a LEFT JOIN ADMIN_ROLES b ON a.ROLE_ID=b.ID WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.ID="+obj.getId();
			}
			
			if(obj.getUser_name()!=null&&!"".equals(obj.getUser_name())){
				sql+=" AND a.USER_NAME LIKE '%"+obj.getUser_name()+"%'";
			}
			
			if(obj.getAdminRoles()!=null&&obj.getAdminRoles().getId()!=null&&obj.getAdminRoles().getId()!=0){
				sql+=" AND a.ROLE_ID="+obj.getAdminRoles().getId();
			}
		}
		
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<Admin> list=new ArrayList<Admin>();//存放Admin类型对象的集合
		for(Map m:resultList){
			
			Integer idd = (Integer)m.get("ID");//编号
			String user_name = (String)m.get("USER_NAME");//用户名
			String user_pass = (String)m.get("USER_PASS");//密码
			String phone = (String)m.get("PHONE");//手机号码
			Integer login_times = (Integer)m.get("LOGIN_TIMES");//登录次数
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
			Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp login_date = (Timestamp)m.get("LOGIN_DATE");//最后登录日期
			Date login_date2 = new Date(login_date.getTime());//需要转换一下时间
			String status = (String)m.get("STATUS");//状态
			String remark = (String)m.get("REMARK");//备注
			
			Integer role_id = (Integer)m.get("ROLE_ID");//编号
			String role_name = (String)m.get("ROLE_NAME");//角色名称
			String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
			
			Admin admin=new Admin();//创建实体类对象
			admin.setId(idd);
			admin.setUser_name(user_name);
			admin.setUser_pass(user_pass);
			admin.setPhone(phone);
			admin.setLogin_times(login_times);
			admin.setLogin_date(login_date2);
			admin.setCreate_date(create_date2);
			admin.setStatus(status);
			admin.setRemark(remark);
			
			AdminRoles adminRoles=new AdminRoles();//创建实体类对象
			//将数据回填到实体类对象中
			adminRoles.setId(role_id);
			adminRoles.setRole_name(role_name);
			adminRoles.setRole_privelege(role_privelege);
			
			admin.setAdminRoles(adminRoles);//设置关联对象
			
			list.add(admin);//对象存入集合
		}
		return list;
	}
	
	
	/**
	 *通过用户名去获取管理员 
	 * @param user_name
	 * @return
	 */
	public Admin getAdminByUserName(String userName) throws Exception{
		//构建查询语句
		String sql="SELECT a.ID,a.ROLE_ID,a.USER_NAME,a.USER_PASS,a.PHONE,a.LOGIN_TIMES,a.CREATE_DATE,a.LOGIN_DATE,a.STATUS,a.REMARK,b.ROLE_NAME,b.ROLE_PRIVELEGE FROM ADMIN a LEFT JOIN ADMIN_ROLES b ON a.ROLE_ID=b.id WHERE a.USER_NAME=?";
		Map m = this.uniqueQuery(sql, new Object[]{userName});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String user_name = (String)m.get("USER_NAME");//用户名
		String user_pass = (String)m.get("USER_PASS");//密码
		String phone = (String)m.get("PHONE");//手机号码
		Integer login_times = (Integer)m.get("LOGIN_TIMES");//登录次数
		
		//由于数据返回的日期类型是Timestamp, 得强转
		Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
		Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
		
		//由于数据返回的日期类型是Timestamp, 得强转
		Timestamp login_date = (Timestamp)m.get("LOGIN_DATE");//最后登录日期
		Date login_date2 = new Date(login_date.getTime());//需要转换一下时间
		String status = (String)m.get("STATUS");//状态
		String remark = (String)m.get("REMARK");//备注
		
		Integer role_id = (Integer)m.get("ROLE_ID");//编号
		String role_name = (String)m.get("ROLE_NAME");//角色名称
		String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
		
		Admin admin=new Admin();//创建实体类对象
		admin.setId(idd);
		admin.setUser_name(user_name);
		admin.setUser_pass(user_pass);
		admin.setPhone(phone);
		admin.setLogin_times(login_times);
		admin.setLogin_date(login_date2);
		admin.setCreate_date(create_date2);
		admin.setStatus(status);
		admin.setRemark(remark);
		
		AdminRoles adminRoles=new AdminRoles();//创建实体类对象
		//将数据回填到实体类对象中
		adminRoles.setId(role_id);
		adminRoles.setRole_name(role_name);
		adminRoles.setRole_privelege(role_privelege);
		
		admin.setAdminRoles(adminRoles);//设置关联对象
		
		return admin;
	}

}
