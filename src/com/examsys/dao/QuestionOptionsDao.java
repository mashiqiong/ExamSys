package com.examsys.dao;

import com.examsys.po.QuestionOptions;
/**
 * 试题选项数据访问层接口
 * @author edu-1
 *
 */
public interface QuestionOptionsDao extends IBaseDao<QuestionOptions, Integer> {
	// 通过试题编号删除试题选项信息
	public void deleteByQuestionId(Integer obj) throws Exception;
}
