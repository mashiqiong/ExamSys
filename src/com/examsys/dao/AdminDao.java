package com.examsys.dao;

import com.examsys.po.Admin;
/**
 * 管理员数据访问层接口
 * @author edu-1
 *
 */
public interface AdminDao extends IBaseDao<Admin, Integer> {
	/**
	 *通过用户名去获取管理员 
	 * @param user_name
	 * @return
	 */
	public Admin getAdminByUserName(String user_name) throws Exception;
}
