package com.examsys.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.ExamDetail;
import com.examsys.po.ExamMain;
import com.examsys.po.Question;

/**
 * 考试答题卡明细数据访问层实现类
 * @author edu-1
 *
 */
public class ExamDetailDaoImpl extends AbstractBaseDao<ExamDetail, Integer> implements ExamDetailDao {

	/**
	 * 添加考试答题卡明细
	 */
	@Override
	public void add(ExamDetail obj) throws Exception {
		//构造插入语句
		String sql="Insert into EXAM_DETAIL (ID,MAIN_ID,QUESTION_ID,ANSWER,SCORE,STATUS,REMARK) values (EXAM_DETAIL_ID_SEQ.nextval,?,?,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getExamMain().getId(),obj.getQuestion().getId(),obj.getAnswer(),obj.getScore(),obj.getStatus(),obj.getRemark()});
	}

	/**
	 * 添加多条信息
	 * @param examDetails
	 * @throws Exception
	 */
	public void add(List<ExamDetail> examDetails) throws Exception{
		for(ExamDetail examDetail:examDetails){
			this.add(examDetail);
		}
	}
	/**
	 * 更新考试答题卡明细
	 */
	@Override
	public void update(ExamDetail obj) throws Exception {
		//构造更新语句
		String sql="UPDATE EXAM_DETAIL SET MAIN_ID=?,QUESTION_ID=?,ANSWER=?,SCORE=?,STATUS=?,REMARK=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getExamMain().getId(),obj.getQuestion().getId(),obj.getAnswer(),obj.getScore(),obj.getStatus(),obj.getRemark(),obj.getId()});
	}

	/**
	 * 删除考试答题卡明细
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM EXAM_DETAIL WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 通过主答题卡编号删除答题卡明细
	 */
	public void deleteByExamMainId(Integer id) throws Exception{
		//构建删除语句
		String sql="DELETE FROM EXAM_DETAIL WHERE MAIN_ID=?";
		this.execute(sql, new Object[]{id});
	}
	/**
	 * 获取考试答题卡明细
	 * @param id 编号
	 */
	@Override
	public ExamDetail get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.MAIN_ID,a.QUESTION_ID,a.ANSWER,a.SCORE,a.STATUS,a.REMARK,b.CONTENT,b.SKEY FROM EXAM_DETAIL a LEFT JOIN exam_main b ON a.main_id=b.ID LEFT JOIN QUESTION c ON a.QUESTION_ID=c.ID WHERE a.ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		Integer score = (Integer)m.get("SCORE");//总得分
		String answer = (String)m.get("ANSWER");//考生答题内容
		String status = (String)m.get("STATUS");//考试状态
		String remark = (String)m.get("REMARK");//备注
		
		Integer question_id = (Integer)m.get("QUESTION_ID");//题目编号
		String content = (String)m.get("CONTENT");//题干内容
		String skey = (String)m.get("SKEY");//标准答案
		
		Integer main_id = (Integer)m.get("MAIN_ID");//考试答题卡编号
		
		ExamDetail examDetail=new ExamDetail();//创建实体类对象
		examDetail.setId(idd);
		examDetail.setScore(score);
		examDetail.setAnswer(answer);
		examDetail.setStatus(status);
		examDetail.setRemark(remark);
		
		Question question=new Question();//创建实体类对象
		question.setId(question_id);
		question.setContent(content);
		question.setSkey(skey);
		
		ExamMain examMain=new ExamMain();//创建实体类对象
		examMain.setId(main_id);
		
		examDetail.setQuestion(question);//设置关联对象
		examDetail.setExamMain(examMain);//设置关联对象
		return examDetail;
		
	}

	/**
	 * 获取所有考试答题卡明细
	 */
	@Override
	public List<ExamDetail> getList() throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.MAIN_ID,a.QUESTION_ID,a.ANSWER,a.SCORE,a.STATUS,a.REMARK,b.CONTENT,b.SKEY FROM EXAM_DETAIL a LEFT JOIN exam_main b ON a.main_id=b.ID LEFT JOIN QUESTION c ON a.QUESTION_ID=c.ID";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<ExamDetail> list =new ArrayList<ExamDetail> ();//存放ExamDetail类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			Integer score = (Integer)m.get("SCORE");//总得分
			String answer = (String)m.get("ANSWER");//考生答题内容
			String status = (String)m.get("STATUS");//考试状态
			String remark = (String)m.get("REMARK");//备注
			
			Integer question_id = (Integer)m.get("QUESTION_ID");//题目编号
			String content = (String)m.get("CONTENT");//题干内容
			String skey = (String)m.get("SKEY");//标准答案
			
			Integer main_id = (Integer)m.get("MAIN_ID");//考试答题卡编号
			
			ExamDetail examDetail=new ExamDetail();//创建实体类对象
			examDetail.setId(idd);
			examDetail.setScore(score);
			examDetail.setAnswer(answer);
			examDetail.setStatus(status);
			examDetail.setRemark(remark);
			
			Question question=new Question();//创建实体类对象
			question.setId(question_id);
			question.setContent(content);
			question.setSkey(skey);
			
			ExamMain examMain=new ExamMain();//创建实体类对象
			examMain.setId(main_id);
			
			examDetail.setQuestion(question);//设置关联对象
			examDetail.setExamMain(examMain);//设置关联对象
			list.add(examDetail);
		}
		return list;
	}

	/**
	 * 带条件获取考试答题卡明细
	 */
	@Override
	public List<ExamDetail> getList(ExamDetail obj) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.MAIN_ID,a.QUESTION_ID,a.ANSWER,a.SCORE,a.STATUS,a.REMARK,b.CONTENT,b.SKEY FROM EXAM_DETAIL a LEFT JOIN exam_main b ON a.main_id=b.ID LEFT JOIN QUESTION c ON a.QUESTION_ID=c.ID WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.ID="+obj.getId();
			}
			
			if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null
					&&obj.getQuestion().getId()!=0){
				sql+=" AND a.QUESTION_ID="+obj.getQuestion().getId();
			
			}
			
			if(obj.getExamMain()!=null&&obj.getExamMain().getId()!=null
					&&obj.getExamMain().getId()!=0){
				sql+=" AND a.MAIN_ID="+obj.getExamMain().getId();
			}
		}
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<ExamDetail> list =new ArrayList<ExamDetail> ();//存放ExamDetail类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			Integer score = (Integer)m.get("SCORE");//总得分
			String answer = (String)m.get("ANSWER");//考生答题内容
			String status = (String)m.get("STATUS");//考试状态
			String remark = (String)m.get("REMARK");//备注
			
			Integer question_id = (Integer)m.get("QUESTION_ID");//题目编号
			String content = (String)m.get("CONTENT");//题干内容
			String skey = (String)m.get("SKEY");//标准答案
			
			Integer main_id = (Integer)m.get("MAIN_ID");//考试答题卡编号
			
			ExamDetail examDetail=new ExamDetail();//创建实体类对象
			examDetail.setId(idd);
			examDetail.setScore(score);
			examDetail.setAnswer(answer);
			examDetail.setStatus(status);
			examDetail.setRemark(remark);
			
			Question question=new Question();//创建实体类对象
			question.setId(question_id);
			question.setContent(content);
			question.setSkey(skey);
			
			ExamMain examMain=new ExamMain();//创建实体类对象
			examMain.setId(main_id);
			
			examDetail.setQuestion(question);//设置关联对象
			examDetail.setExamMain(examMain);//设置关联对象
			list.add(examDetail);
		}
		return list;
	}

}
