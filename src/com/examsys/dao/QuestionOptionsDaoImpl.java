package com.examsys.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.Question;
import com.examsys.po.QuestionOptions;

/**
 * 试题选项数据访问层实现类
 * @author edu-1
 *
 */
public class QuestionOptionsDaoImpl extends AbstractBaseDao<QuestionOptions, Integer> implements QuestionOptionsDao {

	/**
	 * 添加试题选项
	 */
	@Override
	public void add(QuestionOptions obj) throws Exception {
		//构造插入语句
		String sql="Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (QUESTION_OPTIONS_ID_SEQ.nextval,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getQuestion().getId(),obj.getSalisa(),obj.getSoption(),obj.getSextend()});
	}

	/**
	 * 更新试题选项
	 */
	@Override
	public void update(QuestionOptions obj) throws Exception {
		//构造更新语句
		String sql="UPDATE QUESTION_OPTIONS SET QUESTION_ID=?,SALISA=?,SOPTION=?,SEXTEND=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getQuestion().getId(),obj.getSalisa(),obj.getSoption(),obj.getSextend(),obj.getId()});
	}

	/**
	 * 删除试题选项
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM QUESTION_OPTIONS WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 通过试题编号删除试题选项
	 */
	@Override
	public void deleteByQuestionId(Integer question_id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM QUESTION_OPTIONS WHERE QUESTION_ID=?";
		this.execute(sql, new Object[]{question_id});
	}
	
	/**
	 * 获取试题选项
	 * @param id 编号
	 */
	@Override
	public QuestionOptions get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.QUESTION_ID,a.SALISA,a.SOPTION,a.SEXTEND FROM QUESTION_OPTIONS a LEFT JOIN QUESTION b ON a.QUESTION_ID=b.ID WHERE a.ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		Integer question_id = (Integer)m.get("QUESTION_ID");//题目编号
		String salisa = (String)m.get("SALISA");//选项编号
		String soption = (String)m.get("SOPTION");//选项描述
		String sextend = (String)m.get("SEXTEND");//面试题的扩展项
		
		QuestionOptions questionOptions=new QuestionOptions();//创建实体类对象
		questionOptions.setId(idd);
		questionOptions.setSalisa(salisa);
		questionOptions.setSoption(soption);
		questionOptions.setSextend(sextend);
		
		Question question=new Question();//创建实体类对象
		question.setId(question_id);
		
		questionOptions.setQuestion(question);//设置关联对象
		
		return questionOptions;
}

	/**
	 * 获取所有试题选项
	 */
	@Override
	public List<QuestionOptions> getList() throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.QUESTION_ID,a.SALISA,a.SOPTION,a.SEXTEND FROM QUESTION_OPTIONS a LEFT JOIN QUESTION b ON a.QUESTION_ID=b.ID";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<QuestionOptions> list=new ArrayList<QuestionOptions>();//存放QuestionOptions类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			Integer question_id = (Integer)m.get("QUESTION_ID");//题目编号
			String salisa = (String)m.get("SALISA");//选项编号
			String soption = (String)m.get("SOPTION");//选项描述
			String sextend = (String)m.get("SEXTEND");//面试题的扩展项
			
			QuestionOptions questionOptions=new QuestionOptions();//创建实体类对象
			questionOptions.setId(idd);
			questionOptions.setSalisa(salisa);
			questionOptions.setSoption(soption);
			questionOptions.setSextend(sextend);
			
			Question question=new Question();//创建实体类对象
			question.setId(question_id);
			
			questionOptions.setQuestion(question);//设置关联对象
			
			list.add(questionOptions);
		}
		return list;
		
	}

	/**
	 * 带条件获取试题选项
	 */
	@Override
	public List<QuestionOptions> getList(QuestionOptions obj) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.QUESTION_ID,a.SALISA,a.SOPTION,a.SEXTEND FROM QUESTION_OPTIONS a LEFT JOIN QUESTION b ON a.QUESTION_ID=b.ID WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.ID="+obj.getId();
			}
			
			if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null
					&&obj.getQuestion().getId()!=0){
				sql+=" AND a.QUESTION_ID="+obj.getQuestion().getId();
			}
			
		}

		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<QuestionOptions> list=new ArrayList<QuestionOptions>();//存放QuestionOptions类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			Integer question_id = (Integer)m.get("QUESTION_ID");//题目编号
			String salisa = (String)m.get("SALISA");//选项编号
			String soption = (String)m.get("SOPTION");//选项描述
			String sextend = (String)m.get("SEXTEND");//面试题的扩展项
			
			QuestionOptions questionOptions=new QuestionOptions();//创建实体类对象
			questionOptions.setId(idd);
			questionOptions.setSalisa(salisa);
			questionOptions.setSoption(soption);
			questionOptions.setSextend(sextend);
			
			Question question=new Question();//创建实体类对象
			question.setId(question_id);
			
			questionOptions.setQuestion(question);//设置关联对象
			
			list.add(questionOptions);
		}
		return list;
	}

}
