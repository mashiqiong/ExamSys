package com.examsys.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.Admin;
import com.examsys.po.QuestionDb;

/**
 * 题库数据访问层实现类
 * @author edu-1
 *
 */
public class QuestionDbDaoImpl extends AbstractBaseDao<QuestionDb, Integer> implements QuestionDbDao {

	/**
	 * 添加题库
	 */
	@Override
	public void add(QuestionDb obj) throws Exception {
		//构造插入语句
		String sql="Insert into QUESTION_DB(ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (QUESTION_DB_ID_SEQ.nextval,?,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getAdmin().getId(),obj.getName(),obj.getCreate_date(),obj.getStatus(),obj.getRemark()});
	}

	/**
	 * 更新题库
	 */
	@Override
	public void update(QuestionDb obj) throws Exception {
		//构造更新语句
		String sql="UPDATE QUESTION_DB SET ADMIN_ID=?,NAME=?,CREATE_DATE=?,STATUS=?,REMARK=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getAdmin().getId(),obj.getName(),obj.getCreate_date(),obj.getStatus(),obj.getRemark(),obj.getId()});
	}

	/**
	 * 删除题库
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM QUESTION_DB WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取题库
	 * @param id 编号
	 */
	@Override
	public QuestionDb get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.ADMIN_ID,a.NAME,a.CREATE_DATE,a.STATUS,a.REMARK,b.USER_NAME FROM QUESTION_DB a LEFT JOIN ADMIN b ON a.ADMIN_ID=b.ID WHERE a.ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String name = (String)m.get("NAME");//代码
		
		//由于数据返回的日期类型是Timestamp, 得强转
		Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//注册时间
		Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
		
		String status = (String)m.get("STATUS");//信息内容
		String remark = (String)m.get("REMARK");//信息内容
		
		QuestionDb questionDb=new QuestionDb();
		questionDb.setId(idd);
		questionDb.setName(name);
		questionDb.setStatus(status);
		questionDb.setRemark(remark);
		questionDb.setCreate_date(create_date2);
		
		Integer admin_id = (Integer)m.get("ADMIN_ID");//管理员编号
		String user_name = (String)m.get("USER_NAME");//管理员名称
		
		Admin admin=new Admin();//创建实体类对象
		admin.setId(admin_id);
		admin.setUser_name(user_name);
		
		questionDb.setAdmin(admin);//设置关联对象
		
		return questionDb;
	}

	/**
	 * 获取所有题库
	 */
	@Override
	public List<QuestionDb> getList() throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.ADMIN_ID,a.NAME,a.CREATE_DATE,a.STATUS,a.REMARK,b.USER_NAME FROM QUESTION_DB a LEFT JOIN ADMIN b ON a.ADMIN_ID=b.ID";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<QuestionDb> list=new ArrayList<QuestionDb>();//存放QuestionDb类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String name = (String)m.get("NAME");//题库名称
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//注册时间
			Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
			
			String status = (String)m.get("STATUS");//状态
			String remark = (String)m.get("REMARK");//备注
			
			QuestionDb questionDb=new QuestionDb();
			questionDb.setId(idd);
			questionDb.setName(name);
			questionDb.setStatus(status);
			questionDb.setRemark(remark);
			questionDb.setCreate_date(create_date2);
			
			Integer admin_id = (Integer)m.get("ADMIN_ID");//管理员编号
			String user_name = (String)m.get("USER_NAME");//管理员名称
			
			Admin admin=new Admin();//创建实体类对象
			admin.setId(admin_id);
			admin.setUser_name(user_name);
			
			questionDb.setAdmin(admin);//设置关联对象
			list.add(questionDb);
		}
		return list;
	}

	/**
	 * 带条件获取题库
	 */
	@Override
	public List<QuestionDb> getList(QuestionDb obj) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.ADMIN_ID,a.NAME,a.CREATE_DATE,a.STATUS,a.REMARK,b.USER_NAME FROM QUESTION_DB a LEFT JOIN ADMIN b ON a.ADMIN_ID=b.ID WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.ID="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.NAME LIKE '%"+obj.getName()+"%'";
			}
			
		}

		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<QuestionDb> list=new ArrayList<QuestionDb>();//存放QuestionDb类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String name = (String)m.get("NAME");//题库名称
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//注册时间
			Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
			
			String status = (String)m.get("STATUS");//状态
			String remark = (String)m.get("REMARK");//备注
			
			QuestionDb questionDb=new QuestionDb();//创建实体类对象
			questionDb.setId(idd);
			questionDb.setName(name);
			questionDb.setStatus(status);
			questionDb.setRemark(remark);
			questionDb.setCreate_date(create_date2);
			
			Integer admin_id = (Integer)m.get("ADMIN_ID");//管理员编号
			String user_name = (String)m.get("USER_NAME");//管理员名称
			
			Admin admin=new Admin();//创建实体类对象
			admin.setId(admin_id);
			admin.setUser_name(user_name);
			
			questionDb.setAdmin(admin);//设置关联对象
			list.add(questionDb);
		}
		return list;
	}

}
