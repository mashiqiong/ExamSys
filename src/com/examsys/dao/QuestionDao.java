package com.examsys.dao;

import java.util.List;

import com.examsys.po.Question;
/**
 * 试题数据访问层接口
 * @author edu-1
 *
 */
public interface QuestionDao extends IBaseDao<Question, Integer> {
	public Integer getSeq() throws Exception;//获取试题系列
	
	/**
	 * 通过条件获得信息列表，不包括给定编号的试题
	 * @param notInIds
	 * @return
	 */
	public List<Question> getList(String notInIds,Question question) throws Exception;
}
