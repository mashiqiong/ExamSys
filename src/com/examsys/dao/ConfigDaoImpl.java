package com.examsys.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.Config;
/**
 * 系统参数数据访问层实现类
 * @author edu-1
 *
 */
public class ConfigDaoImpl extends AbstractBaseDao<Config, Integer> implements ConfigDao {

	/**
	 * 添加系统参数
	 */
	@Override
	public void add(Config obj) throws Exception {
		//构造插入语句
		String sql="Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (CONFIG_ID_SEQ.nextval,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getName(),obj.getConfig_key(),obj.getConfig_value(),obj.getRemark()});
	}

	/**
	 * 更新系统参数
	 */
	@Override
	public void update(Config obj) throws Exception {
		//构造更新语句
		String sql="UPDATE CONFIG SET NAME=?,CONFIG_KEY=?,CONFIG_VALUE=?,REMARK=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getName(),obj.getConfig_key(),obj.getConfig_value(),obj.getRemark(),obj.getId()});
	}

	/**
	 * 删除系统参数
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM CONFIG WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取系统参数
	 */
	@Override
	public Config get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM CONFIG WHERE ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String name = (String)m.get("NAME");//参数名称
		String config_key = (String)m.get("CONFIG_KEY");//参数代码
		String config_value = (String)m.get("CONFIG_VALUE");//参数值
		String remark = (String)m.get("REMARK");//备注
		
		Config config=new Config();//创建实体类对象
		config.setId(idd);
		config.setName(name);
		config.setConfig_key(config_key);
		config.setConfig_value(config_value);
		config.setRemark(remark);
		
		return config;
	}

	/**
	 * 获取所有系统参数记录
	 */
	@Override
	public List<Config> getList() throws Exception {
		//构建查询语句
		String sql="SELECT * FROM CONFIG";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<Config> list=new ArrayList<Config>();//存放Config类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String name = (String)m.get("NAME");//参数名称
			String config_key = (String)m.get("CONFIG_KEY");//参数代码
			String config_value = (String)m.get("CONFIG_VALUE");//参数值
			String remark = (String)m.get("REMARK");//备注
			
			Config config=new Config();//创建实体类对象
			config.setId(idd);
			config.setName(name);
			config.setConfig_key(config_key);
			config.setConfig_value(config_value);
			config.setRemark(remark);
			list.add(config);
		}
		return list;
	}

	/**
	 * 带条件获取系统参数记录
	 */
	@Override
	public List<Config> getList(Config obj) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM CONFIG WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND ID="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND NAME LIKE '%"+obj.getName()+"%'";
			}
		}
		
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<Config> list=new ArrayList<Config>();//存放Config类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String name = (String)m.get("NAME");//参数名称
			String config_key = (String)m.get("CONFIG_KEY");//参数代码
			String config_value = (String)m.get("CONFIG_VALUE");//参数值
			String remark = (String)m.get("REMARK");//备注
			
			Config config=new Config();//创建实体类对象
			config.setId(idd);
			config.setName(name);
			config.setConfig_key(config_key);
			config.setConfig_value(config_value);
			config.setRemark(remark);
			list.add(config);
		}
		return list;
	}

}
