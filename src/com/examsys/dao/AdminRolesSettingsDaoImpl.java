package com.examsys.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.AdminRolesSettings;
/**
 * 系统功能数据访问层实现类
 * @author edu-1
 *
 */
public class AdminRolesSettingsDaoImpl extends AbstractBaseDao<AdminRolesSettings, Integer> implements AdminRolesSettingsDao {

	/**
	 * 添加系统功能
	 */
	@Override
	public void add(AdminRolesSettings obj) throws Exception {
		//构造插入语句
		String sql="Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER) values (ADMIN_ROLES_SETTINGS_ID_SEQ.nextval,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getName(),obj.getCode(),obj.getAdminRolesSettings().getId(),obj.getPorder()});
	}

	/**
	 * 更新系统功能
	 */
	@Override
	public void update(AdminRolesSettings obj) throws Exception {
		//构造更新语句
		String sql="UPDATE ADMIN_ROLES_SETTINGS SET NAME=?,CODE=?,PARENT_ID=?,PORDER=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getName(),obj.getCode(),obj.getAdminRolesSettings().getId(),obj.getPorder(),obj.getId()});
	}

	/**
	 * 删除系统功能
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM ADMIN_ROLES_SETTINGS WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取系统功能
	 * @param id 编号
	 */
	@Override
	public AdminRolesSettings get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.NAME,a.CODE,a.PARENT_ID,a.PORDER,b.NAME PARENT_NAME,b.CODE PARENT_CODE,b.PORDER PARENT_PORDER  FROM ADMIN_ROLES_SETTINGS a LEFT JOIN ADMIN_ROLES_SETTINGS b ON a.PARENT_ID=b.ID WHERE a.ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String name = (String)m.get("NAME");//菜单或功能名称
		String code = (String)m.get("CODE");//菜单或者功能代码
		Integer porder = (Integer)m.get("PORDER");//排序号
		
		AdminRolesSettings adminRolesSettings= new AdminRolesSettings();//菜单对象
		adminRolesSettings.setId(idd);
		adminRolesSettings.setName(name);
		adminRolesSettings.setCode(code);
		adminRolesSettings.setPorder(porder);
		
		Integer parent_id = (Integer)m.get("PARENT_ID");//父级菜单编号
		String parent_name = (String)m.get("PARENT_NAME");//父级菜单功能名称
		String parent_code = (String)m.get("PARENT_CODE");//父级菜单代码
		Integer parent_porder = (Integer)m.get("PARENT_PORDER");//父级菜单排序号
		
		AdminRolesSettings pAdminRolesSettings= new AdminRolesSettings();//它的父级菜单对象
		pAdminRolesSettings.setId(parent_id);
		pAdminRolesSettings.setName(parent_name);
		pAdminRolesSettings.setCode(parent_code);
		pAdminRolesSettings.setPorder(parent_porder);
		
		adminRolesSettings.setAdminRolesSettings(pAdminRolesSettings);//设置关联对象
		
		return adminRolesSettings;
	}

	/**
	 * 获取所有系统功能所有记录
	 */
	@Override
	public List<AdminRolesSettings> getList() throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.NAME,a.CODE,a.PARENT_ID,a.PORDER,b.NAME PARENT_NAME,b.CODE PARENT_CODE,b.PORDER PARENT_PORDER  FROM ADMIN_ROLES_SETTINGS a LEFT JOIN ADMIN_ROLES_SETTINGS b ON a.PARENT_ID=b.ID";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<AdminRolesSettings> list=new ArrayList<AdminRolesSettings>();//存放AdminRolesSettings类型对象的集合
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String name = (String)m.get("NAME");//菜单或功能名称
			String code = (String)m.get("CODE");//菜单或者功能代码
			Integer porder = (Integer)m.get("PORDER");//排序号
			
			AdminRolesSettings adminRolesSettings= new AdminRolesSettings();//菜单对象
			adminRolesSettings.setId(idd);
			adminRolesSettings.setName(name);
			adminRolesSettings.setCode(code);
			adminRolesSettings.setPorder(porder);
			
			Integer parent_id = (Integer)m.get("PARENT_ID");//父级菜单编号
			String parent_name = (String)m.get("PARENT_NAME");//父级菜单功能名称
			String parent_code = (String)m.get("PARENT_CODE");//父级菜单代码
			Integer parent_porder = (Integer)m.get("PARENT_PORDER");//父级菜单排序号
			
			AdminRolesSettings pAdminRolesSettings= new AdminRolesSettings();//它的父级菜单对象
			pAdminRolesSettings.setId(parent_id);
			pAdminRolesSettings.setName(parent_name);
			pAdminRolesSettings.setCode(parent_code);
			pAdminRolesSettings.setPorder(parent_porder);
			
			adminRolesSettings.setAdminRolesSettings(pAdminRolesSettings);//设置关联对象
			
			list.add(adminRolesSettings);
		}
		return list;
	}

	/**
	 * 带条件获取所有系统功能记录
	 */
	@Override
	public List<AdminRolesSettings> getList(AdminRolesSettings obj) throws Exception {
		//构建查询语句
		String sql="SELECT a.ID,a.NAME,a.CODE,a.PARENT_ID,a.PORDER,b.NAME PARENT_NAME,b.CODE PARENT_CODE,b.PORDER PARENT_PORDER  FROM ADMIN_ROLES_SETTINGS a LEFT JOIN ADMIN_ROLES_SETTINGS b ON a.PARENT_ID=b.ID WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" a.AND a.ID="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.NAME like '%"+obj.getName()+"%'";
			}
			
			if(obj.getAdminRolesSettings()!=null&&obj.getAdminRolesSettings().getId()!=null){
				sql+=" AND a.PARENT_ID="+obj.getAdminRolesSettings().getId();
			}
		}

		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<AdminRolesSettings> list=new ArrayList<AdminRolesSettings>();//存放AdminRolesSettings类型对象的集合
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String name = (String)m.get("NAME");//菜单或功能名称
			String code = (String)m.get("CODE");//菜单或者功能代码
			Integer porder = (Integer)m.get("PORDER");//排序号
			
			AdminRolesSettings adminRolesSettings= new AdminRolesSettings();//菜单对象
			adminRolesSettings.setId(idd);
			adminRolesSettings.setName(name);
			adminRolesSettings.setCode(code);
			adminRolesSettings.setPorder(porder);
			
			Integer parent_id = (Integer)m.get("PARENT_ID");//父级菜单编号
			String parent_name = (String)m.get("PARENT_NAME");//父级菜单功能名称
			String parent_code = (String)m.get("PARENT_CODE");//父级菜单代码
			Integer parent_porder = (Integer)m.get("PARENT_PORDER");//父级菜单排序号
			
			AdminRolesSettings pAdminRolesSettings= new AdminRolesSettings();//它的父级菜单对象
			pAdminRolesSettings.setId(parent_id);
			pAdminRolesSettings.setName(parent_name);
			pAdminRolesSettings.setCode(parent_code);
			pAdminRolesSettings.setPorder(parent_porder);
			
			adminRolesSettings.setAdminRolesSettings(pAdminRolesSettings);//设置关联对象
			
			list.add(adminRolesSettings);
		}
		return list;
	}

}
