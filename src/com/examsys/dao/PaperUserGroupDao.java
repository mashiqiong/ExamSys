package com.examsys.dao;

import java.util.List;

import com.examsys.po.PaperUserGroup;
/**
 * 参与考试的用户组数据访问层接口
 * @author edu-1
 *
 */
public interface PaperUserGroupDao extends IBaseDao<PaperUserGroup, Integer> {
	/**
	 * 添加多条参与考试的用户组信息
	 * @param paperUserGroups
	 * @throws Exception
	 */
	public void add(List<PaperUserGroup> paperUserGroups) throws Exception;
}
