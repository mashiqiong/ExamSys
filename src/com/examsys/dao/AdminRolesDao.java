package com.examsys.dao;

import com.examsys.po.AdminRoles;
/**
 * 管理员角色数据访问层接口
 * @author edu-1
 *
 */
public interface AdminRolesDao extends IBaseDao<AdminRoles, Integer>{

}
