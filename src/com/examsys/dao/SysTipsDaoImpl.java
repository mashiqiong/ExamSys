package com.examsys.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.SysTips;
/**
 * 系统提示信息数据访问层实现类
 * @author edu-1
 *
 */
public class SysTipsDaoImpl extends AbstractBaseDao<SysTips, Integer> implements SysTipsDao {

	/**
	 * 添加系统提示信息
	 */
	@Override
	public void add(SysTips obj) throws Exception {
		//构造插入语句
		String sql="Insert into SYS_TIPS (ID,SCODE,SDESC) values (SYS_TIPS_ID_SEQ.nextval,?,?)";
		this.execute(sql, new Object[]{obj.getScode(),obj.getSdesc()});
	}

	/**
	 * 更新系统提示信息
	 */
	@Override
	public void update(SysTips obj) throws Exception {
		//构造更新语句
		String sql="UPDATE SYS_TIPS SET SCODE=?,SDESC=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getScode(),obj.getSdesc(),obj.getId()});
	}

	/**
	 * 删除系统提示信息
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM SYS_TIPS WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取系统提示信息
	 */
	@Override
	public SysTips get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM SYS_TIPS WHERE ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String scode = (String)m.get("SCODE");//代码
		String sdesc = (String)m.get("SDESC");//信息内容
		
		SysTips config=new SysTips();//创建实体类对象
		config.setId(idd);
		config.setScode(scode);
		config.setSdesc(sdesc);
		
		return config;
	}

	/**
	 * 获取所有系统提示信息记录
	 */
	@Override
	public List<SysTips> getList() throws Exception {
		//构建查询语句
		String sql="SELECT * FROM SYS_TIPS";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<SysTips> list=new ArrayList<SysTips>();//存放SysTips类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String scode = (String)m.get("SCODE");//代码
			String sdesc = (String)m.get("SDESC");//信息内容
			
			SysTips config=new SysTips();//创建实体类对象
			config.setId(idd);
			config.setScode(scode);
			config.setSdesc(sdesc);
			
			list.add(config);
		}
		return list;
	}

	/**
	 * 带条件获取系统提示信息记录
	 */
	@Override
	public List<SysTips> getList(SysTips obj) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM SYS_TIPS WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND ID="+obj.getId();
			}
			
			if(obj.getScode()!=null&&!"".equals(obj.getScode())){
				sql+=" AND SCODE LIKE '%"+obj.getScode()+"%'";
			}
			
			if(obj.getSdesc()!=null&&!"".equals(obj.getSdesc())){
				sql+=" AND SDESC LIKE '%"+obj.getSdesc()+"%'";
			}
		}
		
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<SysTips> list=new ArrayList<SysTips>();//存放SysTips类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String scode = (String)m.get("SCODE");//代码
			String sdesc = (String)m.get("SDESC");//信息内容
			
			SysTips config=new SysTips();//创建实体类对象
			config.setId(idd);
			config.setScode(scode);
			config.setSdesc(sdesc);
			
			list.add(config);
		}
		return list;
	}

}
