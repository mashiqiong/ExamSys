package com.examsys.dao;

import java.util.List;

import com.examsys.po.ExamDetail;
/**
 * 考试答题卡明细数据访问层接口
 * @author edu-1
 *
 */
public interface ExamDetailDao extends IBaseDao<ExamDetail, Integer> {
	
	/**
	 * 通过主答题卡编号删除答题卡明细
	 */
	public void deleteByExamMainId(Integer id) throws Exception;
	
	/**
	 * 添加多条信息
	 * @param examDetails
	 * @throws Exception
	 */
	public void add(List<ExamDetail> examDetails) throws Exception;
}
