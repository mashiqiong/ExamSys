package com.examsys.dao;

import com.examsys.po.QuestionDb;
/**
 * 题库数据访问层接口
 * @author edu-1
 *
 */
public interface QuestionDbDao extends IBaseDao<QuestionDb, Integer> {

}
