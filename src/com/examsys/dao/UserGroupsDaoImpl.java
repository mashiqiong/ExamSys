package com.examsys.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.UserGroups;
/**
 * 会员组数据访问层实现类
 * @author edu-1
 *
 */
public class UserGroupsDaoImpl extends AbstractBaseDao<UserGroups, Integer> implements UserGroupsDao {

	/**
	 * 添加会员组
	 */
	@Override
	public void add(UserGroups obj) throws Exception {
		//构造插入语句
		String sql="Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (USER_GROUPS_ID_SEQ.nextval,?,?)";
		this.execute(sql, new Object[]{obj.getGroup_name(),obj.getRemark()});
	}

	/**
	 * 更新会员组
	 */
	@Override
	public void update(UserGroups obj) throws Exception {
		//构造更新语句
		String sql="UPDATE USER_GROUPS SET GROUP_NAME=?,REMARK=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getGroup_name(),obj.getRemark(),obj.getId()});
	}

	/**
	 * 删除会员组
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM USER_GROUPS WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取会员组
	 */
	@Override
	public UserGroups get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM USER_GROUPS WHERE ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String group_name = (String)m.get("GROUP_NAME");//会员组名称
		String remark = (String)m.get("REMARK");//备注
		
		UserGroups userGroups=new UserGroups();//创建实体类对象
		userGroups.setId(idd);
		userGroups.setGroup_name(group_name);
		userGroups.setRemark(remark);
		return userGroups;
	}

	@Override
	public List<UserGroups> getList() throws Exception {
		//构建查询语句
		String sql="SELECT * FROM USER_GROUPS";
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<UserGroups> list=new ArrayList<UserGroups>();//存放UserGroups类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String group_name = (String)m.get("GROUP_NAME");//会员组名称
			String remark = (String)m.get("REMARK");//备注
			
			UserGroups userGroups=new UserGroups();//创建实体类对象
			userGroups.setId(idd);
			userGroups.setGroup_name(group_name);
			userGroups.setRemark(remark);
			list.add(userGroups);
		}
		return list;
	}

	@Override
	public List<UserGroups> getList(UserGroups obj) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM USER_GROUPS WHERE 1=1";
		
		if(obj!=null&&obj.getId()!=null){
			sql+=" AND ID="+obj.getId();
		}
		
		if(obj!=null&&obj.getGroup_name()!=null){
			sql+=" AND GROUP_NAME LIKE '%"+obj.getGroup_name()+"%'";
		}
		
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<UserGroups> list=new ArrayList<UserGroups>();//存放UserGroups类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String group_name = (String)m.get("GROUP_NAME");//会员组名称
			String remark = (String)m.get("REMARK");//备注
			
			UserGroups userGroups=new UserGroups();//创建实体类对象
			userGroups.setId(idd);
			userGroups.setGroup_name(group_name);
			userGroups.setRemark(remark);
			list.add(userGroups);
		}
		return list;
	}
	
	/**
	 * 通过条件获得信息列表，不包括给定编号的用户组
	 * @param notInIds
	 * @return
	 */
	public List<UserGroups> getList(String notInIds)throws Exception{
		//构建查询语句
		String sql="SELECT * FROM USER_GROUPS WHERE 1=1";
		
		if(notInIds!=null&&!"".equals(notInIds)){
			sql+=" AND ID NOT IN("+notInIds+")";
		}
		
		List<Map> resultList = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<UserGroups> list=new ArrayList<UserGroups>();//存放UserGroups类型对象的集合
		
		for(Map m:resultList){
			Integer idd = (Integer)m.get("ID");//编号
			String group_name = (String)m.get("GROUP_NAME");//会员组名称
			String remark = (String)m.get("REMARK");//备注
			
			UserGroups userGroups=new UserGroups();//创建实体类对象
			userGroups.setId(idd);
			userGroups.setGroup_name(group_name);
			userGroups.setRemark(remark);
			list.add(userGroups);
		}
		return list;
	}
	

}
