package com.examsys.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.AdminRoles;
/**
 * 管理员角色数据访问层实现类
 * @author edu-1
 *
 */
public class AdminRolesDaoImpl extends AbstractBaseDao<AdminRoles,Integer> implements AdminRolesDao{

	/**
	 * 添加管理员角色
	 * @throws Exception 
	 */
	@Override
	public void add(AdminRoles obj) throws Exception {
		//构建插入语句
		String sql="Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (ADMIN_ROLES_ID_SEQ.nextval,?,?,?,?)";
		this.execute(sql, new Object[]{obj.getRole_name(),obj.getRole_privelege(),obj.getCreate_date(),obj.getRemark()});
	}

	/**
	 * 修改管理员角色
	 * @throws Exception 
	 */
	@Override
	public void update(AdminRoles obj) throws Exception {
		//构建更新语句
		String sql="UPDATE ADMIN_ROLES SET ROLE_NAME=?,ROLE_PRIVELEGE=?,CREATE_DATE=?,REMARK=? WHERE ID=?";
		this.execute(sql, new Object[]{obj.getRole_name(),obj.getRole_privelege(),obj.getCreate_date(),obj.getRemark(),obj.getId()});
	}

	/**
	 * 删除管理员角色
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public void delete(Integer id) throws Exception {
		//构建删除语句
		String sql="DELETE FROM ADMIN_ROLES WHERE ID=?";
		this.execute(sql, new Object[]{id});
	}

	/**
	 * 获取管理员角色
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public AdminRoles get(Integer id) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM ADMIN_ROLES WHERE ID=?";
		Map m = this.uniqueQuery(sql, new Object[]{id});//调用父类的查询方法拿数据
		
		Integer idd = (Integer)m.get("ID");//编号
		String role_name = (String)m.get("ROLE_NAME");//角色名称
		String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
		
		//由于数据返回的日期类型是Timestamp, 得强转
		Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
		Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
		
		String remark = (String)m.get("REMARK");//备注
		
		AdminRoles adminRoles=new AdminRoles();//创建实体类对象
		//将数据回填到实体类对象中
		adminRoles.setId(idd);
		adminRoles.setRole_name(role_name);
		adminRoles.setRole_privelege(role_privelege);
		
		adminRoles.setCreate_date(create_date2);
		adminRoles.setRemark(remark);
		
		return adminRoles;
	}

	/**
	 * 获得管理员角色表所有记录
	 */
	@Override
	public List<AdminRoles> getList() throws Exception {
		//构建查询语句
		String sql="SELECT * FROM ADMIN_ROLES";
		List<Map> resultList=this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<AdminRoles> list=new ArrayList<AdminRoles>();//存放AdminRoles类型对象的集合
		
		for(Map m:resultList){
		
			AdminRoles adminRoles=new AdminRoles();//创建对象
			Integer idd = (Integer)m.get("ID");//编号
			String role_name = (String)m.get("ROLE_NAME");//角色名称
			String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
			Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
			
			String remark = (String)m.get("REMARK");//备注
			
			adminRoles.setId(idd);
			adminRoles.setRole_name(role_name);
			adminRoles.setRole_privelege(role_privelege);
			
			adminRoles.setCreate_date(create_date2);
			adminRoles.setRemark(remark);
			
			list.add(adminRoles);//对象存入集合
		}
		
		return list;
	}

	/**
	 * 带条件获取管理员角色记录
	 */
	@Override
	public List<AdminRoles> getList(AdminRoles obj) throws Exception {
		//构建查询语句
		String sql="SELECT * FROM ADMIN_ROLES WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND ID="+obj.getId();
			}
			
			if(obj.getRole_name()!=null&&!"".equals(obj.getRole_name())){
				sql+=" AND ROLE_NAME LIKE '%"+obj.getRole_name()+"%'";
			}
		}
		
		List<Map> resultList=this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		List<AdminRoles> list=new ArrayList<AdminRoles>();//存放AdminRoles类型对象的集合
		
		for(Map m:resultList){
		
			AdminRoles adminRoles=new AdminRoles();//创建对象
			Integer idd = (Integer)m.get("ID");//编号
			String role_name = (String)m.get("ROLE_NAME");//角色名称
			String role_privelege = (String)m.get("ROLE_PRIVELEGE");//此角色所能操作的功能项
			
			//由于数据返回的日期类型是Timestamp, 得强转
			Timestamp create_date = (Timestamp)m.get("CREATE_DATE");//创建时间
			Date create_date2 = new Date(create_date.getTime());//需要转换一下时间
			
			String remark = (String)m.get("REMARK");//备注
			
			adminRoles.setId(idd);
			adminRoles.setRole_name(role_name);
			adminRoles.setRole_privelege(role_privelege);
			
			adminRoles.setCreate_date(create_date2);
			adminRoles.setRemark(remark);
			
			list.add(adminRoles);//对象存入集合
		}
		
		return list;
	}

}
