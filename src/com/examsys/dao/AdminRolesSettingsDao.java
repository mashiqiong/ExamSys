package com.examsys.dao;

import com.examsys.po.AdminRolesSettings;

/**
 * 
 * 系统功能数据访问层接口
 * @author edu-1
 *
 */
public interface AdminRolesSettingsDao extends IBaseDao<AdminRolesSettings, Integer> {

}
