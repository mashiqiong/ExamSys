package com.examsys.dao;

import com.examsys.po.ExamMain;
/**
 * 考生答题卡数据访问层接口
 * @author edu-1
 *
 */
public interface ExamMainDao extends IBaseDao<ExamMain, Integer> {
	/**
	 * 获取试题系列
	 * @return
	 * @throws Exception
	 */
	public Integer getSeq() throws Exception;//获取试题系列
}
