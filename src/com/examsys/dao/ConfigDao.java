package com.examsys.dao;

import com.examsys.po.Config;
/**
 * 系统参数数据访问层接口
 * @author edu-1
 *
 */
public interface ConfigDao extends IBaseDao<Config, Integer> {

}
