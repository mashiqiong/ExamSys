package com.examsys.dao;

import java.util.List;

import com.examsys.po.Paper;
/**
 * 试卷数据访问层接口
 * @author edu-1
 *
 */
public interface PaperDao extends IBaseDao<Paper, Integer> {
	/**
	 * 获取试题系列
	 * @return
	 * @throws Exception
	 */
	public Integer getSeq() throws Exception;//获取试题系列
	
	/**
	 * 通过编号范围来获得试卷信息列表
	 * @param inIds
	 * @return
	 */
	public List<Paper> getList(String inIds)throws Exception;
}
