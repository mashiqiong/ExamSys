package com.examsys.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * 考生答题主表实体类或也就是答题卡
 * @author edu-1
 *
 */
public class ExamMain {
	private Integer id;//编号
	private Users users;//答题卡所属的用户或者考试者
	private Paper paper;//所答的试卷
	private Timestamp start_time;//开始考试时间
	private Timestamp end_time;//结束考试时间
	private double score;//所得分数
	private String ip;//登录进来的IP
	private String status;//考试状态,1考试中，2已经交卷，3已经评分
	private String remark;//备注
	private List<ExamDetail> examDetails=new ArrayList<ExamDetail>();//答题明细,一对多
	
	public ExamMain() {
		super();
	}
	
	public ExamMain(Integer id, Users users, Paper paper, Timestamp start_time, Timestamp end_time, double score, String ip,
			String status, String remark) {
		super();
		this.id = id;
		this.users = users;
		this.paper = paper;
		this.start_time = start_time;
		this.end_time = end_time;
		this.score = score;
		this.ip = ip;
		this.status = status;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	public Timestamp getStart_time() {
		return start_time;
	}
	public void setStart_time(Timestamp start_time) {
		this.start_time = start_time;
	}
	public Timestamp getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Timestamp end_time) {
		this.end_time = end_time;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public List<ExamDetail> getExamDetails() {
		return examDetails;
	}

	public void setExamDetails(List<ExamDetail> examDetails) {
		this.examDetails = examDetails;
	}

	@Override
	public String toString() {
		return "ExamMain [id=" + id + ", users=" + users + ", paper=" + paper + ", start_time=" + start_time
				+ ", end_time=" + end_time + ", score=" + score + ", ip=" + ip + ", status=" + status + ", remark="
				+ remark + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamMain other = (ExamMain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
