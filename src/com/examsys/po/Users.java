package com.examsys.po;

import java.sql.Date;

/**
 * 会员或或者考试者实体类
 * @author edu-1
 *
 */
public class Users implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private UserGroups userGroups;//所属用户组
	private String user_name;//会员名或者叫账号
	private String user_pass;//登录密码
	private String user_no;//学号
	private String real_name;//真实姓名
	private String email;//邮箱
	private String phone;//联系电话
	private Date create_date;//注册时间
	private Date login_date;//最后一次的登录时间
	private Integer login_times;//登录次数
	private String status;//状态，1启用，0停用，默认1
	private String remark;//备注
	
	public Users() {
		super();
	}
	
	public Users(Integer id, UserGroups userGroups, String user_name, String user_pass, String user_no,
			String real_name, String email, String phone, Date create_date, Date login_date, Integer login_times,
			String status, String remark) {
		super();
		this.id = id;
		this.userGroups = userGroups;
		this.user_name = user_name;
		this.user_pass = user_pass;
		this.user_no = user_no;
		this.real_name = real_name;
		this.email = email;
		this.phone = phone;
		this.create_date = create_date;
		this.login_date = login_date;
		this.login_times = login_times;
		this.status = status;
		this.remark = remark;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserGroups getUserGroups() {
		return userGroups;
	}
	public void setUserGroups(UserGroups userGroups) {
		this.userGroups = userGroups;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_pass() {
		return user_pass;
	}
	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}
	public String getUser_no() {
		return user_no;
	}
	public void setUser_no(String user_no) {
		this.user_no = user_no;
	}
	public String getReal_name() {
		return real_name;
	}
	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getLogin_date() {
		return login_date;
	}
	public void setLogin_date(Date login_date) {
		this.login_date = login_date;
	}
	public Integer getLogin_times() {
		return login_times;
	}
	public void setLogin_times(Integer login_times) {
		this.login_times = login_times;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", userGroups=" + userGroups + ", user_name=" + user_name + ", user_pass="
				+ user_pass + ", user_no=" + user_no + ", real_name=" + real_name + ", email=" + email + ", phone="
				+ phone + ", create_date=" + create_date + ", login_date=" + login_date + ", login_times=" + login_times
				+ ", status=" + status + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((user_name == null) ? 0 : user_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (user_name == null) {
			if (other.user_name != null)
				return false;
		} else if (!user_name.equals(other.user_name))
			return false;
		return true;
	}
	
}
