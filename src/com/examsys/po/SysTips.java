package com.examsys.po;

/**
 * 系统提示信息实体类
 * @author edu-1
 *
 */
public class SysTips implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private String scode;//代码
	private String sdesc;//信息内容
	
	public SysTips() {
		super();
	}

	public SysTips(Integer id, String scode, String sdesc) {
		super();
		this.id = id;
		this.scode = scode;
		this.sdesc = sdesc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getScode() {
		return scode;
	}

	public void setScode(String scode) {
		this.scode = scode;
	}

	public String getSdesc() {
		return sdesc;
	}

	public void setSdesc(String sdesc) {
		this.sdesc = sdesc;
	}

	@Override
	public String toString() {
		return "SysTips [id=" + id + ", scode=" + scode + ", sdesc=" + sdesc + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SysTips other = (SysTips) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
