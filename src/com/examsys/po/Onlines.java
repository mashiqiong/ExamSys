package com.examsys.po;

import java.sql.Date;

/**
 * 考试者在线考试的时间记录表实体类
 * @author edu-1
 *
 */
public class Onlines implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private Users users;//用户或考试者
	private Paper paper;//试卷
	private Date last_time;//最后一次登录时间
	private String ip;//登录进来的IP
	
	public Onlines() {
		super();
	}
	
	public Onlines(Integer id, Users users, Paper paper, Date last_time, String ip) {
		super();
		this.id = id;
		this.users = users;
		this.paper = paper;
		this.last_time = last_time;
		this.ip = ip;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
	}

	public Date getLast_time() {
		return last_time;
	}

	public void setLast_time(Date last_time) {
		this.last_time = last_time;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "Onlines [id=" + id + ", users=" + users + ", paper=" + paper + ", last_time=" + last_time + ", ip=" + ip
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Onlines other = (Onlines) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
