package com.examsys.po;
/**
 * 记录可以参与考试的用户组实体类
 * @author edu-1
 *
 */
public class PaperUserGroup implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private Paper paper;//试卷
	private UserGroups userGroups;//所允许的用户组
	
	public PaperUserGroup() {
		super();
	}
	
	public PaperUserGroup(Integer id, Paper paper, UserGroups userGroups) {
		super();
		this.id = id;
		this.paper = paper;
		this.userGroups = userGroups;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	public UserGroups getUserGroups() {
		return userGroups;
	}
	public void setUserGroups(UserGroups userGroups) {
		this.userGroups = userGroups;
	}

	@Override
	public String toString() {
		return "PaperUserGroup [id=" + id + ", paper=" + paper + ", userGroups=" + userGroups + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaperUserGroup other = (PaperUserGroup) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
