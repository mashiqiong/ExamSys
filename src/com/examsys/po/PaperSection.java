package com.examsys.po;

import java.util.ArrayList;
import java.util.List;

/**
 * 试卷中的章节实体类
 * @author edu-1
 *
 */
public class PaperSection {
	private Integer id;//编号
	private Paper paper;//本章或本部分所属的试卷
	private String section_name;//章节名称
	private Integer per_score;//本章节的分数
	private String remark;//备注
	private List<PaperDetail> paperDetails=new ArrayList<PaperDetail>();//试卷明细
	
	public PaperSection() {
		super();
	}

	public PaperSection(Integer id, Paper paper, String section_name, Integer per_score, String remark) {
		super();
		this.id = id;
		this.paper = paper;
		this.section_name = section_name;
		this.per_score = per_score;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	public String getSection_name() {
		return section_name;
	}
	public void setSection_name(String section_name) {
		this.section_name = section_name;
	}
	public Integer getPer_score() {
		return per_score;
	}
	public void setPer_score(Integer per_score) {
		this.per_score = per_score;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<PaperDetail> getPaperDetails() {
		return paperDetails;
	}

	public void setPaperDetails(List<PaperDetail> paperDetails) {
		this.paperDetails = paperDetails;
	}

	@Override
	public String toString() {
		return "PaperSection [id=" + id + ", paper=" + paper + ", section_name=" + section_name + ", per_score="
				+ per_score + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaperSection other = (PaperSection) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
