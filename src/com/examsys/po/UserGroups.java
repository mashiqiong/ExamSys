package com.examsys.po;
/**
 * 会员组或考试组实体类
 * @author edu-1
 *
 */
public class UserGroups implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private String group_name;//组名
	private String remark;//备注
	
	public UserGroups() {
		super();
	}
	
	public UserGroups(Integer id, String group_name, String remark) {
		super();
		this.id = id;
		this.group_name = group_name;
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "UserGroups [id=" + id + ", group_name=" + group_name + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group_name == null) ? 0 : group_name.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserGroups other = (UserGroups) obj;
		if (group_name == null) {
			if (other.group_name != null)
				return false;
		} else if (!group_name.equals(other.group_name))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
