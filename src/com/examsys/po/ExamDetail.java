package com.examsys.po;

/**
 * 考生答题明细表实体类或者答题卡明细
 * @author edu-1
 *
 */
public class ExamDetail implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private ExamMain examMain;//答题卡主表
	private Question question;//所答题目
	private String answer;//考生答题内容
	private double score;//本题所得分数
	private String status;//状态，用于记录此题是否作答了,0未作答，1已作答
	private String remark;//备注
	
	public ExamDetail() {
		super();
	}
	
	public ExamDetail(Integer id, ExamMain examMain, Question question, String answer, double score, String status,
			String remark) {
		super();
		this.id = id;
		this.examMain = examMain;
		this.question = question;
		this.answer = answer;
		this.score = score;
		this.status = status;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ExamMain getExamMain() {
		return examMain;
	}
	public void setExamMain(ExamMain examMain) {
		this.examMain = examMain;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "ExamDetail [id=" + id + ", examMain=" + examMain + ", question=" + question + ", answer=" + answer
				+ ", score=" + score + ", status=" + status + ", remark=" + remark + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamDetail other = (ExamDetail) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
