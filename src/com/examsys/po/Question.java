package com.examsys.po;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * 试题实体类
 * @author edu-1
 *
 */
public class Question implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private QuestionDb questionDb;//所属题库
	private Admin admin;//添加此题的管理员
	private Integer qtype;//题目类型，1单选，2多选，3填空，4判断，5问答
	private Integer qlevel;//难度级别,1易，2正常，3难
	private String qfrom;//题目来源
	private String content;//题干内容
	private String skey;//标准答案
	private String key_desc;//试题解析
	private Date create_date;//创建时间
	private String status;//状态，题目状态，0不完全开放，1完全开放
	private String remark;//备注
	private List<QuestionOptions> questionOptionss=new ArrayList<QuestionOptions>();//试题选项集合
	
	public Question() {
		super();
	}
	
	public Question(Integer id, QuestionDb questionDb, Admin admin, Integer qtype, Integer qlevel, String qfrom,
			String content, String skey, String key_desc, Date create_date, String status, String remark) {
		super();
		this.id = id;
		this.questionDb = questionDb;
		this.admin = admin;
		this.qtype = qtype;
		this.qlevel = qlevel;
		this.qfrom = qfrom;
		this.content = content;
		this.skey = skey;
		this.key_desc = key_desc;
		this.create_date = create_date;
		this.status = status;
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public QuestionDb getQuestionDb() {
		return questionDb;
	}
	public void setQuestionDb(QuestionDb questionDb) {
		this.questionDb = questionDb;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public Integer getQtype() {
		return qtype;
	}
	public void setQtype(Integer qtype) {
		this.qtype = qtype;
	}
	public Integer getQlevel() {
		return qlevel;
	}
	public void setQlevel(Integer qlevel) {
		this.qlevel = qlevel;
	}
	public String getQfrom() {
		return qfrom;
	}
	public void setQfrom(String qfrom) {
		this.qfrom = qfrom;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSkey() {
		return skey;
	}
	public void setSkey(String skey) {
		this.skey = skey;
	}
	public String getKey_desc() {
		return key_desc;
	}
	public void setKey_desc(String key_desc) {
		this.key_desc = key_desc;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<QuestionOptions> getQuestionOptionss() {
		return questionOptionss;
	}

	public void setQuestionOptionss(List<QuestionOptions> questionOptionss) {
		this.questionOptionss = questionOptionss;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", questionDb=" + questionDb + ", admin=" + admin + ", qtype=" + qtype
				+ ", qlevel=" + qlevel + ", qfrom=" + qfrom + ", content=" + content + ", skey=" + skey + ", key_desc="
				+ key_desc + ", create_date=" + create_date + ", status=" + status + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
