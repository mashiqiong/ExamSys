package com.examsys.po;
/**
 * 试题选项实体类
 * @author edu-1
 *
 */
public class QuestionOptions implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private Question question;//所属试题
	private String salisa;//选项编号
	private String soption;//选项描述
	private String sextend;//面试题的扩展项
	
	public QuestionOptions() {
		super();
	}
	
	public QuestionOptions(Integer id, Question question, String salisa, String soption, String sextend) {
		super();
		this.id = id;
		this.question = question;
		this.salisa = salisa;
		this.soption = soption;
		this.sextend = sextend;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getSalisa() {
		return salisa;
	}
	public void setSalisa(String salisa) {
		this.salisa = salisa;
	}
	public String getSoption() {
		return soption;
	}
	public void setSoption(String soption) {
		this.soption = soption;
	}
	public String getSextend() {
		return sextend;
	}
	public void setSextend(String sextend) {
		this.sextend = sextend;
	}

	
	@Override
	public String toString() {
		return "QuestionOptions [id=" + id + ", question=" + question + ", salisa=" + salisa + ", soption=" + soption
				+ ", sextend=" + sextend + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionOptions other = (QuestionOptions) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
