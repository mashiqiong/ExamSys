package com.examsys.po;

import java.sql.Date;

/**
 * 题库实体类
 * @author edu-1
 *
 */
public class QuestionDb implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private Admin admin;//创建此题库的管理员
	private String name;//题库名称
	private Date create_date;//创建时间
	private String status;//状态，题库状态，1表示正常，-1表示锁定
	private String remark;//备注
	
	public QuestionDb() {
		super();
	}
	
	public QuestionDb(Integer id, Admin admin, String name, Date create_date, String status, String remark) {
		super();
		this.id = id;
		this.admin = admin;
		this.name = name;
		this.create_date = create_date;
		this.status = status;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	@Override
	public String toString() {
		return "QuestionDb [id=" + id + ", admin=" + admin + ", name=" + name + ", create_date=" + create_date
				+ ", status=" + status + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionDb other = (QuestionDb) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
