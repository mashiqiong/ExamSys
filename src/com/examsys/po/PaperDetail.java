package com.examsys.po;
/**
 * 试卷明细实体类
 * @author edu-1
 *
 */
public class PaperDetail implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private Paper paper;//本章或本部分所属的试卷
	private PaperSection paperSection;//所属的章节
	private Question question;//所属试题
	private Double score;//本题分数
	private Integer porder;//排序号
	
	public PaperDetail() {
		super();
	}

	public PaperDetail(Integer id, Paper paper, PaperSection paperSection, Question question, Double score,
			Integer porder) {
		super();
		this.id = id;
		this.paper = paper;
		this.paperSection = paperSection;
		this.question = question;
		this.score = score;
		this.porder = porder;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
	}

	public PaperSection getPaperSection() {
		return paperSection;
	}

	public void setPaperSection(PaperSection paperSection) {
		this.paperSection = paperSection;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getPorder() {
		return porder;
	}

	public void setPorder(Integer porder) {
		this.porder = porder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaperDetail other = (PaperDetail) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaperDetail [id=" + id + ", paper=" + paper + ", paperSection=" + paperSection + ", question="
				+ question + ", score=" + score + ", porder=" + porder + "]";
	}
	
}
