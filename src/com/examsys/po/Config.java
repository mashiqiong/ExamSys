package com.examsys.po;
/**
 * 系统参数实体类
 * @author edu-1
 *
 */
public class Config implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer id;//编号
	private String name;//参数名称
	private String config_key;//参数代码
	private String config_value;//参数值
	private String remark;//备注
	
	public Config() {
		super();
	}

	public Config(Integer id, String name, String config_key, String config_value, String remark) {
		super();
		this.id = id;
		this.name = name;
		this.config_key = config_key;
		this.config_value = config_value;
		this.remark = remark;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfig_key() {
		return config_key;
	}

	public void setConfig_key(String config_key) {
		this.config_key = config_key;
	}

	public String getConfig_value() {
		return config_value;
	}

	public void setConfig_value(String config_value) {
		this.config_value = config_value;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Config [id=" + id + ", name=" + name + ", config_key=" + config_key + ", config_value=" + config_value
				+ ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Config other = (Config) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
}
