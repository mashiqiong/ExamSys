package com.examsys.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * 试卷实体类
 * @author edu-1
 *
 */
public class Paper {
	private Integer id;//编号
	private Admin admin;//创建此试卷的管理员
	private String paper_name;//试卷名称
	private Timestamp start_time;//开始考试时间
	private Timestamp end_time;//结束考试时间
	private Integer paper_minute;//考试总时间
	private Integer total_score;//考试总分数
	private Timestamp post_date;//交卷时间
	private Timestamp show_score;//公布成绩时间
	private Integer qorder;//题目顺序,0表示自然顺序，1表示打乱随机
	private String status;//状态，1开放，-1不开放
	private String remark;//备注
	private List<PaperSection> paperSections=new ArrayList<PaperSection>();//试卷章节
	private List<PaperDetail> paperDetails=new ArrayList<PaperDetail>();//试卷明细
	
	public Paper() {
		super();
	}
	
	public Paper(Integer id, Admin admin, String paper_name, Timestamp start_time, Timestamp end_time, Integer paper_minute,
			Integer total_score, Timestamp post_date, Timestamp show_score, Integer qorder, String status, String remark) {
		super();
		this.id = id;
		this.admin = admin;
		this.paper_name = paper_name;
		this.start_time = start_time;
		this.end_time = end_time;
		this.paper_minute = paper_minute;
		this.total_score = total_score;
		this.post_date = post_date;
		this.show_score = show_score;
		this.qorder = qorder;
		this.status = status;
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public String getPaper_name() {
		return paper_name;
	}
	public void setPaper_name(String paper_name) {
		this.paper_name = paper_name;
	}
	public Timestamp getStart_time() {
		return start_time;
	}
	public void setStart_time(Timestamp start_time) {
		this.start_time = start_time;
	}
	public Timestamp getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Timestamp end_time) {
		this.end_time = end_time;
	}
	public Integer getPaper_minute() {
		return paper_minute;
	}
	public void setPaper_minute(Integer paper_minute) {
		this.paper_minute = paper_minute;
	}
	public Integer getTotal_score() {
		return total_score;
	}
	public void setTotal_score(Integer total_score) {
		this.total_score = total_score;
	}
	public Timestamp getPost_date() {
		return post_date;
	}
	public void setPost_date(Timestamp post_date) {
		this.post_date = post_date;
	}
	public Timestamp getShow_score() {
		return show_score;
	}
	public void setShow_score(Timestamp show_score) {
		this.show_score = show_score;
	}
	public Integer getQorder() {
		return qorder;
	}
	public void setQorder(Integer qorder) {
		this.qorder = qorder;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public List<PaperSection> getPaperSections() {
		return paperSections;
	}

	public void setPaperSections(List<PaperSection> paperSections) {
		this.paperSections = paperSections;
	}

	public List<PaperDetail> getPaperDetails() {
		return paperDetails;
	}

	public void setPaperDetails(List<PaperDetail> paperDetails) {
		this.paperDetails = paperDetails;
	}

	@Override
	public String toString() {
		return "Paper [id=" + id + ", admin=" + admin + ", paper_name=" + paper_name + ", start_time=" + start_time
				+ ", end_time=" + end_time + ", paper_minute=" + paper_minute + ", total_score=" + total_score
				+ ", post_date=" + post_date + ", show_score=" + show_score + ", qorder=" + qorder + ", status="
				+ status + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((paper_name == null) ? 0 : paper_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paper other = (Paper) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (paper_name == null) {
			if (other.paper_name != null)
				return false;
		} else if (!paper_name.equals(other.paper_name))
			return false;
		return true;
	}

}
