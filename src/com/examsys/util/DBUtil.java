package com.examsys.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * 数据库工具类，用于打开连接，关闭资源等
 * @author Oracle
 *
 */
public class DBUtil {

    private static final ThreadLocal threadLocal = new ThreadLocal();//存放数据库连接的线程池

    /**
     * 获得数据库连接对象的方法
     * @return
     * @throws Exception
     */
    public static synchronized Connection getConnection()
            throws Exception {
        Connection conn = (Connection) threadLocal.get();//从线程池中拿出数据库连接对象
        try {
            if (conn == null) {//如果没有拿到数据库连接对象,则打开一个新的连接供使用
                conn = ConnectionManager.getInstance().getConnection();
                conn.setAutoCommit(false);//设置此数据库连接不自动提交事务
                threadLocal.set(conn);//把打开的连接对象存放线程池中，供下次使用省掉创建连接的时间
            }
        } catch (Exception e) {
            throw new Exception("数据库访问失败..");
        }
        return conn;
    }

    /**
     * 关掉数据库连接对象
     * @throws Exception
     */
    public static void closeConnection()
            throws Exception {
        Connection conn = (Connection) threadLocal.get();//从线程池中拿出数据库连接对象
        if ((conn != null) && (!conn.isClosed())) {//如果拿到数据库连接对象,则关闭连接
            try {
                conn.close();//关闭连接
            } catch (Exception e) {
                throw new Exception("关闭数据库连接失败..");
            }
            threadLocal.set(null);//从线程池中清掉此数据库连接对象
        }
    }

    /**
     * 提交事务的方法
     * @throws Exception
     */
    public static void commit() throws Exception {
        getConnection().commit();//提交事务
    }

    /**
     * 回滚事务的方法
     */
    public static void rollback() {
        try {
            getConnection().rollback();//回滚事务的方法
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关掉sql发送对象，释放资源
     * @param stmt
     */
    public static void close(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();//关掉sql发送对象，释放资源
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关掉数据集对象，释放资源
     * @param rs
     */
    public static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();//关掉数据集对象，释放资源
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关掉给定的数据库连接对象
     * @param conn
     */
    public static void close(Connection conn) {
        try {
            if (conn != null) {//如果给的连接对象不为null,则进一关闭
                conn.close();//关掉给定的数据库连接对象
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
