package com.examsys.util;

import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

/**
 * 数据源管理类
 * @author Oracle
 *
 */
public class ConnectionManager {

    private static ConnectionManager instance;//单例数据源管理对象
    private ComboPooledDataSource ds;//数据源对象

    private ConnectionManager()
            throws Exception {
        //初始数据源，扫描类路径下的配置文件，并连接数据库
        this.ds = new ComboPooledDataSource("userApp");
    }

    /**
     * 获得本数据源管理对象的工厂方法
     * @return
     */
    public static final ConnectionManager getInstance() {
        if (instance == null) {
            try {
                instance = new ConnectionManager();//构造自己的一个实例对象
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    /**
     * 通过数据源取得一个数据库连接对象
     * @return
     */
    public final synchronized Connection getConnection() {
        Connection cnn = null;//数据库连接对象
        if (this.ds != null) {//如果数据源存在，也就是数据库资源有效时
            try {
                cnn = this.ds.getConnection();//打开连接
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (cnn == null) {//如果数据库连接还是没初始化成功，刚加载XML文件读取相关数据库连接信息，如：账号，密码，URL等
            try {
                this.ds = new ComboPooledDataSource("userApp");//读取c3p0-config.xml中name为userApp单元
                return this.ds.getConnection();//打开连接
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return cnn;//返回数据库连接对象
    }

    protected void finalize() throws Throwable {
        DataSources.destroy(this.ds);//释放数据库源对象
        super.finalize();
    }
}

